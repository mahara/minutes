20:02:29 <anzeljg> #startmeeting
20:02:29 <maharameet> Meeting started Tue Feb 26 20:02:29 2013 UTC.  The chair is anzeljg. Information about MeetBot at http://wiki.debian.org/MeetBot.
20:02:29 <maharameet> Useful Commands: #action #agreed #help #info #idea #link #topic.
20:02:31 <anzeljg> Welcome to the 24th Mahara Developer Meeting. Please state your name and location by starting a line with #info. When you have a long statement broken up over several   lines, please put .. on a separate line when you are done.
20:02:39 <anzeljg> #info anzeljg is Gregor Anželj, developer and translator, Ljubljana, Slovenia
20:02:44 <aquaman> #info aquaman is Hugh Davenport
20:03:08 <rkabalin_> #info rkabalin_ is Ruslan Kabalin, Lancaster University, Lancaster, UK
20:03:20 <anitsirk> #info anitsirk is Kristina Hoeppner, Catalyst IT, Wellington, NZ
20:04:02 <rkabalin_> not that many people )
20:04:05 <anzeljg> anybody else?
20:04:37 <anzeljg> #topic Items from last meeting
20:04:52 <anzeljg> #info anzeljg will create a wiki page under "Specifications in development" for his enhancement suggestions for the resume and start a forum topic to discuss these.
20:05:03 <anzeljg> So I'll chair and start...
20:05:29 <anzeljg> Created wiki page, which can be accessed via #link https://wiki.mahara.org/index.php/Developer_Area/Specifications_in_Development/Resume_enhancements
20:05:47 <anzeljg> and forum topic, accessible via #link https://mahara.org/interaction/forum/topic.php?id=5245
20:06:11 <anzeljg> Dajan replied to forum topic and he agrees with proposed changes.
20:06:12 <anzeljg> ..
20:07:24 <anzeljg> any questions, comments...
20:07:57 <anzeljg> hi sonn
20:08:16 <anzeljg> please use info to introduce yourself
20:08:21 <sonn2> Hi
20:08:43 <sonn2> #info sonn, Son Nguyen, Catalyst IT NZ
20:09:13 <rkabalin_> The address fromat idea is good
20:09:38 <anzeljg> We're discussing Resume enhancements, although I'm getting the felling that I0m talking with myself :)
20:10:05 <sonn2> :)
20:10:19 <anzeljg> rkabalin: it's taken from Europass AddressFormat xml/xls file...
20:10:26 <rkabalin_> I see
20:11:34 <anzeljg> so the import/export from/to Europass (and/or other systmes) can be as compatible as possible...
20:12:14 <sonn2> What is Europass Address format?
20:13:12 <anzeljg> sonn2, please see #link http://europass.cedefop.europa.eu/xml/resources/EuropassAddressFormats_V1.0.xsd
20:13:29 <sonn2> thank anzeljg
20:14:00 <anzeljg> basically it serves for different display formats of addresses throughout European countries
20:14:04 <aquaman> hey jimcrib
20:14:10 <anzeljg> Addresses are formatted differently...
20:14:17 <anzeljg> jimcrib hi
20:14:25 <jimcrib> morning
20:14:55 <anzeljg> jimcrib could you info introduce yourself
20:15:36 <anzeljg> Is there anything else to discuss or shall we move forward?
20:16:38 <sonn2> I have one
20:16:45 <anzeljg> ok
20:17:17 <sonn2> I would replace the file browser by elfinder
20:17:38 <sonn2> as it has many bugs
20:17:56 <anzeljg> sonn2 can we discuss that a bit later...
20:18:02 <sonn2> ok
20:18:04 <anzeljg> we have items from last meeting?
20:18:11 <anzeljg> ok
20:18:19 <anzeljg> so the last item from previous meeting was:
20:18:23 <anzeljg> #info VolkerJSchmidt will contact Eleni Kargioti about Leap2A and Europass
20:22:03 <sonn2> is that all?
20:22:43 <anzeljg_> IRC stopped responding...
20:22:59 <anitsirk> anzeljg_: sorry. gotta go.
20:23:28 <anzeljg_> can anyone tell me, if you got my messages, which I got from Eleni?
20:23:33 <anzeljg_> anitsirk bye.
20:23:44 <sonn2> yes
20:23:48 <sonn2> I got it
20:23:54 <anzeljg_> everything?
20:23:59 <sonn2> just one
20:24:12 <anzeljg_> ok. So I'll repost that...
20:24:28 <anzeljg_> "At the end of 2012, we released the new version of Europass XML Schema (v3.0), which radically revamps the previous XML schema.
20:24:38 <anzeljg_> As you may be already aware, the main purpose of version 3.0 was to define an XML structure for the portfolio of the learner and serve as a portable repository of information relevant to the learner's career and development.
20:24:57 <anzeljg_> At this point we do not plan a major revision to the Europass CV/ESP implementation. However we are aware of the impact of Leap2A and therefore will definitely study it when we will be extending our implementation.
20:25:06 <anzeljg_> Unfortunately I cannot tell you at this point a specific time schedule, as this is something that needs to be analysed and agreed with Cedefop, the owner of Europass.
20:25:15 <anzeljg_> Nevertheless, I believe that interoperability between Europass CV/ESP and Leap2A could be achieved even at this point, by defining the business and technical details on how a Leap2A profile can be converted to a Europass CV/ESP profile and vice versa.
20:25:23 <anzeljg_> We have done something similar in the past in order to allow the seamless conversion between HR-XML and Europass-XML profiles.
20:25:33 <anzeljg_> If we decide to support such a feature our company can cooperate with technical staff from Mahara or Leap2A in order to define the conversion scenaria, the steps and technical resources (e.g. XSLT)."
20:25:41 <anzeljg_> That was her answer...
20:25:42 <anzeljg_> ..
20:28:05 <anzeljg_> I guess we will not deal with that right now, or are there any ideas, comments?
20:28:47 <sonn2> I think we should wait
20:29:45 <anzeljg_> #idea Wait for interoperability between Leap2A and Europass formats, but keep that in mind
20:30:06 <anzeljg_> If ekly is not here than sonn2 can continue...
20:30:28 <sonn2> So we move to next step>?
20:30:32 <anzeljg_> yep
20:31:16 <anzeljg_> sonn2 you were saying that you want to replace... (sorry forgot that)
20:31:18 <sonn2> When debugging Mahara, I find file browser is a bit odd and has many bugs
20:31:47 <sonn2> I'd use another 3rd party solution: elfinder
20:32:00 <anzeljg_> #topic Replace Mahara file browser with elfinder
20:32:01 <sonn2> http://elfinder.org/
20:32:40 <anzeljg_> sonn2: will that open in a separate pop-up?
20:32:40 <sonn2> It is a complete file manager using jquery
20:32:57 <rkabalin_> looks interesing
20:33:00 <sonn2> we can easily customize it for mahara use
20:33:01 <aquaman> looks cool
20:33:25 <anzeljg_> agree, look nice and cool
20:33:31 <sonn2> also easy to integrate with our current TinyMCE
20:34:18 <anzeljg_> #link http://elfinder.org/
20:34:24 <aquaman> and would save doing a security audit on the current filebrowser :P
20:34:25 <anzeljg_> to have the link also in minutes...
20:34:29 <aquaman> which probably has issues
20:34:34 <sonn2> sure
20:35:23 <anzeljg_> I guess now is the time that I should ask if we agree with that?
20:35:27 <anzeljg_> so do we agree?
20:35:40 <rkabalin_> #agree
20:35:46 <anzeljg_> #agree
20:36:03 <sonn2> It needs to do security investigation
20:36:16 <sonn2> before using it
20:37:16 <anzeljg_> sonn2: You'll do it, right?
20:37:56 <sonn2> I will put it in the development specification
20:38:11 <sonn2> and will try to do it when possible
20:38:57 <anzeljg_> #action sonn2 will create development specification and security investigation for replacing file browser with elfinder
20:38:58 <rkabalin_> might also worth checking if there any other good open filebrowsers in the internet
20:39:00 <sonn2> will need aquaman's help about security issue
20:39:22 <sonn2> I did
20:39:39 <anzeljg_> what is the file browser that Moodle uses?
20:39:43 <sonn2> and IMO, elfinder is the best
20:40:05 <sonn2> I have no idea
20:40:17 <sonn2> aquaman may know
20:40:18 <anzeljg_> anyway, I like elfinder...
20:40:29 <rkabalin_> It is a custom browser there AFAIK
20:40:48 <rkabalin_> Written by Davo Smith I think
20:41:15 <anzeljg_> aquaman: will you help sonn2 with security issue?
20:41:34 <aquaman> ohai
20:41:56 <anzeljg_> #action aquaman will help sonn2 with security issue
20:42:12 <sonn2> https://moodle.org/plugins/view.php?plugin=block_repo_filemanager
20:42:16 <aquaman> should have time one weekend
20:42:28 <anzeljg_> :)
20:42:32 <sonn2> :)
20:43:03 <anzeljg_> Is there anything else on that matter?
20:43:29 <sonn2> rkabalin_: is the plugin you want?
20:44:12 <sonn2> I think that is all for the moment
20:44:21 <anzeljg_> OK
20:44:58 <anzeljg_> Is elky here or any of you guys knows anything about Mahara 1.7 release progress
20:45:50 <sonn2> anitsirk knows it better than me :)
20:46:18 <anzeljg_> anitsirk would you like to update us all?
20:46:37 <anitsirk> still in a meeting. sorry
20:47:00 <anzeljg_> ok. so we'll skip that for now and maybe return to that later?
20:47:06 <sonn2> yes
20:47:21 <anzeljg_> #topic Next meeting and chair
20:47:32 <anzeljg_> We usually wanted to have a meeting every 5-6 weeks. That would be the 16th of April at 8 p.m. NZDT. That would be 9 a.m. UK and 10 a.m. Central Europe.
20:47:46 <anzeljg_> That is my suggestion...
20:48:46 <sonn2> agreed
20:48:54 <rkabalin_> agreed
20:49:58 <anzeljg_> anybody else? aquaman? anitsirk?
20:51:12 <anzeljg_> #info The 25th Mahara Developer Meeting will be held on the 16th of April at 8 p.m. NZDT. That will be 9 a.m. UK and 10 a.m. Central Europe. #link http://www.timeanddate.com/worldclock/fixedtime.html?msg=25th+Mahara+Developer+Meeting&iso=20130416T08
20:51:29 <anzeljg_> who would like to chair it?
20:51:57 <rkabalin_> I can
20:52:08 <sonn2> thank rkabalin_
20:52:17 <rkabalin_> np
20:52:20 <rkabalin_> ))
20:52:29 <anzeljg_> thanx
20:52:46 <sonn2> I would be but I may be late :)
20:52:52 <anzeljg_> #info rkabalin will chair next, 25th Mahara Developer Meeting
20:53:14 <anzeljg_> #topic Any other business
20:53:25 <sonn2> none for me
20:53:42 <anzeljg_> anybody else?
20:53:45 <rkabalin_> thanks anzeljg_, none for me either
20:53:58 <anzeljg_> I have a question though...
20:54:05 <sonn2> yes
20:54:10 <rkabalin_> go ahead
20:54:31 <anzeljg_> I found that on Mahara wiki... What's with the View (noe Page) Templates?
20:54:34 <anzeljg_> #link https://wiki.mahara.org/index.php/Developer_Area/HowToWriteAViewTemplate
20:54:43 <anzeljg_> noe = now
20:54:57 <anzeljg_> anybody knows?
20:54:59 <anzeljg_> ..
20:55:52 <sonn2> I did not find what you mentioned
20:57:34 <sonn2> I have no idea
20:57:44 <anzeljg_> ok. From wiki main index, click "Developer Area", than scroll down to "Finding your way around" section and click "How to write a view template"...
20:57:46 <rkabalin_> Brett written it originally, I did not even know about that functionality
20:57:59 <anzeljg_> Is it implemented or not...
20:58:18 <anzeljg_> I got the impression that it's not fully impemented. Would be nice though...
21:01:07 <sonn2> anzeljg_: did you try to write a view template?
21:01:19 <anzeljg_> no I didn't
21:01:24 <sonn2> ok
21:01:26 <anzeljg_> will try that
21:01:39 <sonn2> It sounds interesting
21:01:45 <anzeljg_> indeed
21:01:51 <sonn2> I will try one day
21:02:12 <anzeljg_> Still no sign of elky or anitsirk?
21:02:30 <sonn2> no
21:02:44 <anzeljg_> Does anybody else know about Mahara 1.7 release progress?
21:03:29 <anzeljg_> Are we past the feature freeze date?
21:03:31 <sonn2> I guess elky will update it later on wiki
21:03:45 <sonn2> or in IRC channel
21:03:46 <anzeljg_> ok
21:03:55 <anzeljg_> Any other AOB?
21:04:05 <anzeljg_> otherwise I'll end the meeting
21:04:17 <sonn2> Anyone hear about Hiphop-php?
21:04:26 <jimcrib> yeap
21:04:53 <sonn2> I have attended the Multicore World conference last week
21:05:16 <sonn2> It would be good to speed up Mahara
21:05:22 <jimcrib> I did some work with implementing intels TBB into it
21:05:36 <sonn2> awesome
21:06:26 <sonn2> It is still lack some extensions for mahara
21:06:52 <jimcrib> yeap plus it doesn't allow eval
21:07:03 <sonn2> I have tried to run mahara on Hiphop VM
21:07:32 <sonn2> after disabling some functions
21:07:45 <sonn2> It is very promissing
21:07:49 <jimcrib> The vm is make better than the compiler
21:08:04 <jimcrib> s/make/much/
21:08:05 <sonn2> It ran twice faster
21:08:23 <sonn2> It is worth to try
21:09:01 <sonn2> ok
21:09:18 <sonn2> that's all of me
21:09:57 <anzeljg_> anitsirk: Do you have the time now and want to talk about Mahara 1.7 release progress?
21:11:11 <anitsirk> anzeljg_: sorry. still in meetings.
21:11:21 <anzeljg_> ok
21:11:22 <anzeljg_> anybody else, anything else?
21:12:50 <anzeljg_> #endmeeting
21:13:08 <sonn2> bye anzeljg_
21:13:14 <anzeljg_> seems that now I can't end meeting
21:13:30 <aarow> whoops, I had the wrong time down on my calendar. I just got here.
21:13:30 <anzeljg_> anitsirk: could you end the meeting
21:14:45 <anzeljg_> aarow, sorry
21:15:36 <anzeljg_> since my IRC client stried on me previously, I'm now unable to end meeting...
21:15:50 <anzeljg_> can anybody do that instead of me?
21:16:27 <rkabalin_> thanks anzeljg_
21:16:34 <rkabalin_> #endmeeting
21:16:59 <anzeljg_> rkabalin: guess you can't do that also...
21:17:25 <rkabalin_> dobedobedoh might be able, but he is not here ))
21:17:42 <anzeljg_> anitsirk is having ops privileges I guess...
21:18:42 <rkabalin_> ah, I see you began meeing being anzeljg
21:18:49 <anzeljg_> yep
21:18:50 <rkabalin_> and now you are anzeljg_
21:18:57 <anzeljg_> yep again
21:19:10 <rkabalin_> any chance you can close that session
21:19:18 <anzeljg_> can i change my nick back to anzeljg
21:19:34 <rkabalin_> and connect again using original nick?
21:19:50 <aarow> #endmeeting
21:20:06 <rkabalin_> anzeljg_ try to leave and connect again
21:20:15 <anzeljg_> ok. will try
21:21:02 <rkabalin_> hello anzeljg
21:21:02 <anzeljg> finally...
21:21:08 <anzeljg> #endmeeting