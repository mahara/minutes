08:01:15 <aarowlaptop> #startmeeting
08:01:15 <maharameet> Meeting started Thu Sep  5 08:01:15 2013 UTC.  The chair is aarowlaptop. Information about MeetBot at http://wiki.debian.org/MeetBot.
08:01:15 <maharameet> Useful Commands: #action #agreed #help #info #idea #link #topic.
08:01:31 <aarowlaptop> Greetings one and all, welcome to the 27th Mahara dev meeting
08:01:42 <aarowlaptop> Please introduce yourself using the #info tag
08:01:49 <anitsirk> #info anitsirk is Kristina Hoeppner, Catalyst IT, Wellington, NZ
08:02:03 <aarowlaptop> #info aarowlaptop is Aaron Wells at Catalyst IT in Wellington, NZ
08:02:30 <robertl_> #info robertl_ is Robert Lyon, Catalyst IT in Wellington, NZ
08:02:47 <dobedobedoh> #info dobedobedoh is Andrew Nicols at Lancaster University, UK (just)
08:02:55 <sonn_> #info sonn_ is Son Nguyen at Catalyst IT in Wellington, NZ
08:03:21 <anitsirk> hi dobedobedoh. had feared we'd not have one of you guys here.
08:03:38 <dobedobedoh> I won't be so helpful for channel ops - I'll be in Perth in 3 weeks
08:03:52 <anitsirk> to visit or to stay?
08:04:06 <dobedobedoh> I'm moving to Perth to work for Moodle HQ
08:04:21 <anitsirk> congrats! i can visit you then in january during lca 2014 :-)
08:04:29 <dobedobedoh> cool :)
08:04:39 <aarowlaptop> Well, let's proceed
08:04:40 <anitsirk> i.e. you could come to the conference as well...
08:04:44 <aarowlaptop> #topic Items from last meeting
08:05:21 <aarowlaptop> #info to see about changing the channel operators on the #mahara-dev IRC room, and running his own copy of meetbot
08:05:34 <aarowlaptop> I have not done either of those :)
08:06:21 <aarowlaptop> But now that I know that dobedobedoh runs the meetbot, I at least know to check with him a day or so before a meeting to make sure it's up and running
08:06:47 <dajan> #info dajan - Dominique-Alain Jan coordinator of the francophone community
08:07:12 <aarowlaptop> #action Aaron to see about changing the channel operators on the #mahara-dev IRC room
08:07:19 <anitsirk> hi dajan
08:07:20 <aarowlaptop> I'll try to get that done before the next meeting
08:07:23 <aarowlaptop> Hi dajan!
08:07:33 <sonn_> Hi dajan
08:07:36 <anzeljg> hi
08:07:45 <dajan> congratulation dobedobedoh
08:07:52 <dajan> Hi to all
08:08:04 <anitsirk> dobedobedoh and i chatted a bit whether i could change the chanops, but since i moved to bip, i had trouble getting my nick password and the one i think i have won't be taken. sorry that i can't help. :-(
08:08:19 <anitsirk> hi anzeljg. great that you could make it.
08:08:51 <anzeljg> anitsirk hi ;)
08:08:53 <aarowlaptop> Hi anzeljg, can you introduce yourself with #info for the record? :)
08:09:25 <anzeljg> #info anzeljg is Gregor Anzelj, translator & developer from Ljubljana, Slovenia
08:09:37 <aarowlaptop> thanks
08:10:07 <aarowlaptop> okay, next topic
08:10:22 <aarowlaptop> #topic Aaron to discuss switching to "asynchronous meetings"
08:10:59 <dajan> As suggested I have revamp the Developer Area/Developer Meetings page on the wiki, to have the last dev meeting first. ½ hour of happy copy paste.
08:11:00 <aarowlaptop> It was just something I was wondering about, because it seemed the turnout had been pretty low the past few dev meetings, whether we should continue with them. Although, now that I'm looking through the historical minutes I can see that we're more or less within the average
08:11:05 <rkabalin> #info kabalin is Ruslan Kabalin, Lancaster University, Lancaster, UK
08:11:15 <aarowlaptop> oh, that's who did that, thanks dajan
08:11:16 <dajan> Hi Ruslan
08:11:20 <dajan> Hi Greg
08:11:33 <sonn_> Hi Ruslan
08:12:42 <rkabalin> Hi everyone, sorry for being late :)
08:13:01 <anitsirk> aarowlaptop: do you still suggest moving to asynchronous meetings then or see how it goes?
08:13:02 <aarowlaptop> So anyhow, I think we're fine continuing with the IRC dev meetings as before
08:13:11 <aarowlaptop> I think we can keep with the status quo
08:13:22 <robertl_> that sounds good to me
08:13:42 <dobedobedoh> We could reduce the frequency to 6 weekly?
08:13:47 <aarowlaptop> we perhaps just need to be more assiduous about getting them regularly scheduled, and getting the announcements out
08:14:00 <anitsirk> #info Keep developer meetings synchronously in irc.
08:14:01 <dobedobedoh> I only remembered today because I saw anitsirk 's tweet
08:14:19 <anzeljg> 6 weekly sounds fine. and announcements would be much apreciated...
08:14:39 <dajan> Maybe advertising them on a wider scale with other tools (e.g. twitter, G+,…)
08:14:43 <aarowlaptop> it's too bad Mahara doesn't have an event calendar to send out reminder emails
08:14:48 <anitsirk> normally, a reminder is sent the week before and then the day before. we missed this this time due to lots of work. sorry about that.
08:15:03 <anitsirk> dajan: i always tweet about them.
08:15:14 <dajan> Sorry.
08:16:10 <anitsirk> and if i remember i also put an announcement in the mahara user group on FB. not this time because most people there are in the usa fast asleep when we made the announcement.
08:16:36 <aarowlaptop> they're just not dedicated enough ;)
08:16:41 <dajan> On my side I will advertise more about them to french dev in Switzerland, France and Canada. I have to put this in my calendar and do it.
08:16:52 <anitsirk> cool.
08:17:38 <anitsirk> aarowlaptop: wanna move on to the next topic?
08:17:44 <aarowlaptop> I was just about to suggest that
08:17:45 <dobedobedoh> We ahve FB?
08:18:00 <dobedobedoh> Should we make the meetings less developer-focused?
08:18:04 <aarowlaptop> Do we have an official Facebook feed?
08:18:19 <anitsirk> dobedobedoh: there are a few FB groups / pages: mahara user group, the maharamoot de and the mahara page now as well. then there is the moodle-mahara meetup (down under)
08:18:20 <aarowlaptop> #idea Should we make the meetings less developer-focused?
08:18:45 <aarowlaptop> and there's the LinkedIn page too, right?
08:18:45 <anitsirk> the mahara user group has infrequent meetings in a webinar setting.
08:18:53 <anitsirk> yep. linkedin, too.
08:19:06 <aarowlaptop> what do they talk about at the Mahara User Group?
08:19:20 <dajan> Aaron, I don't think so. The forums on Mahara.org are already here for users. I don't think that IRC is something "lambda" users will engage, really.
08:19:33 <anitsirk> in the beginning, we wanted to have the dev meetings dev focused so that you devs could talk about tricky tech questions. but i guess that hasn't happened much.
08:20:25 <aarowlaptop> We do occasionally use them to discuss what direction we should go with the API's and stuff
08:20:28 <anitsirk> aarowlaptop: primarily implementation at institutions, examples, last year we had a student showcase; new features, potential collaborations (only started)
08:20:35 <aarowlaptop> but it does seem to mostly be about discussing community rules
08:20:50 <aarowlaptop> and community infrastructure
08:21:33 <aarowlaptop> I agree with dajan, though, end-users don't really use IRC
08:21:36 <anitsirk> the meetings are open for anyone to join. same with the MUG meetings
08:21:53 <robertl_> could there be say one or two meetings a year where the focus is brainstorming new features?
08:22:20 <robertl_> where all the focus is to bring new ideas to the meeting?
08:22:25 <anzeljg> great idea!
08:22:27 <dajan> Interesting, Robert, +1
08:23:09 <dajan> #idea one or two meetings a year where the focus is brainstorming new features
08:23:18 <dobedobedoh> Perhaps we could also do the occasional hangout?
08:23:28 <anitsirk> you mean as in google?
08:23:30 <dobedobedoh> anitsirk: yup
08:23:42 <dajan> Kirisina is not very keen of using G+
08:23:57 <aarowlaptop> and I don't actually have a webcam ;)
08:23:57 <dobedobedoh> Okay then, bbb
08:23:58 <dajan> I respect this
08:24:00 <anitsirk> it's a bit limiting because you need to have a G+ account - it excludes people more than irc does i think.
08:24:27 <anitsirk> bbb is a good idea :-) would need to do a bit of testing the connectino beforehand but that would be good to use
08:24:29 <aarowlaptop> Well, I think more people probably have Gmail or G+ accounts, than know how to use IRC
08:24:39 <anitsirk> we do have an instance to test with.
08:24:46 <dobedobedoh> I just wonder whether once in a while, it would be helpful to meet people a little more in the flesh
08:24:59 <anitsirk> yep.
08:25:08 <aarowlaptop> I do see that, meeting people "in person", as one of the main goods of the dev meetings, actually
08:25:11 <anitsirk> that's what the mug meetings do. they use webinar software.
08:25:40 <aarowlaptop> #idea Occasional dev meetings that use Google Hangouts or BBB or other webinar software, rather than just IRC
08:26:22 <aarowlaptop> okay, let's move on to the next topic :)
08:26:23 <dobedobedoh> aarowlaptop: Can you add robertl_ 's idea about a different focus for some meetings?
08:26:37 <anitsirk> dajan: you already did that
08:26:38 <dajan> So lets organise the next dev meeting in NZ. I offer the beers.
08:26:40 <aarowlaptop> dajan got that one already :)
08:26:50 <dobedobedoh> ah okay, sorry.
08:27:02 <dobedobedoh> I'm game. Wellington isn't so far from Perth in the grand scheme of things
08:27:02 <aarowlaptop> well yeah, free beer for anyone who attends a dev meeting in New Zealand, that's a given. ;)
08:27:18 <robertl_> indeed it would be
08:27:28 <dobedobedoh> I can confirm the presence of free beer at such meetings I have attended
08:27:28 <aarowlaptop> free virtual beer for those abroad
08:28:01 <dobedobedoh> ANyway, moving on ;)
08:28:08 <aarowlaptop> #topic Dajan improvements to the Mahara admin interface for installing/deleting plugins
08:29:03 <dajan> Yes. I was on it. But I had to use my time to focus on the CAS development. So this task is on hold at that time. I will work on it in October.
08:29:34 <aarowlaptop> #action Dajan to discuss improvements to the Mahara admin interface for installing/deleting plugins
08:29:36 <anitsirk> aarowlaptop: instead of making this an action item, we should add it to the todo list on the wiki. i'll find the page
08:29:40 <aarowlaptop> #undo
08:29:40 <maharameet> Removing item from minutes: <MeetBot.items.Action object at 0x2c3e310>
08:29:47 <dajan> About CAS, I am negotiating with the University of Troyes and Strasbourg to find the needed funding to develop and put CAS in core.
08:29:54 <aarowlaptop> excellent
08:30:22 <anzeljg> what is CAS if I may ask?
08:30:34 <aarowlaptop> It's a SSO standard
08:30:39 <anitsirk> #link added to https://wiki.mahara.org/index.php/Developer_Area/Current_Tasks
08:30:44 <aarowlaptop> There's a third-party plugin for it in Mahara currently
08:30:55 <dajan> I will come back to you soon to discuss about cost/blue print, etc. But it takes time to meet all the people who can decide about money in France….
08:31:08 <aarowlaptop> by Patrick Pollet https://github.com/patrickpollet/mahara_plugin_auth_cas
08:31:36 <dajan> Yes. the third-party is Pollet's one. And it would be used as dev basis, I think.
08:31:58 <dajan> More detail about CAS later this month, I hope
08:32:03 <aarowlaptop> Yep, as we discussed when dajan was visiting here, it's about 90% of the way there
08:32:10 <anitsirk> fantastic
08:32:46 <aarowlaptop> #info dajan negotiating funding for the development work to get Patrick Pollet's CAS plugin into core
08:32:49 <dajan> Both universities agreed to pay for it… So just the time to finalise the blue print...
08:33:20 <aarowlaptop> #link CAS plugin https://github.com/patrickpollet/mahara_plugin_auth_cas
08:33:54 <aarowlaptop> Okay, I think we can move on the next topic then
08:34:04 <aarowlaptop> #topic Aaron: Exceptional cases when an approved code reviewer may push code directly without another reviewer looking at it
08:34:53 <anitsirk> oh hi iarenaza :-)
08:35:12 <iarenaza> hi, just show your tweet about the meeting :-)
08:35:13 <dajan> Hi iarenaza
08:36:05 <iarenaza> I'm not much into Mahara devel these days but I thought I could pass by and have a peek
08:36:11 <iarenaza> :-)
08:36:18 <robertl_> aaron, there would need to be some solid guidelines around this otherwise it could get messy
08:36:18 <anitsirk> you are always welcome, iarenaza
08:36:40 <aarowlaptop> correct, robertl_, back to the topic ;)
08:37:02 <anzeljg> iarenaza hi
08:37:51 <aarowlaptop> I've been admittedly cutting corners on the review process, occasionally pushing certain types of commits without getting a +2 on them from another reviewer in gerrit. Kristina thought it would be a good idea to go over this in the dev meeting.
08:37:57 <aarowlaptop> The situations would be:
08:38:33 <aarowlaptop> #info 1. Cherry-picking of already approved commits, that touch relatively few lines of code, to other supported branches (if the cherry-pick applies cleanly)
08:39:05 <aarowlaptop> #info 2. Patches that only make changes to comments or documentation files but do not touch executable Mahara code
08:40:02 <aarowlaptop> #info 3. (Possibly) changes to development tools, like the pre-commit hooks and Makefiles
08:40:03 <rkabalin> those two sounds good to me
08:40:47 <aarowlaptop> #info 4. Patches to revert already-approved patches which have caused major regressions
08:41:02 <aarowlaptop> So, those would be the four situations
08:42:16 <robertl_> with point 1. would the few lines that change need to be all in the one file or can it be across multiple files?
08:42:19 <anitsirk> I'm not an approved reviewer, but they sound ok to me because they are not pushing new bug fixes or new features through without someone else having looked at them first.
08:43:27 <aarowlaptop> I'd say across multiple files is fine. In practice, since we're only supposed to backport bug fixes and security fixes to the stable branches, not new features, the patches that are being cherry-picked to them will usually be pretty small anyway
08:44:13 <anitsirk> should "relatively few lines" be defined a bit more closely then as "relatively" is very interpretable?
08:44:14 <aarowlaptop> Though admittedly, item #1 there is the riskiest of them. Even when something applies cleanly, there's always the possibility that it will cause problems.
08:44:53 <anitsirk> we could add that the dev needs to verify it for sure and not only apply the patch.
08:45:15 <aarowlaptop> No, I think it'd be best to leave it up to just developer judgement as to what is "minor" versus "major".
08:46:24 <aarowlaptop> I think that's fair, every patch should be verified, even if it's just by the dev who pushes it
08:46:58 <aarowlaptop> #info In the case of Item 1, the cherry-picked patches should be verified (this can be by the dev who is pushing them)
08:47:02 <rkabalin> I agree re dev judgement
08:47:34 <anitsirk> does anyone not agree with any of the suggestions?
08:47:53 <aarowlaptop> Actually I'm thinking of rescinding #3, about the dev tools :)
08:48:35 <rkabalin> #agree
08:48:41 <aarowlaptop> but other than that, I agree with #s 1, 2, and 4
08:49:29 <rkabalin> I agree with all of them, given that developer may decide on his/her own whether verification is required
08:49:30 <sonn_> #agreed
08:49:57 <sonn_> with all of them
08:50:03 <anitsirk> since we have 5 approved devs in the room, well 5 if robertl omes back, we can put it forward to vote on and make it a guideline.
08:50:15 <anitsirk> sonn_: 1,2 and 4 or 1, 2, 3 and 4?
08:50:40 <dajan> I agree
08:50:54 <anitsirk> rkabalin: i think verification would be required for all of them, esp. #1 at least a smoketest.
08:51:14 <sonn_> I agree with all of them 1, 2, 3, and 4
08:51:20 <dobedobedoh> 1,2,5 for me
08:51:27 <dobedobedoh> 1,2,4 even
08:51:36 <anitsirk> what's #5, dobedobedoh ?
08:52:10 <aarowlaptop> I believe that's a typo ;)
08:52:13 <dobedobedoh> 5 = anything minor against code which is unit tested and causes no regressions ;)
08:52:31 * dobedobedoh adds things to the mix
08:52:34 <anitsirk> do we have anything like that?
08:52:41 <dobedobedoh> We have a unit testing framework, but no tests
08:52:47 <aarowlaptop> lol
08:52:56 <dobedobedoh> We could do with some tests (please)
08:52:56 <aarowlaptop> well, it's a start
08:53:11 <anitsirk> it's on the backlog https://wiki.mahara.org/index.php/Developer_Area/Current_Tasks
08:53:14 <dobedobedoh> I havent' checked it still works, but phpunit is there ish
08:53:26 <anitsirk> welcome back, robertl_
08:53:39 <dobedobedoh> But, in reality, my preference is really only #5, but since we don't have that yet, 1,2,4 will suffice
08:53:44 <robertl_> had to reset router, sorry bout that
08:53:52 <dobedobedoh> Sorry, 4 and 5 maybe
08:54:49 <aarowlaptop> So what do you say, robertl? cherry-picks, non-code changes, and emergency reverts?
08:54:51 <anitsirk> dobedobedoh: wouldn't we then actually also need automated functional testing?
08:55:43 <robertl_> cherry-picks that merge cleanly sound fine and non code changes
08:55:58 <anitsirk> i've counted a couple that were in favor of #3 as well. so it might be easiest to look at each item individually and tally the agrees
08:56:07 <robertl_> emergency reverts in some situations will be fine
08:56:15 <dobedobedoh> anitsirk: yes
08:56:38 <anitsirk> dobedobedoh: yes to what? my last or second last line? :-)
08:57:00 <anitsirk> or both?
08:57:06 <dobedobedoh> anitsirk: yes to automated functional testing
08:57:13 <dobedobedoh> but also yes to tally
08:58:40 <anitsirk> aarowlaptop: do you want to do the tallying? you have the power to give the final numbers as chair and use the #agreed (if i remember correctly)
08:58:50 <aarowlaptop> please do, anitsirk
09:00:02 <anitsirk> so it's only approved reviewer's that are counted, right?
09:00:53 <aarowlaptop> I don't really know if we have a rule about these things, to be honest
09:02:20 <anitsirk> i only know that approved reviewers can vote others to become approved, but don't remember that we had a situation like this here.
09:03:04 <aarowlaptop> Looking back at the minutes, we have three devs voting for my new proposal of #124, and two devs voting for #1234. I think it'd be fair to say that numbers #124 are approved, because they have 5/5 approval, while #3 is not approved at this time because it only has 2/5
09:03:34 <dobedobedoh> #agree
09:03:39 <aarowlaptop> #agreed
09:04:02 <anitsirk> with #1 caveat of verification by the dev
09:04:52 <anitsirk> but other than that, that was also my count.
09:05:34 <anitsirk> sonn_, robertl_, rkabalin: do you want to add anything?
09:06:04 <aarowlaptop> #agreed: +2 Reviewers can commit things without getting review from others in the following scenarios: cherry-picking already-reviewed, cleanly applied, patches from one branch to another (though they should verify them on the target branch); changes to non-code files and to comments; Reverts to already-approved patches that caused major regressions
09:06:50 <aarowlaptop> Well, we're about at an hour now. We've got one more topic
09:07:01 <aarowlaptop> #topic Update on the 1.8 release (Kristina)
09:07:43 <sonn_> no
09:07:56 <anitsirk> i just wanted to briefly update on the 1.8 release. there are still a number of things in gerrit that need to be code reviewed and verified before we can bring out RC1. it would be great if as many people as possible could pick a few things and go through the reviews.
09:08:36 <robertl_> that would be helpful, as more eyes on things find more mistakes :)
09:08:41 <anitsirk> we do intend to release RC1 as quickly as possible, but the big features need to make it in first and then i'll need to go over the lang files. the themes are pretty much done. we'll just move some items around in the artefact chooser and group them a bit better.
09:09:42 <anitsirk> we still intend to release mahara 1.8 stable as quickly after the RC but also need people to test the RC. it would be great if you could publicize the RC once it's out in your networks so we can get as many people to test.
09:10:35 <anitsirk> one of the big features, flexible layouts, has been causing a number of regressions because it touches so many things that we didn't see straight away. i think robertl_ and aarowlaptop know that bug number in their sleep. having people test the front end would already be a great help.
09:11:15 <anitsirk> we'll be updating master.dev with the RC so that users wouldn't have to install a site in order to do some testing. but of course, full installations / upgrades would also be great.
09:11:20 <anitsirk> ..
09:12:24 <anitsirk> #info Mahara 1.8 will have a lot of new features and lots of new Javascript that can cause a number of regressions and sticky points. Anyone who is interested in testing even only one feature or a work flow would be adding a lot of value to making the stable release a good one.
09:12:24 <aarowlaptop> thanks Kristina!
09:12:48 <aarowlaptop> if no one's got any questions, we'll move on to the final item, scheduling the next meeting
09:13:26 <anzeljg> Page skins?
09:13:40 <dajan> When? Page skins.
09:13:51 <anitsirk> anzeljg: all of the above apply: the more testers, the better we'll see what still needs fixing.
09:14:22 <anitsirk> we will need to bring out the RC very soon (ideally next week) in order to meet the release time frame of mid october.
09:14:45 <dajan> Do you have put page skins in 1.8 dev? I haven't found them yet.
09:14:47 <aarowlaptop> anzeljg: page skins are still slated for inclusion
09:14:54 <aarowlaptop> but it's still in code review
09:14:54 <anitsirk> dajan: they are still in gerrit
09:15:08 <aarowlaptop> I've been meaning to finish that code review for a while now, sorry
09:15:57 <aarowlaptop> I'll also be writing up a patch to let institutions turn the feature on or off for their own users' pages
09:16:33 <anzeljg> I see... hopefully it'll make it into 1.8
09:16:46 <aarowlaptop> yep, we're still aiming to get it in 1.8
09:16:55 <anzeljg> nice
09:17:24 <aarowlaptop> Evonne our designer hates the idea of letting end users set their own CSS for the page ;)
09:17:42 <aarowlaptop> but we've been ignoring her on this ;)
09:18:04 <anitsirk> because some people will create fantastic skins
09:18:37 <anzeljg> :)
09:19:09 <aarowlaptop> #topic Deciding the time and chair for the next meeting
09:19:10 <rkabalin> disabling custom css could be a theme setting
09:19:11 <dajan> Ok everybody is against Evonne. Poor Evonne
09:19:28 <aarowlaptop> A web designer's life is hard
09:19:29 <robertl_> so are skins and resume attachments the biggest of the items still to be reviewed?
09:19:36 <aarowlaptop> Yes, I believe so
09:19:41 <anitsirk> only on this one, dajan. not on the rest which is the majority :-)
09:19:53 <rkabalin> e.g. for some really nice theme, custom css could be disabled
09:19:55 <aarowlaptop> oh, and leap2a import into existing user accounts
09:19:58 <dajan> ;)
09:19:58 <anitsirk> robertl_: and the leap2a import stuff
09:20:02 <aarowlaptop> although I don't think that touches as much code
09:20:47 <aarowlaptop> So the next meeting will be at UTC 1600 (NZ morning, UK evening, US afternoon)
09:21:01 <anzeljg> EU evening ;)
09:21:11 <anitsirk> aarowlaptop: do you mean to say that this would be 4 a.m.?
09:21:36 <aarowlaptop> erm, maybe I mean 2000, not 1600
09:21:48 <aarowlaptop> anyway, how does this sound: http://www.timeanddate.com/worldclock/fixedtime.html?iso=20131003T20&p1=1440
09:21:52 <anitsirk> thanks. that's better :-)
09:21:56 <robertl_> that's better for me :)
09:22:01 <aarowlaptop> 3 Oct, 8pm UTC
09:22:07 <rkabalin> good
09:22:14 <anitsirk> that won't work for me. the eportfolio forum is on at that time. i'll be away.
09:22:15 <dajan> +1
09:22:22 <anzeljg> ok
09:22:51 <anzeljg> anitsirk: a day before?
09:23:04 <robertl_> +1
09:23:04 <anitsirk> same thing. unless the hotel wifi is decent.
09:23:13 <aarowlaptop> We could push it out a week
09:23:17 <aarowlaptop> 10 Oct, 8pm UTC
09:23:28 <anzeljg> +1
09:23:36 <anitsirk> ignore me for that october meeting then i guess because i'll also be at a conf. 10-11
09:23:44 <robertl_> +1 for that time too
09:23:57 <anitsirk> same thing: if hotel wifi is ok, i can participate.
09:24:08 <anitsirk> (or 3g)
09:24:16 <dajan> LTE?
09:24:19 <aarowlaptop> anitsirk: is 3 Oct as good as 10 Oct for you, then?
09:24:47 <dajan> 10th is fine on my side too
09:24:53 <anitsirk> 3rd oct. might then actually be a bit better because that'll be 6 a.m. and i could 1.5 hours more easily than on the 11th as that would go into starting time of the conf.
09:25:23 <anzeljg> RC1 should be out by Oct 3rd, right?
09:25:44 <anitsirk> anzeljg: if we get enough help with the reviews...
09:25:56 <anzeljg> yeah, I know...
09:26:05 <aarowlaptop> okay then
09:26:25 <aarowlaptop> #agreed The next dev meeting will be on 3 Oct, 8pm UTC
09:26:27 <aarowlaptop> #link http://www.timeanddate.com/worldclock/fixedtime.html?iso=20131003T20&p1=1440
09:26:34 <aarowlaptop> Who wants to chair?
09:26:48 <rkabalin> Me
09:26:55 <anitsirk> thanks for volunteering
09:26:59 <rkabalin> np
09:27:09 <aarowlaptop> #action Ruslan to chair the next dev meeting
09:27:15 <dajan> Once I would do it, but I would have to learn how to do it first...
09:27:25 <rkabalin> :)
09:27:30 <anitsirk> dajan: you learn while you are the chair for the first time ;-)
09:27:39 <anzeljg> exactly!
09:27:48 <aarowlaptop> dajan: I just expanded the wiki page about it today ;) https://wiki.mahara.org/index.php/Developer_Area/Developer_Meetings/Chair_Duties
09:27:49 <dajan> Learning by doing… the history of my life
09:28:00 <aarowlaptop> okay, last topic then
09:28:11 <aarowlaptop> #topic Any other business?
09:28:14 <rkabalin> yes
09:28:19 <anzeljg> yep
09:28:23 <anitsirk> no
09:28:28 <dajan> no
09:28:33 <aarowlaptop> maybe
09:28:42 <rkabalin> lol
09:28:42 <dajan> but ruslan has?
09:28:54 <rkabalin> right
09:28:55 <anitsirk> rkabalin: go ahead
09:29:00 <aarowlaptop> rkabalin: You said yes first, go ahead. ;)
09:29:18 <rkabalin> Coding guidelines require some update
09:29:29 <rkabalin> I have outlined changes
09:29:33 <rkabalin> #link https://wiki.mahara.org/index.php/Talk:Developer_Area/Coding_guidelines
09:29:40 <anitsirk> #info the coding guidelines require updating
09:29:55 <rkabalin> if everyone happy, I will add them to wiki
09:29:56 <anitsirk> #idea rkabalin outlined changes at https://wiki.mahara.org/index.php/Talk:Developer_Area/Coding_guidelines
09:31:17 <anitsirk> rkabalin: the whitespace thing is a bit tricky for third-party code. maharabot always complains. i think there was talk of finding a way to ignore those things.
09:31:19 <robertl_> they seem like good guidelines to me
09:31:24 <rkabalin> you are welcome to ask questions here or in the wiki talk, or may be suggest something else
09:31:31 <aarowlaptop> I agree with everything except for requiring no whitespace on the end of lines
09:32:28 <robertl_> it's good not to have the extra whitespace but it doesn't break code or ability to read it
09:33:16 <aarowlaptop> Well, speaking of third-party code, we have been following a de facto policy of *not* applying the coding standards to third-party libraries that are brought in complete
09:33:28 <robertl_> true
09:33:53 <aarowlaptop> which I guess is not actually specified in the coding guidelines... perhaps it should be
09:34:19 <rkabalin> in general it is not a good style to have trailing whitespaces in the code
09:34:37 <rkabalin> obviously it does not affect the ability to read it
09:34:58 <robertl_> so for things like jquery or adodb that are well established libraries it not necessary to apply the coding guidelines
09:35:18 <rkabalin> I agree regarding third-party stuff
09:35:31 <aarowlaptop> I agree that it's preferrable to clear out whitespace because it can make comparisons using automated tools marginally more difficult, but these days most tools (including reviews.mahara.org) have the ability to ignore whitespace
09:35:50 <aarowlaptop> which is why I'd prefer to take a relaxed attitude about it, and not reject patches solely because of their whitespace
09:36:35 <aarowlaptop> but just leave it as a recommendation
09:36:45 <rkabalin> aarowlaptop, when you run diff, red bits at the end do not irriate you then :)
09:36:51 <aarowlaptop> diff -w
09:37:03 <aarowlaptop> I habitually do "git diff -w"
09:37:12 <aarowlaptop> I actually don't know if normal diff has a "-w" flag...
09:37:37 <robertl_> so it would be more like "you need to fix this bug here, and while your at it there are whitespace errors to attend to"?
09:37:53 <robertl_> rather than solely rejecting on whitespace?
09:38:03 <rkabalin> ok, let us leave it as recommendation
09:38:27 <aarowlaptop> While we're talking about coding guidelines, how do people feel about the "cuddled else" rule?
09:38:50 <rkabalin> I like it
09:38:51 <aarowlaptop> I think, if I could change one thing in the Mahara coding guidelines, it would be to allow } else {
09:39:35 <rkabalin> we did that for ages, allowing } else { will intorduce inconsistency
09:39:36 <sonn_> I like cuddled else
09:40:03 <aarowlaptop> It is true that it would cause inconsistency because uncuddled elses are everywhere in the code
09:40:05 <sonn_> it makes more clear for me
09:40:28 <robertl_> I prefer the non cuddled else myself
09:40:45 <aarowlaptop> okay, we'll leave it as is
09:41:38 <rkabalin> ok, I will then update guidelines
09:42:00 <rkabalin> keeping whitespace rule as recommended
09:42:09 <rkabalin> that is it from me
09:42:12 <aarowlaptop> And anzeljg, you had other business as well?
09:42:28 <dajan> do you put an action tag for ruslan?
09:42:53 <anzeljg> I've finhished Profile completeness / progress bar blocktype plugin as discussed in https://mahara.org/interaction/forum/topic.php?id=5649
09:43:03 <anzeljg> see: https://twitter.com/anzeljg/status/375481998074068992/photo/1
09:43:10 <anzeljg> just to let you know...
09:43:20 <anzeljg> that's it from me
09:43:23 <rkabalin> cool
09:43:25 <anitsirk> that is cool :-)
09:43:29 <sonn_> kool
09:43:31 <rkabalin> thanks anzeljg
09:43:34 <dajan> Nice
09:43:44 <dajan> I mean super great.
09:43:47 <robertl_> cool
09:43:55 <anitsirk> anzeljg: did you also publish the other very cool plugin for the cookies?
09:44:16 <aarowlaptop> #info Gregor has completed a profile completeness block
09:44:17 <anzeljg> anitsirk: sorry none of them isn't published yet
09:44:27 <aarowlaptop> #link https://twitter.com/anzeljg/status/375481998074068992/photo/1
09:44:56 <anzeljg> do i add the links into wiki -> plugins? or will you consider this to make it into core and I upload it to gerrit?
09:45:13 <rkabalin> I have leave and go to meeting, thanks everyone, thanks aarowlaptop for chairing
09:45:23 <aarowlaptop> See you later!
09:45:29 <anitsirk> anzeljg: i think for the time being adding it to the plugins would be great for people who want to use it now. but we can look into it for 1.9
09:45:38 <anzeljg> the other plugin that anitsirk was mentionig was implementation of Cookie Consent plugin for mahara
09:45:45 <aarowlaptop> I was about to say the same thing as anitsirk
09:45:55 <anzeljg> see: http://silktide.com/cookieconsent
09:46:00 <anitsirk> i think having the cookie one in core would definitely be useful and i think the completeness block as well
09:46:25 <anitsirk> and they shouldn't be huge things like skins or flexible page layouts ;-)
09:46:41 <anzeljg> ok. I'll put them into plugins wiki for time beeing...
09:46:55 <aarowlaptop> indeed, I imagine as block plugins they're probably pretty self-contained
09:47:07 <robertl_> with the profile completeness are the options for completeness selectable? or is the list fixed
09:47:11 <anitsirk> anzeljg: i think people would like to use them already now with 1.7 / 1.8
09:47:26 <anzeljg> the list is fixed for now
09:47:44 <robertl_> ok
09:47:49 <anzeljg> basically most of the stuff from profile and resume - profile photo
09:49:00 <dajan> I am already listening to people at Alumni or staff at students' placement asking for more flexibility, asking for choosing the items they want...
09:49:09 <anitsirk> let's look at it once 1.8 is out :-)
09:49:27 <anzeljg> ok
09:49:59 <anitsirk> does anybody else have other business?
09:50:20 <anzeljg> I was also thinking about implementing support for social websites (twitter, FB, etc.) in a way that the user will add those url address similar to adding multiple email addresses...
09:50:31 <anzeljg> but after 1.8 gets out.
09:50:31 <anzeljg> ..
09:50:48 <anitsirk> yes, instead of AIM and MSN messenger ;-)
09:50:56 <dajan> Have to go now. Already noon. Gosh how time passes by with good friend. anitsirk: when is the due date for the next newsletter?
09:51:09 <anitsirk> dajan: 28 september
09:51:24 <anitsirk> i'll send out a CFP
09:51:30 <dajan> Ok I will write something about: http://vimeo.com/channels/eportfolios
09:51:44 <aarowlaptop> Alright, I declare this meeting complete!
09:51:47 <aarowlaptop> #endmeeting