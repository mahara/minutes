08:04:09 <anitsirk> #startmeeting
08:04:09 <maharameet> Meeting started Wed Jan 16 08:04:09 2013 UTC.  The chair is anitsirk. Information about MeetBot at http://wiki.debian.org/MeetBot.
08:04:09 <maharameet> Useful Commands: #action #agreed #help #info #idea #link #topic.
08:04:10 <anitsirk> Welcome to the 23rd Mahara Developer Meeting and a happy new year. Please state your name and location by starting a line with #info. And please keep in mind that when you have a long statement broken up over several lines, please put .. on a separate line when you are done.
08:04:11 <anitsirk> #info anitsirk is Kristina Hoeppner, Catalyst IT, Wellington, NZ
08:04:23 <anzeljg> #info anzeljg is Gregor Anželj, developer and translator, Ljubljana, Slovenia
08:04:42 <aarow> #aarow is Aaron Wells, Catalyst IT, Wellington NZ
08:05:05 <aarow> #info aarow is Aaron Wells, Catalyst IT, Wellington NZ
08:05:17 <elky> #info elky is melissa draper, catalyst it, wellington nz
08:05:19 <VolkerJSchmidt> #info Volker Schmidt, Mediasmederij, Arnhem, Netherlands
08:05:35 <anitsirk> i guess that's us for the time being.
08:05:37 <anitsirk> #topic Items from last meeting
08:05:38 <anitsirk> There were no action items or agenda items to take up again this time. Thus, we can move on to our first new topic.
08:05:46 <anitsirk> #topic Feature freeze for Mahara 1.7 (elky)
08:06:22 <elky> aside: hugh thought the meeting was 9:30
08:06:35 <elky> so i annoyed him and texted
08:06:54 <aquaphone> Hi
08:07:05 <anitsirk> hi aquaphone.
08:07:10 <aquaphone> #info Hugh Davenport
08:07:11 <elky> ok, feature freeze is currently scheduled for the week 4th to 10th of February
08:07:40 <aquaphone> Sorry if I'm not quick. I'm at dinner
08:07:55 <anitsirk> #idea feature freeze scheduled for 4-10 February 2013
08:08:45 <anzeljg> It isprobably too late to try and squeze the skins feature in? If I make it untill the end of January?
08:08:47 <elky> And we're going to stick with that goal, but with a standing exception for one large feature which is borderline between feature and ui
08:09:18 <elky> but it's so big that it could cause problems if we leave it and push it in at the UI freeze date
08:09:46 <elky> so we're going to aim to get that out asap, but it can go in after the feature freeze if it's not quite ready
08:10:03 <anitsirk> it's the javascript things that sonn is fixing and that are in review waiting to be code reviewed.
08:10:22 <elky> feature freeze will probably happen on the wednesday or thursday of that week
08:11:27 <anitsirk> #info feature freeze will probably be on 6 or 7 February 2013.
08:11:33 <elky> aarow, is going to be doing the majority of the reviewing/verifying, but I'll have to give it a once-over to +2 and submit it
08:11:57 <anitsirk> and anybody else is welcome to test (verify from the frontend) and do code review.
08:12:04 <elky> yes, more eyes the better
08:12:17 <anitsirk> elky: anzeljg had a question earlier
08:12:58 <elky> anzeljg, is there somewhere i can see what the changes involve?
08:13:17 <anzeljg> Im in the middle of the rebase phase?!?
08:13:43 <anitsirk> anzeljg: do you still have a demo site with the code in action?
08:13:43 <elky> anzeljg, so it's feature complete and you're just cleaning up for submitting?
08:13:52 <anzeljg> Basically I tried to make the code as isolated as possible...
08:14:02 <elky> anitsirk, i want to see the code to judge, actually
08:14:21 <anzeljg> but there are places that the code needs to change the core code
08:14:22 <VolkerJSchmidt> Where could I test 1.7 from frontend? Or should I just download the branch from github?
08:14:34 <elky> i'm somewhat concerned it might conflict with sonn's work
08:14:45 <elky> VolkerJSchmidt, depends what you're aiming to test, specifically :)
08:14:48 <anitsirk> VolkerJSchmidt: you can set up your developer environment and test the patches. let me get the links
08:14:48 <anzeljg> there are the menues, and the page creating step for users to select skin over theme, etc
08:15:05 <elky> anzeljg, does it depend on javascript to do any part?
08:15:17 <anzeljg> elky: ok, so we'll leave it right after 1.7 release then...
08:15:38 <anzeljg> it depends on jquery to allow dynamical preview of a skin
08:15:44 <elky> anzeljg, send me an email with places i can look at the code and i'll make a judgement in the next few days
08:15:53 <anitsirk> #info: VolkerJSchmidt: everything you need for testing a patch that is currently in review (where it's best to test before it makes it into core) is at https://wiki.mahara.org/index.php/Developer_Area/Developer_Environment
08:16:10 <anzeljg> elky: never mind.... we'll do it more "peacefilly" for the 1.8
08:16:19 <VolkerJSchmidt> antisirk: thanks
08:16:22 <anzeljg> peacefully
08:16:22 <elky> anzeljg, that may not be an issue if you can save it and re-edit
08:16:38 <anzeljg> what do you mean sace it and re-edit
08:16:43 <anzeljg> save
08:17:13 <anzeljg> oh, you mean jquery?
08:17:27 <anitsirk> anzeljg: i would recommend to do it at the beginning of 1.8 because it is a huge feature. it will be great to finally implement, but it will need a lot of testing esp. now that we have device detection etc.
08:17:29 <elky> yes, the jquery preview isn't a functional necessity
08:18:05 <anzeljg> I actually agree with anitsirk, just thought to check and  remind you guys...
08:18:19 <anzeljg> and it is a huge feature.
08:18:22 <elky> ok, if you're happy, then that's what we'll do
08:18:35 <anzeljg> ok
08:18:36 <anitsirk> anzeljg: it hasn't left my mind, but i didn't want to ask too much for progress on it as i saw that you've work so much on the cloud plugin.
08:19:00 <anzeljg> yeah... and there were other things, we'll discuss later
08:19:16 <anitsirk> anzeljg: submit it really early after the 1.7 release so there is plenty of time to put it in
08:19:25 <anzeljg> ok
08:19:31 <elky> he can submit it any time and we'll just not merge it
08:19:39 <anzeljg> :)
08:19:40 <elky> that way we can be ready
08:20:00 <anitsirk> we will be rather busy until at least mid-year and thus getting this early will help to not have to rush them right before a deadline. ;-)
08:20:22 <anzeljg> I'll cake a little extra time, rebase it and re-check it and submit it after that.
08:20:45 <anzeljg> cake = take
08:20:47 <anitsirk> elky: i'm just worried that there might be a number of rebases necessary if it's just sitting in gerrit (or at least one) which might be time consuming to solve.
08:20:51 <anzeljg> oh those typos!
08:21:13 <anitsirk> anzeljg: if you could bake extra time in the form of a cake that would be wonderful.
08:21:33 <anzeljg> how do you want it delivered to NZ?
08:21:34 <anzeljg> ;)
08:21:35 <elky> anitsirk, we can always abandon the first set and have him re-submit it. but if we can see it in gerrit we can suggest things easier
08:21:58 <anitsirk> are there any other things to discuss for the feature freeze?
08:22:05 <anzeljg> yep
08:22:12 <anzeljg> speaking of javascript issues
08:22:39 <anzeljg> will this affect cloud plugin which uses javascript to display list of files/folders from cloud service?
08:22:43 <anzeljg> ..
08:22:57 <anitsirk> anzeljg: sonn is fixing the site so it can be used without javascript
08:23:03 <elky> anzeljg, plugins are use at your own risk
08:23:14 <anitsirk> see #link https://blueprints.launchpad.net/mahara/+spec/no-javascript
08:24:10 <elky> he has made some pieforms changes, they may interfere more than the lack of js
08:24:28 <anzeljg> oh?
08:24:40 <elky> we don't know the extent yet
08:24:44 <anitsirk> anzeljg: the code hasn't been merged yet. it's waiting to be reviewed.
08:24:53 <elky> anzeljg, the main thing is to ensure missing javascript doesn't stop the whole page working, or big things like that
08:25:20 <elky> you can sort out workarounds separately
08:25:26 <anitsirk> you can see all javascript fixes in review at #link https://reviews.mahara.org/#/q/status:open+project:mahara+branch:master+topic:jsbugs,n,z
08:25:40 <anzeljg> thanks, will have a look...
08:26:07 <anzeljg> elky, anitsirk: seems to me that we're only ones to do the talking today ;)
08:26:30 <elky> anzeljg, we'll move on to other things soon and see who is still awake :)
08:26:42 <anzeljg> :)
08:26:49 <anitsirk> anything else for the feature freeze? as elky said, the UI freeze will be soon thereafter. The schedule at #link https://wiki.mahara.org/index.php/6MonthlyCycle still holds.
08:26:52 <elky> i think that's all we need to discuss for the feature freeze
08:26:53 <anitsirk> anzeljg: ;-)
08:27:00 <anitsirk> alright. moving on then.
08:27:08 <anitsirk> #topic Employment history / Education history attachments (anzeljg)
08:27:19 <anitsirk> and here you get the floor for more, anzeljg. :-)
08:27:30 <anzeljg> Please beare with me, it will be quite a lot ov copy/pase...
08:27:37 <anzeljg> CEDEFOP is introductiong "Europass Skills Passport" (ESP) #link http://europass.cedefop.europa.eu/en/documents/european-skills-passport
08:27:44 <anitsirk> just do it all in one. ;-)
08:27:50 <anzeljg> as a supplement to "Europass CV". Uploading attachments will be a part of ESP's functionality. Their description:
08:27:52 <anzeljg> "Skill Passport is the root element of any Europass XML document. This element actually defines a person’s portfolio that includes
08:27:59 <anzeljg> information coming from the Curriculum Vitae, Language Passport or other Europass documents, as well other non-Europass documents."
08:28:09 <anzeljg> Non-Europass documents are the documents attached to ESP. Again the description ot this attachment list:
08:28:19 <anzeljg> "List any digital document (PDF, JPEG or PNG format) that an individual has attached to her Europass document to support/evidence
08:28:26 <anzeljg> of her personal data, learning achievements, work experiences, skills, competences, diplomas, etc."
08:28:33 <anzeljg> So my question is: Is it possible to extend Education/Employment history in such a way, that it would allow users to attach
08:28:40 <anzeljg> digital document(s) (PDF, JPEG or PNG) to each record in Education/Employment history?
08:28:49 <anzeljg> ..
08:28:55 <anzeljg> There is another question...
08:29:04 <anzeljg> Another question, closely related to personal information and new ESP: Is it possible to somehow change the user's address field?
08:29:09 <anitsirk> I'd love to see the ability to attach files to individual resume sections and the plans. makes a lot of sense.
08:29:10 <anzeljg> The address field in new Europass XML Schema v3.0 - currently the RC7 is published - is more detailed than textarea in Mahara.
08:29:17 <anzeljg> Mahara.
08:29:25 <anzeljg> #link https://joinup.ec.europa.eu/asset/europassxmlschemav3_0/asset_release/europassxmlschemav3_0-30-rc-7
08:29:31 <anzeljg> The address consists of several parts:
08:29:38 <anzeljg> #link http://europass.cedefop.europa.eu/xml/resources/EuropassAddressFormats_V1.0.xsd Description from that XSD file:
08:29:45 <anzeljg> "Defines a pattern to be followed when formatting the address.
08:29:50 <elky> anitsirk, was thinking the same thing. makes a lot of sense for recommendation letters and so forth
08:29:53 <anzeljg> s: street name and number
08:30:01 <anzeljg> p: country postal code prefix
08:30:07 <anzeljg> z: postal code
08:30:13 <anzeljg> m: municipality
08:30:21 <anzeljg> c: country
08:30:28 <anitsirk> #idea Extend the resume in Mahara to allow for uploading of files
08:30:31 <anzeljg> So is it possible to have those input text fields instead of textarea? How can it be achieved?
08:30:33 <anzeljg> ..
08:30:37 <anzeljg> finally ;)
08:30:54 <elky> anzeljg, i bet a bit of custom jquery could do that
08:31:21 <anzeljg> elky: jquery for address of attachments?
08:31:35 <elky> " to have those input text fields instead of textarea'
08:31:59 <anzeljg> and what? parse the resulat from DB?
08:32:35 <elky> maybe i'm not understanding the workflow
08:32:49 <anzeljg> it would be really nice, since it causes a lot of problems trying to export address info from Mahara and to make it accetable for EuropassWebService
08:32:55 <anitsirk> Why not just have extra input fields like other forms that ask for your address?
08:33:41 <anzeljg> anitsirk: yes, but currently we only have textarea in Mahara (Personal information I believe)
08:33:54 <elky> perhaps this one is better over email where we can discuss the requirements more fully
08:34:07 <anzeljg> ok
08:34:24 <anitsirk> i'd put it on the wiki under "Specifications in development" and flesh it out there and discuss it in a forum topic.
08:34:36 <anzeljg> cool
08:34:53 <anitsirk> in general, i don't see why a text area should be kept when individual input boxes would also guide a user more.
08:35:28 <anzeljg> my thoughts exactly, but as with dates I guess we (you guys) wanted users to have more freedom???
08:35:29 <anitsirk> but that now deviated from the actual file attachment question. anzeljg: could you please put everything in the specifications? we'll make a blueprint out of it.
08:35:48 <anzeljg> will do...
08:35:56 <anzeljg> on wiki or over email?
08:36:09 <elky> anitsirk suggested wiki and forum
08:36:13 <anzeljg> ok
08:36:27 <anitsirk> that way everyone can join in who wants to discuss it. :-)
08:36:31 <elky> that lets others not here to add their ideas
08:36:35 <VolkerJSchmidt> Would it not be an idea to introduce costum fields, so that an administrator has the choice?
08:36:36 <elky> jinx :P
08:36:58 <anitsirk> #action anzeljg will create a wiki page under "Specifications in development" for his enhancement suggestions for the resume and start a forum topic to discuss these.
08:37:27 <VolkerJSchmidt> Ok, I post the idea there.
08:37:32 <anzeljg> BRB
08:38:20 <anitsirk> ok. i'm looking forward to the specs.
08:38:41 <anitsirk> ok. i think that should be it for this and we'll see more on the wiki and the forum.
08:38:50 <anitsirk> #topic Catalyst Open Source Academy Students on Mahara (aquaman /-laptop / -ghost / -phone)
08:39:03 <aquaphone> Hey
08:39:35 <aquaphone> Was just gonna say that catalyst is running the open source academy atm
08:39:46 <elky> conveniently, i just found a nice easy bug for them.
08:40:05 <aquaphone> Even though I'm not at catalyst anymore I'm helping out for a week
08:40:21 <aquaphone> Basically school students learn about oos
08:40:28 <anitsirk> #info Catalyst is currently running the Open Source Academy http://catalyst.net.nz/academy
08:40:38 <anitsirk> thanks for still mentoring, aquaphone!
08:40:55 <aquaphone> The mahara students have submitted and merged  7 patches!
08:40:55 <anitsirk> #info you can see all the things that the students are and have been working on at https://reviews.mahara.org/#/q/project:mahara+branch:master+topic:academy,n,z
08:41:04 <aquaphone> More to come
08:41:11 <anitsirk> aquaphone: i thought we were already up to 10
08:41:30 <aquaphone> Hopefully they stick with the project
08:41:39 <aquaphone> That is me
08:41:41 <aquaphone> ..
08:42:07 <anitsirk> and they only started yesterday. 1.5 more days to go for the project week.
08:42:29 <aquaphone> Anitsirk  maybe i lost count.  7 was merged as well
08:43:23 <anitsirk> aquaphone: no worries. we'll count them up at the end.
08:43:28 <anitsirk> anzeljg: are you back?
08:43:34 <aquaphone> Heh yup
08:44:20 <anitsirk> VolkerJSchmidt: do you want to talk about your topic? i'll make a proper one out of it. i'd like to wait for anzeljg to come back when talking about the next meeting.
08:44:24 <anzeljg> yep
08:44:29 <anitsirk> ah. never mind.
08:44:30 <anitsirk> #topic Next meeting and chair
08:44:31 <anitsirk> We usually wanted to have a meeting every 5-6 weeks. That would be the 27th of February at 8 a.m. NZDT. That would be 7 p.m. UK and 8 p.m. central Europe.
08:44:50 <anitsirk> would that work or would you suggest another date / time?
08:45:02 <anzeljg> Fine by me
08:45:38 <anzeljg> well maybe it could be an hour later actually
08:46:30 <anitsirk> anzeljg: would work for me.
08:46:48 <elky> I'm not going to argue with later :P
08:47:03 <anitsirk> aarow? VolkerJSchmidt?
08:47:11 <anitsirk> aquaphone?
08:47:16 <aarow> Sounds good to me
08:47:25 <VolkerJSchmidt> Ok, I try to make it
08:47:48 <aquaphone> Wfm
08:48:31 <anitsirk> #agreed The next Mahara developer meeting will be on 27 February at 9 a.m. NZDT (26 February 8 p.m. UTC). http://www.timeanddate.com/worldclock/fixedtime.html?msg=24th+Mahara+Developer+Meeting&iso=20130227T09&p1=264
08:48:35 <anitsirk> who wants to chair it?
08:49:13 <anzeljg> never done it, so I could do it
08:49:27 <anitsirk> fantastic, anzeljg.
08:49:37 <anzeljg> :)
08:49:52 <anitsirk> #info anzeljg will chair the next, the 24th, Mahara developer meeting.
08:50:00 <anitsirk> thank you very much for volunteering, anzeljg.
08:50:17 <anitsirk> #topic Any other business
08:50:22 <anitsirk> VolkerJSchmidt: do you want to start?
08:50:28 <VolkerJSchmidt> Question: Is it not possible to use the EuroCV api to connect to EuroCV? I think it would make it easier to intruduce Mahara to institutions and companies as an integrated tool with the european employment program. http://www.eurocv.eu/soap.php
08:51:12 <anitsirk> #idea VolkerJSchmidt: use the EuroCV api to connect to EuroCV. I think it would make it easier to intruduce Mahara to institutions and companies as an integrated tool with the european employment program. http://www.eurocv.eu/soap.php
08:51:42 <anzeljg> actually it would mean one step more in the process...
08:52:02 <anzeljg> currently europass plugin for mahara connects directly to europass webservice
08:52:35 <anzeljg> seein the diagram on eurocv site it states that the mahara would have to use eurocv, which than connects to europass webservice
08:52:38 <anzeljg> ..
08:53:25 <anzeljg> or maybe I missed something?
08:54:04 <anitsirk> VolkerJSchmidt: the resume as it currently stands is a very generic one that doesn't fit all countries. klaus gutermann and florian also have great ideas for expansion, e.g. allow for multiple languages in it. thus, rethinking the resume in the larger context and thinking about big continent-wide initiatives etc. will help to guide where it could be going. can you please put your idea also on anzeljg's wiki page?
08:54:48 <VolkerJSchmidt> My idea is that any information enter in Mahara would reflect in the EuroCV and also within the Mahara plugin
08:54:53 <VolkerJSchmidt> Ok will do
08:55:01 <VolkerJSchmidt> @ antisirk
08:55:13 <VolkerJSchmidt> #idea VolkerJSchmidt: There is a very nice plugin for TinyMCE which I and some poeple tried to implement in Mahara, without luck. It would make for a great improvement in the handeling of text input as there are enough employees working with the new Microsoft Office products. http://tinymce.swis.nl/
08:55:32 <VolkerJSchmidt> My second and last question
08:55:34 <anitsirk> anzeljg: do you know if europass supports leap2a?
08:55:44 <anzeljg> I can check
08:56:45 <anitsirk> VolkerJSchmidt: sonn is in the process of updating tinymce. maybe the skins work better in there?
08:57:23 <VolkerJSchmidt> Where can I get in touch with him/her?
08:58:02 <anitsirk> VolkerJSchmidt: the patch is at https://reviews.mahara.org/1997 and once applied in your developer environment you could put the skins on top to test them.
08:58:22 <VolkerJSchmidt> Maybe my understanding of the EuroCV api is not good enough. It is my assumption that there is a different way then importing/exporting XML files ?!?
08:58:37 <VolkerJSchmidt> ok
08:59:14 <anzeljg> VolkerJSchmidt: will look into it...
08:59:21 <VolkerJSchmidt> thanks
09:00:12 <anitsirk> any other business?
09:00:19 <anzeljg> anitsirk: As far as I can tell Europass support/uses XML Schema, JSON Schema and offers web services to generate CV in different formats #link http://europass.cedefop.europa.eu/en/resources/for-developers.iehtml
09:00:47 <aquaghost> hi, on a computer now
09:00:54 <anzeljg> but they do offer interoperability for interested partners #link http://europass.cedefop.europa.eu/en/resources/for-developers/technical-documentation-and-support.iehtml
09:01:01 <anzeljg> maybe worth checking out...
09:01:09 <anzeljg> no sign of Leap2A though
09:01:43 <anitsirk> it is more of a portfolio standard and not used elsewhere i think.
09:01:49 <VolkerJSchmidt> we could suggest it to cedefop,no?
09:02:04 <anzeljg> i think yes
09:02:55 <anzeljg> I think you should contact Eleni Kargioti (#link https://joinup.ec.europa.eu/people/11463)
09:03:24 <anzeljg> she is in charge for XML Schema v3.0 and I think she will know about those things...
09:03:41 <anitsirk> #idea bring Leap2A to the attention of cedefop and see if it would be of interest for data interchange.
09:03:56 <VolkerJSchmidt> ok, I ask for contact and discuss it with her. Can I refer to this meeting?
09:04:17 <anitsirk> sure. the meeting is public. links will be published at the end and also appear on the wiki.
09:04:26 <VolkerJSchmidt> ok
09:04:45 <anitsirk> #action VolkerJSchmidt will contact Eleni Kargioti (https://joinup.ec.europa.eu/people/11463) about Leap2A and europass
09:04:52 <anitsirk> thank you very much, VolkerJSchmidt
09:05:07 <anitsirk> any other business?
09:05:17 <aquaghost> none from me
09:05:24 <anzeljg> nope
09:05:29 <elky> man, first meeting and we're already actioning stuff to you, Volker :)
09:05:31 <VolkerJSchmidt> me either
09:05:59 <VolkerJSchmidt> That's what I like about Open Source
09:06:09 <anzeljg> +1
09:06:30 <anitsirk> One short FYI from me: Our Australian colleagues are working on logging masquerading as admin, license metadata for artefacts and also group tags. the latter are currently on hold, but the first two are going strong and in review right now. just so you know what other new big features are on the way.
09:07:29 <aquaghost> VolkerJSchmidt: sorry I was a bit absent this meeting, hopefully I am a bit more present next one, will read over the minutes tomorrow NZ time though, but welcome to the community!
09:07:40 <anitsirk> if nobody has anything else, we'll conclude this meeting at a little over 1 hour.
09:07:50 <elky> i'm done
09:07:56 <anitsirk> aquaghost: BTW: VolkerJSchmidt has been one of the first mahara users he told me.
09:08:08 <VolkerJSchmidt> aquaghost: thanks
09:08:21 <anitsirk> #endmeeting