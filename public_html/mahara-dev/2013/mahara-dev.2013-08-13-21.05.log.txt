21:05 < aarowlaptop> #startmeeting
21:06 < aarowlaptop> hm, actually the maharabot isn't present
21:07 < aarowlaptop> Well we can proceed anyway, and just manually enter the logs into the record later
21:08 < sonn_> #info sonn_ - Son Nguyen @ Catalyst IT, Wellington, NZ
21:08 < robertl> #info robertl is Robert Lyon at Catalyst IT, Wellington, NZ
21:08 < aarowlaptop> Hello and Welcome to the 26th Mahara dev meeting
21:09 < aarowlaptop> please introduce yourself using the #info tag, (as sonn and robertl have done)
21:09 < aarowlaptop> #info aaronw - Aaron Wells at Catalyst IT, Wellington, NZ
21:09 < aarowlaptop> Anyone else here besides us Catalyst folks? dobedobedoh?
21:10 < aarowlaptop> rkabalin?
21:12 -!- dajan [dajan@nat/catalyst-it/x-obfwsavmulhltfzg] has joined #mahara-dev
21:14 < dajan> Sorry for the late arrival. Colloquy was crashing. Had an old version. Now all good.
21:15 < aarowlaptop> Hi dajan, we were just waiting to see if anyone else was showing up
21:15 < aarowlaptop> also, do you know how to start up the meetbot?
21:15 < dajan> Feel empty and lonely here.
21:16 < dajan> No never used it. Just looking for help on the web. Hold on.
21:16 < aarowlaptop> https://wiki.mahara.org/index.php/Developer_Area/Developer_Meetings/Chair_Duties tells how to use it, but it does not mention starting the bot up
21:17 < aarowlaptop> apparently #startmeeting does nothing unless the bot is actually present ;)
21:17 < robertl> is it a matter of inviting it to the channel?
21:17 < aarowlaptop> robertl: Could be
21:17 < aarowlaptop> hm, I can't invite it because I'm not a channel operator
21:17 < dajan> #startmeeting is the command (http://www.muse.net.nz/blog/2012/10/15/running-irc-meetings-with-meetbot/)
21:17 < robertl> ah none of the ops are here
21:18 < aarowlaptop> I'll place that on the agenda for next meeting -- assign more channel ops in the #mahara-dev channel
21:18 < robertl> good idea
21:18 < robertl> there should be at least 1 catalyst ops
21:20 < dajan> Interesting conclusion in the blogpost I sent you : It was a real pain to install this the first time around. All the information is available but not in a single clean place. I almost completed a Node.js replacement in less time. More on that in my next post!
21:21 < aarowlaptop> Well, let's go over the stuff on the agenda, such as it is https://wiki.mahara.org/index.php/Developer_Area/Developer_Meetings/2013-05-28
21:22 < aarowlaptop> First item is "Items from last meeting"
21:23 < aarowlaptop> I believe the last official meeting was the one in April. There was an attempted meeting on 28 May, but no one showed up except me and Kristina so we pushed it back to today
21:25 < aarowlaptop> At that meeting sonn_ said he was planning to look into replacing the file browser with the elfinder library. Has anything happened with that, sonn_?
21:25 < aarowlaptop> right, here's the minutes from last meeting, with action items at the bottom: http://meetbot.mahara.org/mahara-dev/2013/mahara-dev.2013-04-16-08.02.html
21:26 < sonn_> I have developed an prototype for it
21:26 < sonn_> It is not fully developed
21:26 < aarowlaptop> Good enough to share with people yet?
21:26 < sonn_> and need to discuss more about security concerns
21:26 < sonn_> Yes
21:27 < sonn_> I have put it on gerrit
21:27 < sonn_> https://reviews.mahara.org/#/c/2075/
21:27 < dajan> I am interested to have a quick look at it. If it is possible.
21:28 < aarowlaptop> dajan: We should be able to check that out into a test site for you to look at
21:28 < sonn_> Some commands still need to be implemented
21:28 < aarowlaptop> I recall that one of your UI recommendations was to improve/replace the file browser
21:28 < dajan> Unfortunately I don't have access to a test site. So I will look over your shoulder
21:28 < aarowlaptop> ah yes :)
21:29 < dajan> To improve it. Or replace it. All depends on how the files are managed by the code/database after their upload.
21:29 < aarowlaptop> sonn_: Right, the file browser has some major security implications, so we will definitely want to keep an eye on that aspect of it
21:30 < sonn_> I would replace it
21:30 < dajan> Does it impact the way mobile platform such as PortfolioUP or Maharadroid are sending files to Mahara?
21:31 < sonn_> aquaman may have some suggestions about security
21:31 < aarowlaptop> I think the main security ramifications of the file browser are: 1) Making sure people can't access files they have no permission to see; 2) Making sure people can't upload files which they can then execute on the server; 3) Making sure the web aspects of it are secure from injections et al
21:31 < aarowlaptop> dajan: No, as I understand it, this would be a change to the file UI in Mahara only
21:32 < aarowlaptop> the backend file storage remains the same
21:32 < aarowlaptop> is that right, sonn_?
21:32 < sonn_> aarowlaptop: yes
21:33 < aarowlaptop> right, so since the file storage and web services stuff remains the same, PortfolioUP and MaharaDroid don't change
21:34 < sonn_> dajan: sending files in PortfolioUP or Maharadroid will not affect
21:34 < dajan> Good
21:35 < aarowlaptop> okay, next action item: rkabalin and dobedobedoh discuss if we have resources/time to maintain Mahara Debian package and report on the next meeting
21:35 < aarowlaptop> well, since they're not here, we'll have to push that to later
21:36 < aarowlaptop> and third, me, " to look into the feasibility of a common CSS "abstraction layer" for multiple layout plugins"
21:36 < aarowlaptop> This was sort of based on my rough idea of a "Layout" plugin type, that would allow for multiple different types of Page layouts
21:37 < robertl> would that have been similar to how the new flexible layouts will work?
21:38 < aarowlaptop> well... this was something I thought of when we were discussing whether to go with Flexible Layouts or with a different layout that sort of tiled all the blocks responsively
21:38 < robertl> ah
21:38 < aarowlaptop> it made me think, could we provide an API to allow for radically different layouts as a Plugin?
21:39 < robertl> right, I see - so how far did you get with the idea?
21:39 < aarowlaptop> but I've pretty much dropped the idea since then. After a discussion with Evonne, it sounds like it would pose some serious problems for theme designers
21:40 < robertl> ah yes it would be a nightmare for designers
21:40 < dajan> Does it mean it would give plugin designers the freedom to play with the disposition of "things" on the screen?
21:40 < aarowlaptop> Well, it would be for Page Layouts only
21:41 < aarowlaptop> basically, it would allow you to write a Plugin which, if installed, would add additional options to the "Layout" tab of a Page
21:42 < dajan> Umm. Well if the API comes along with abstract CSS classes themes designers can use, it wouldn't be a huge problem. But it means to retying all the themes and document this as well
21:42 < aarowlaptop> yes
21:43 < aarowlaptop> other than the brief discussion about flexible layout vs flowing layout, I haven't seen much call from the community for this kind of pluggability, so I think that's another reason not to pursue it
21:43 < aarowlaptop> okay, so that's all the items from last meeting
21:44 < aarowlaptop> Next item on this meeting's Agenda: Granting reviewer status to Robert Lyon - done in between developer meetings
21:44 < aarowlaptop> So, yep, we've granted +2 reviewer status to Robert Lyon
21:44 < robertl> why thank you kind sir!
21:44 < aarowlaptop> This was meant to be voted on at the fizzled-out meeting in May. Since that meeting fell through, we sent out an email to all the developers to get their approval, instead.
21:45 < aarowlaptop> He's been pretty prolific on gerrit since then, so it's worked out pretty well. ;)
21:46 < aarowlaptop> Next item: Work in development (Kristina)
21:46 < aarowlaptop> Since she's off at a different meeting, I'll cover this
21:47 < aarowlaptop> We are getting close-ish to the 1.8 release. Feature freeze was last week, UI freeze is scheduled for next week.
21:48 < aarowlaptop> A semi-complete list of features going into 1.8 is visible here: https://bugs.launchpad.net/mahara/+bugs?field.tag=nominatedfeature
21:49 < aarowlaptop> unlike 1.7, this one is going to include quite a few noticeable new features, largely upstreamings of the development work we've been doing for myportfolio.school.nz
21:51 < aarowlaptop> We've also got the Page Skins feature, and the attachments-on-resume-items-and-text-notes features contributed by community members, which we're aiming to include as well
21:51 < robertl> sonn_ looking at that list is the tagging feature still in progress - or is it fix committed now?
21:52 < robertl> also should flexible layouts be part of that list?
21:52 < aarowlaptop> yeah, we need to update that list
21:52 < aarowlaptop> :)
21:53 < aarowlaptop> #action aarowlaptop to update the nominatedfeatures list on launchpad for Mahara 1.8
21:53 < dajan> I see you are integrating Patrick Pollet's ldap sync. But don't forget that the main issue for European institution in installing or not installing Mahara is a good CAS auth. plugin. Patrick did one but it should be reviewed and polished
21:54 < robertl> the good thing is over half of the list is done and the rest are close to finishing
21:54 < aarowlaptop> Good suggestion. I've actually been working with the CAS plugin for one of our clients, so I've already code reviewed it.
21:55 < aarowlaptop> The main thing it's lacking is smoother integration with the login screen. Currently the install instructions ask you to manually add a link to /auth/cas/index.php to your site's front page
21:55 < dajan> Universtiy of Troyes (France) have installed it and have some issues we could discuss f2f today.
21:56 < aarowlaptop> #action aarowlaptop and dajan to discuss incorporating Patrick Pollet's CAS plugin into Mahara core
21:56 < dajan> Yes the manual installation of a link on the front page is one of the issues.
21:57 < aarowlaptop> Okay, next item on the agenda is "Coding guidelines (Ruslan)". It sounds like Ruslan had some suggestions about the coding guidelines, but unfortunately he's not present. rkabalin: Maybe you could post your thoughts on the coding guidelines to the Mahara dev forum? Or just post theme here on IRC whenever you get back?
21:57 < aarowlaptop> And lastly: Next meeting and chair
21:59 < aarowlaptop> Given that the last several dev meetings have been pretty dead, I'm wondering whether we actually should continue with these IRC dev meetings? I'm tempted to do a "forum meeting" instead
22:00 -!- iWaa [~iwaa@122.56.234.51] has joined #mahara-dev
22:00 < dajan> Or a Google+ hangout?
22:01 < aarowlaptop> Well, I'm not sure if people would actually show up for any kind of "live" meeting on any venue
22:01 < aarowlaptop> so I'm interested in whether we could get better results from some sort of non-synchronous "meeting"
22:02 < aarowlaptop> (putting "meeting" in quotes because of course it's not actually a meeting if it's non-synchronous)
22:02 < anitsirk> sorry for being late. had two other meetings.
22:02 < aarowlaptop> hi anitsirk
22:02 < robertl> so it would simply be a 'meeting' forum topic where one could add things any time?
22:03 < aarowlaptop> more or less
22:03 < anitsirk> dajan: i wouldn't go for google hangout because you need to have a google account.
22:03 < aarowlaptop> or maybe an etherpad would be more appropriate
22:03 < anitsirk> etherpad sounds better than forum because you can put comments right next to sb else.
22:03 < anitsirk> but at some point we'd have to transfer it to the wiki or somewhere else to keep a record of it.
22:04 < aarowlaptop> I'll send out a forum post and/or email to see what the people not present at this meeting think. ;)
22:04 < aarowlaptop> in the meantime, we'll schedule another meeting as usual
22:04 < anitsirk> sounds good
22:04 < aarowlaptop> So, the next meeting is due to be at around 0800 UTC
22:04 < aarowlaptop> any suggestions on a date?
22:05 < anitsirk> usually we have 4-5 weeks cycles
22:05 < dajan> Suggestion: Could we change the list of the meetings minutes on the wiki to have the latest on the top of the list. I am happy to do this.
22:05 < aarowlaptop> dajan: Excellent idea, please do
22:06 < robertl> if we go 5 weeks from now that should be safely after the 1.8 release, yes?
22:06 < aarowlaptop> probably ;)
22:07 < anitsirk> i guess it's rather 1.8 RC phase.
22:07 < robertl> sorry, that is what I meant
22:07 -!- iWaa [~iwaa@122.56.234.51] has quit [Ping timeout: 245 seconds]
22:07 < aarowlaptop> if it's still during the RC phase, then let's go for earlier rather than later. So let's say 4 weeks from now
22:08 < aarowlaptop> Sept 3, 0800 UTC
22:08 < anitsirk> would work for me
22:08 < sonn_> ok for me
22:09 < robertl> sounds good to me
22:09 < aarowlaptop> good for you, dajan? http://www.timeanddate.com/worldclock/fixedtime.html?iso=20130903T08
22:09 < dajan> Would be with my students in the computer lab. Will do my best to be with you
22:09 < aarowlaptop> would another day be better for you?
22:10 < aarowlaptop> I guess this is 9am in the UK because of daylight saving time
22:11 < dajan> Monday and Thursday are my two days off. But I am not important enough to change any date. I will always do my best to be here.
22:11 < dajan> It is 9am in UK. Right.
22:11 < aarowlaptop> are you more likely to be there, or less likely, on your day off? ;)
22:12 < dajan> I will be more likely behind my computer doing Mahara and PhD related business. Yes.
22:14 < aarowlaptop> okay, let's shift it to Thurs Sep 5 8:00am UTC
22:14 < anitsirk> fine by me
22:14 < aarowlaptop> that's 8pm Thursday here in NZ, which is just as good
22:14 < sonn_> good for me
22:15 < aarowlaptop> http://www.timeanddate.com/worldclock/fixedtime.html?iso=20130905T08&p1=1440
22:15 < aquaman> ohai
22:15 < aquaman> fully missed sorry
22:15 < aarowlaptop> Hi aquaman, just wrapping up a dev meeting
22:15 < sonn_> hi aquaman
22:15 < aarowlaptop> no worries
22:15 < dajan> ok for me
22:15 < aarowlaptop> Final item on the agenda: "Any other business"
22:15 < aarowlaptop> so, any other business?
22:16 < anitsirk> aquaman: next meeting set for 5 September 8 p.m. NZ time
22:16 < anitsirk> who is chairing?
22:16 < aarowlaptop> oh right
22:16 < aarowlaptop> I'm willing to chair the next one.
22:17 < dajan> In the other business. Could we (re)discuss plugin management on Mahara. I wish it would be more easy to manage and moreover to delete
22:17 < anitsirk> #info: next dev meeting on 5 September 2013, 8 a.m. UTC and chair is aarowlaptop
22:17 < anitsirk> http://www.timeanddate.com/worldclock/fixedtime.html?iso=20130905T08&p1=1440
22:18 < aarowlaptop> dajan: You mean the /admin/extensions/plugins.php page?
22:18 < dajan> Maybe let put this into the agenda for a next meeting.
22:18 < aarowlaptop> sure
22:18 < dajan> Yes
22:19 < dajan> Yes I mean this page Aaron
22:20 < anitsirk> dajan: could you please put down your thoughts on a wiki page for the next meeting then so it can be reviewed beforehand?
22:20 < dajan> For the moment. Bye all. Cheers.
22:20 < aarowlaptop> Okay, I declare this meeting adjourned
22:20 < dajan> Yes I will put down my ideas on a doc. no problem
22:20 < anitsirk> cool. thanks.
22:20 < dajan> could put action on this. Do this before the next meeting
22:21 < dajan> Bye
22:21 < aarowlaptop> Right, #action: Dajan to write up suggestions for improving Mahara admin interface for plugin management
22:22 -!- dajan [dajan@nat/catalyst-it/x-obfwsavmulhltfzg] has left #mahara-dev []
22:22 < aarowlaptop> oh, another one: #action: Aaron to look into how to change the Channel Operators for the #mahara-dev channel
22:22 < aarowlaptop> okay, meeting adjourned ;)
22:20 < aarowlaptop> #endmeeting
