===================
#mahara-dev Meeting
===================


Meeting started by lamiette_ at 19:35:46 UTC. The full logs are
available at
http://meetbot.mahara.org/mahara-dev/2011/mahara-dev.2011-06-29-19.35.log.html
.



Meeting summary
---------------
* Meeting Attendees  (lamiette_, 19:35:54)
  * dobedobedoh is Andrew Nicols - LUNS Ltd, UK  (dobedobedoh, 19:36:31)
  * anitsirk is Kristina Hoeppner from Catalyst IT in Wellington, NZ
    (anitsirk, 19:36:36)
  * rkabalin_ is Ruslan Kabalin, LUNS Ltd., Lancaster, UK  (rkabalin_,
    19:36:46)
  * richardm is Richard Mansfield from Catalyst IT in Wellington, NZ
    (richardm, 19:36:52)
  * lamiette_ is Stacey Walker from Catalyst IT in Brighton UK
    (lamiette_, 19:37:02)
  * hughdavenport_ho is Hugh Davenport from Catalyst IT in Wellington,
    NZ  (hughdavenport_ho, 19:37:06)
  * dan_p_ is Dan Poltawski, LUNS ltd uk (on train)  (dan_p_, 19:37:24)
  * thomaswbell88 is Thomas Bell from TDM Ltd, UK  (thomaswbell88,
    19:37:28)
  * is Francois Marier  (fmarier_, 19:37:30)
  * bugg_home is Brett Wilkinsa from Catalyst IT in Wellington, NZ
    (bugg_home, 19:37:30)
  * ricardomano is Richard Hand from TDM Ltd, UK  (ricardomano,
    19:37:43)
  * pxh is Piers Harding from Catalyst IT in Wellington, NZ  (pxh,
    19:40:06)

* MNet future -- We're adding Web Services to 1.5  (lamiette_, 19:43:31)
  * LINK: https://wiki.mahara.org/index.php/Plugins/Artefact/WebServices
    (fmarier_, 19:43:43)
  * pxh has writtten a web services framework, based on the Moodle 2.0
    web services Further information at
    https://wiki.mahara.org/index.php/Plugins/Artefact/WebServices
    (lamiette_, 19:57:36)
  * pxh> features are:  SOAP, REST (really just basic HTTP post
    parameters or JSON), and XML-RPC  (lamiette_, 19:59:00)
  * Auth via WSEE for SOAP, as well as the tokens/users (as mentioned
    before)  (lamiette_, 19:59:14)
  * LINK: https://gitorious.org/mahara-contrib/artefact-webservice
    (lamiette_, 20:02:38)
  * available as a contrib plugin  (lamiette_, 20:02:53)
  * we have to start thinking about Mnet stuff: largely due to this from
    Moodle HQ http://moodle.org/mod/forum/discuss.php?d=175158
    (lamiette_, 20:06:14)

* Bug status proposal  (lamiette_, 20:06:58)
  * milestones have been used for marking bugs to upcoming releases and
    as for a changelog/detailed version of the release notes
    (lamiette_, 20:12:12)
  * ACTION: fmarier_ to update the bug status page with new "milestone"
    usage guidelines  (lamiette_, 20:22:45)
  * every new feature on master should have a bug report associated with
    it  (lamiette_, 20:25:18)
  * reviewers to be stricter about suitable bug numbers for features
    submitted to gerrit - isn't mandatory though  (lamiette_, 20:32:02)

* MaharaUK2011  (lamiette_, 20:32:31)
  * LUNS and TDM have both offered to host maharauk, so we've suggested
    that 12 is in Lancaster, hosted by LUNS and 13 is in the West
    Midlands, hosted by TDM  (lamiette_, 20:34:58)
  * LINK:
    http://cryptnet.net/fdp/crypto/keysigning_party/en/keysigning_party.html
    (fmarier_, 20:40:17)
  * There was also talk about some entity hosting online 'conferences'
    inbetween during the year (webinar type things)  (lamiette_,
    20:40:55)
  * twitter hashtag #maharauk11 for anyone interested in seeing what
    attendees had to say  (lamiette_, 20:42:20)
  * LINK: http://t.co/dFHqjYX - Git in Mahara  (dobedobedoh, 20:42:39)
  * LINK: http://www.slideshare.net/rkabalin/presentations - both my
    presentations  (rkabalin_, 20:43:00)
  * LINK: http://www.slideshare.net/rkabalin/presentations   (lamiette_,
    20:43:29)
  * LINK: http://www.slideshare.net/andrewnicols/presentations for mine
    (dobedobedoh, 20:44:10)
  * LINK:
    http://www.slideshare.net/4nitsirk/21-34-ways-to-get-involved-in-mahara
    (anitsirk, 20:44:28)
  * LINK: http://www.youtube.com/watch?v=Rbz9vST0CLw recording
    (anitsirk, 20:45:01)
  * LINK: Mark Osbornes keynote
    http://prezi.com/dubgzzefzioj/maharauk-open-for-learning/
    (lamiette_, 20:45:45)

* Mahara Reviewer applications#  (lamiette_, 20:46:05)
  * Hugh Davenport Mahara reviewer application  (lamiette_, 20:48:33)
  * Brett Wilkins Mahara reviewer application  (lamiette_, 20:48:49)
  * Darryl Hamilton Mahara reviewer application  (lamiette_, 20:49:26)
  * lordp is Darryl Hamilton  (lordp, 20:49:38)
  * LINK:
    http://meetbot.mahara.org/mahara-dev/2010/mahara-dev.2010-11-10-07.34.html
    FYI  (dobedobedoh, 20:51:39)
  * LINK: https://wiki.mahara.org/index.php/Developer_Area/Code_Review
    (lamiette_, 20:56:30)
  * LINK: https://wiki.mahara.org/index.php/User:Hughdavenport
    (lamiette_, 20:56:40)
  * LINK: https://wiki.mahara.org/index.php/User:Brettwilkins
    (fmarier_, 20:56:54)
  * LINK: https://wiki.mahara.org/index.php/User:Darrylh   (fmarier_,
    20:57:01)
  * AGREED: Hugh Davenport to become Mahara reviewer  (lamiette_,
    21:03:05)
  * AGREED: Brett Wilkins to become a Mahara reviewer  (lamiette_,
    21:09:26)
  * AGREED:   (lamiette_, 21:09:30)

* mahara.org forum badges  (lamiette_, 21:13:27)
  * AGREED:   (lamiette_, 21:21:05)
  * ACTION: thomaswbell88 to come up with ideas for mahara.org badges
    and post on the wiki under Specifications in Development
    (lamiette_, 21:21:44)
  * LINK:
    https://wiki.mahara.org/index.php/Developer_Area/Specifications_in_Development
    (lamiette_, 21:21:52)

* Update on the user manual  (lamiette_, 21:24:27)
  * the user manual is now started in asciidoc. the repository contains
    a Makefile for easy creation of the output file.  (anitsirk,
    21:24:41)
  * the manual sits in git, some information on where it is and how to
    set up asciidoc (esp. the syntax-highlighting in vim) is at
    (anitsirk, 21:24:51)
  * LINK: https://wiki.mahara.org/index.php/Manual/Manual_Setup
    (lamiette_, 21:24:52)
  * LINK: https://wiki.mahara.org/index.php/Manual/Manual_Setup
    (anitsirk, 21:24:55)
  * we now have another mahara family member: the Mahara Scribe thanks
    to evonne who came up with it. it's now also on the wiki. :-)
    (anitsirk, 21:25:07)
  * the user manual is now started in asciidoc. the repository contains
    a Makefile for easy creation of the output file.  (anitsirk,
    21:25:44)
  * the manual sits in git, some information on where it is and how to
    set up asciidoc (esp. the syntax-highlighting in vim) is at
    (anitsirk, 21:25:50)
  * LINK: https://wiki.mahara.org/index.php/Manual/Manual_Setup
    (anitsirk, 21:25:56)
  * we now have another mahara family member: the Mahara Scribe thanks
    to evonne who came up with it. it's now also on the wiki. :-)
    (anitsirk, 21:26:02)

* Invitation to join the Debian Packaging team  (lamiette_, 21:31:00)
  * fmarier_ welcomes dobedobedoh and rkabalin_ to the Mahara packaging
    team  (fmarier_, 21:35:06)

* Next Meeting date  (lamiette_, 21:35:44)
  * LINK:
    http://www.timeanddate.com/worldclock/fixedtime.html?iso=20110803T0730
    (lamiette_, 21:39:02)
  * AGREED:   (lamiette_, 21:39:43)
  * LINK:
    http://www.timeanddate.com/worldclock/fixedtime.html?iso=20110803T0730
    (lamiette_, 21:40:18)
  * next meeting Wednesday, 3 August 2011, 07:30:00 UTC time
    (lamiette_, 21:41:44)

Meeting ended at 21:47:27 UTC.




Action Items
------------
* fmarier_ to update the bug status page with new "milestone" usage
  guidelines
* thomaswbell88 to come up with ideas for mahara.org badges and post on
  the wiki under Specifications in Development




Action Items, by person
-----------------------
* fmarier_
  * fmarier_ to update the bug status page with new "milestone" usage
    guidelines
* thomaswbell88
  * thomaswbell88 to come up with ideas for mahara.org badges and post
    on the wiki under Specifications in Development
* **UNASSIGNED**
  * (none)




People Present (lines said)
---------------------------
* lamiette_ (186)
* fmarier_ (120)
* dobedobedoh (71)
* anitsirk (61)
* rkabalin_ (45)
* pxh (36)
* dan_p (34)
* bugg_home (33)
* richardm (29)
* thomaswbell88 (26)
* hughdavenport_ho (19)
* dan_p_ (11)
* Mjollnir` (9)
* lordp (3)
* waawaamilk (3)
* maharameet (3)
* ricardomano (1)




Generated by `MeetBot`_ 0.1.4

.. _`MeetBot`: http://wiki.debian.org/MeetBot
