19:34:11 <rkabalin_> #startmeeting
19:34:11 <maharameet> Meeting started Tue Sep  6 19:34:11 2011 UTC.  The chair is rkabalin_. Information about MeetBot at http://wiki.debian.org/MeetBot.
19:34:11 <maharameet> Useful Commands: #action #agreed #help #info #idea #link #topic.
19:34:13 <hughdavenport> hi there
19:34:24 <rkabalin_> #topic Meeting Attendees
19:34:35 <rkabalin_> #info rkabalin is Ruslan Kabalin - LUNS Ltd, UK
19:34:45 <dobedobedoh> #info dobedobedoh is Andrew Nicols - LUNS Ltd, UK
19:34:51 <anitsirk> #info: anitsirk is Kristina Hoeppner of Catalyst IT, Wellington, NZ
19:34:53 <fmarier_> #info fmarier_ is Francois Marier
19:34:53 <dan_p> #info dan_p is Dan Poltawski - LUNS Ltd, UK
19:34:58 <richardm> #info richardm is Richard Mansfield, Catalyst, Wellington
19:34:59 <hughdavenport> #info hughdavenport is Hugh Davenport, Catalyst IT, Wgtn
19:35:15 <iarenaza> #info iarenaza is Iñaki Arenaza - Mondragon Unibertsitatea
19:36:00 <rkabalin_> I guess that is everyone
19:36:07 <rkabalin_> please say ".." on a line by itself if you are typing a long multi-line piece to indicate you've finished
19:36:14 <rkabalin_> ..
19:36:33 <rkabalin_> ok
19:36:40 <rkabalin_> #topic Items from previous meeting
19:36:54 <rkabalin_> #info fmarier_ to update the bug status page with new "milestone" usage guidelines
19:37:18 <fmarier_> rkabalin_: i've put it on the agenda already :)
19:37:18 <rkabalin_> this is related to today's topic in fact
19:37:25 <fmarier_> but it's done
19:37:42 <rkabalin_> #info thomaswbell88 to come up with ideas for mahara.org badges and post on the wiki under Specifications in Development
19:37:56 <rkabalin_> thomaswbell88 is not here, but I think he has done that
19:38:26 <dan_p> We said need more detail last time I think?
19:38:45 <rkabalin_> ah, it was discussed last time as well
19:39:04 <fmarier_> actually Don Presant(?) has talked about that on the forums
19:39:28 <fmarier_> I have a meeting with him and the person driving this at Mozilla on Friday
19:39:43 <rkabalin_> that is a bit different
19:39:47 <fmarier_> so we'll hopefully be able to report a bit more on the forum
19:39:49 <rkabalin_> but relevant
19:40:07 <anzeljg> hi
19:40:12 * fmarier_ waves at anzeljg
19:40:15 <rkabalin_> ok, I will not be creating another action for this then
19:40:32 <anitsirk> hello anzeljg and bugg_home
19:40:33 <anzeljg> #info Anzelj Gregor, Slovenian langpack, plugin developer
19:40:33 <rkabalin_> hello bugg_home and anzeljg
19:40:41 <fmarier_> hi bugg_home
19:40:51 <bugg_home> morning folks
19:40:59 <rkabalin_> next item
19:41:04 <bugg_home> I forgot about this channel, it's been too long >.<
19:41:15 <rkabalin_> #info LUNS to investigate adding a new mahara integration project to run selenium tests
19:42:40 <dobedobedoh> dan_p was going to look at this one IIRC ?
19:42:49 <dan_p> That was mine really and I'm afraid haven't done it
19:43:20 <rkabalin_> ok, then I postpone it to the next meeting
19:43:33 <rkabalin_> #action dan_p  LUNS to investigate adding a new mahara integration project to run selenium tests
19:44:07 <rkabalin_> #topic Mahara Newsletter October 2011 [Kristina]
19:44:09 <fmarier_> it's also on the wiki here: https://wiki.mahara.org/index.php/Developer_Area/Current_Tasks
19:44:31 <fmarier_> (not sure it needs to stick around in the action for every meeting)
19:44:43 <rkabalin_> #undo
19:44:43 <maharameet> Removing item from minutes: <MeetBot.items.Topic object at 0x97f9fac>
19:45:06 <rkabalin_> ah, that the wrong one ))
19:45:10 <fmarier_> :)
19:45:17 <dobedobedoh> You can only undo one IIRC
19:45:23 <dan_p> Redo?
19:45:30 <dobedobedoh> no redo... have to copy/paste ;)
19:45:35 <rkabalin_> fmarier_: agree, but leave it for now
19:45:40 <rkabalin_> #topic Mahara Newsletter October 2011 [Kristina]
19:45:56 <anitsirk> oops. already?
19:46:00 <anitsirk> OK. just a sec.
19:46:16 <anitsirk> I would like to remind everybody that the next issue of the Mahara
19:46:16 <anitsirk> Newsletter will come out on October 1, 2011. If you have anything that
19:46:17 <rkabalin_> yep, it has been moved forward ))
19:46:17 <anitsirk> you would like to mention in it, please send an email to
19:46:17 <anitsirk> newsletter@mahara.org so that I am notified. Possible topics are:
19:46:17 <anitsirk> developments you are working on, plugins you have created, conference
19:46:17 <anitsirk> recap, events you have planned within the next 3 months, implementation
19:46:18 <anitsirk> of Mahara in various contexts etc. Spread the word about what you are
19:46:18 <anitsirk> doing with Mahara.
19:46:18 <anitsirk> ..
19:46:52 <anitsirk> #info: next MAhara newsletter will come out October 1, 2011 and everybody is invited to submit stories.
19:47:23 <rkabalin_> great
19:47:30 <iarenaza> anitsirk: would the move of translations to Launchpad qualify?
19:47:35 <anitsirk> that's all i wanted to say
19:47:40 <anitsirk> iarenaza: yes. definitely!
19:47:46 <fmarier_> iarenaza: that's a good idea
19:47:57 <fmarier_> would be nice to have it written from the perspective of a translator
19:48:11 <anitsirk> that's a major change and will hopefully allow others to get involved
19:48:19 <fmarier_> because ultimately translations being easier for translators is all that matters
19:48:38 <iarenaza> I'll try to write up something (and see if I can get some help from other members of the Spanish translation group)
19:48:42 <fmarier_> (i.e. it shouldn't be just to "make things easy for developers")
19:48:58 <dobedobedoh> It'd be good to put a link to the current up-to-date translations too
19:49:11 <anitsirk> that sounds great, iarenaza, and yes, dobedobedoh
19:49:16 <dobedobedoh> mainly for selfish reasons - I doubt many people expect a UK translation exists ;)
19:49:41 <iarenaza> dobedobedoh: you mean a "proper English" translation? :-)
19:49:53 <dobedobedoh> I mean UK english as opposed to Kiwi
19:50:05 <rkabalin_> I personnaly found this launchpad transation interface difficult...
19:50:14 <dobedobedoh> Me too - it's really convoluted
19:50:25 <dobedobedoh> but that's not a dicussion for the newsletter topic I guess
19:50:41 <rkabalin_> on, next topic
19:50:47 <anitsirk> i found it good for easy translations, but as soon as ou have to look into the code to actually find out if there is a different meaning, then it gets tricky for non-techies.
19:51:03 <anitsirk> yep, correct. thanks for keeping us focused, rkabalin
19:51:15 <rkabalin_> #topic Bug status fields [Francois and Ruslan]
19:51:58 <fmarier_> actually, before we move on, this is the page for checking how up to date all of the translations are: https://translations.launchpad.net/mahara-lang
19:52:18 <rkabalin_> #link https://wiki.mahara.org/index.php/Developer_Area/Bug_Status
19:52:20 <richardm> except for the ones that aren't there yet :)
19:52:46 <dobedobedoh> Is langpacks.mahara.org still relevant then?
19:53:07 <fmarier_> dobedobedoh: yes, but that doesn't tell you the % translated
19:53:09 <richardm> langpacks.mahara.org is the place to get a langpack that can be installed into mahara
19:53:11 <rkabalin_> Shall we move langpack discussion to separate topic today?
19:53:34 <rkabalin_> otherwice it becomes messy ))
19:53:41 <dan_p> rkabalin_: Yeah, aob
19:53:44 <anzeljg> on launchpad you can only download langpack in .po or.mo format. Or am I wrong?
19:53:52 <fmarier_> anzeljg: correct
19:53:54 <richardm> anzeljg: yes
19:54:04 <dobedobedoh> the % translated isn't really accurate... I've only done <.5% of UK english because the rest is fine inheritted
19:54:19 <dobedobedoh> Which looks bad for anyone wanting to download it as they think it's not done
19:54:38 <fmarier_> dobedobedoh: right, i guess that one is a special case
19:54:54 <dobedobedoh> Same probably goes for US english, but yeah - they are special cases
19:55:05 <bugg_home> should we be moving on?
19:55:10 <fmarier_> you could just repeat the en_NZ strings though
19:55:29 <fmarier_> if they are the same, there's no harm in doing it and it shows you have thought about it
19:56:03 <dobedobedoh> We should probably come back to this later
19:56:14 <dobedobedoh> If we're doing a separate topic on it ;)
19:56:48 <rkabalin_> #topic Bug status fields [Francois and Ruslan] (part 2)
19:57:40 <fmarier_> so rkabalin_ and I have been revising the wiki page for bug statuses
19:57:59 <fmarier_> and we just wanted to draw attention to the use of milestones
19:58:07 <fmarier_> #link https://wiki.mahara.org/index.php/Developer_Area/Bug_Status
19:58:33 <fmarier_> the most important one is to set a milestone on the bugs that you mark as "fix committed"
19:58:51 <fmarier_> that milestone should be the first release that the bugfix will be in
19:59:03 <fmarier_> e.g. 1.5.0 for things merged onto master
19:59:09 <fmarier_> 1.4.1 for things merged onto 1.4_STABLE
19:59:33 <fmarier_> if you leave the milestone out, then the bug is essentially lost and stuck in the "fix committed" state forever
19:59:47 <dobedobedoh> Is it okay to 'request' a milestone before the fix is committed (where committed == merged and not just pushed to gerritt)
19:59:50 <fmarier_> because it doesn't show up on the list of things we need to switch to "fix released" when the release goes out
20:00:12 <fmarier_> dobedobedoh: yeah that's fine. once you've started working on it, it's fair game to put a milestone on it
20:00:36 <rkabalin_> basically, when a bug is in the "fix committed" state, THE MILESTONE IS A MUST
20:00:39 <fmarier_> also if you really want to do something for 1.5, then you can mark it as such
20:00:58 <dobedobedoh> Shame there's no way of enforcing it in launchpad - worth submitting a feature request to them?
20:01:10 <rkabalin_> otherwise, we will loose the change in the release
20:01:54 <fmarier_> dobedobedoh: either that or an easy way to search for: "fix committed" & "no milestone"
20:02:06 * hughdavenport likes how there is no way to use lp's search to see what is commited with no milestone :P
20:02:14 <fmarier_> at the moment, all you can do is search for "fix committed" and go through all of the bugs one by one
20:02:29 <dobedobedoh> ouch
20:02:35 <fmarier_> (which I have done, so we're all good now)
20:02:52 <fmarier_> I've found a few that went into 1.4.0 though :)
20:03:08 <richardm> well, we shouldn't lose the changes exactly, if we look through the git log to write release notes
20:04:06 <fmarier_> the changes aren't lost, but the bug reports themselves are lost in the sense that they stick around in the open state for a long time after they've been resolved
20:04:20 <rkabalin_> yep
20:04:34 <fmarier_> anyways, that's all I wanted to bring up. have a look at the wiki page and hopefully it makes more sense now
20:04:47 <fmarier_> thanks to rkabalin_ for reviewing it and rewriting large chunks of it with me
20:05:00 <fmarier_> rkabalin_: did you have anything else to add?
20:05:15 <rkabalin_> not really
20:06:07 <rkabalin_> we are pretty efficient today
20:06:14 <rkabalin_> ))
20:06:34 <rkabalin_> shall we discuss langpacks as a separate topic?
20:06:50 <anzeljg> and I also have two things...
20:06:55 * dobedobedoh hasn't got much more to add on the langpacks issue
20:07:02 <rkabalin_> or there is not much to add
20:07:03 <fmarier_> does anybody have anything to add to the langpack discussion? (I don't)
20:07:18 <iarenaza> fmarierjust a question
20:07:19 <dobedobedoh> I also have an update on the phpunit status
20:07:37 <fmarier_> iarenaza: sure, go ahead
20:07:38 <iarenaza> are help files available at launchpad for translation?
20:07:43 <anzeljg> yes
20:07:58 <fmarier_> i think richardm put them in a string each, right?
20:08:14 <fmarier_> (one string per help file)
20:08:14 <rkabalin_> # lanpacks (unplanned topic)
20:08:23 <rkabalin_> #topic lanpacks (unplanned topic)
20:08:52 <richardm> iarenaza: Yes, the help files are in the launchpad translations
20:09:01 <bugg_home> rkabalin_, you mean langpacks? ;)
20:09:04 <iarenaza> ok, thanks :)
20:09:13 <rkabalin_> #undo
20:09:13 <maharameet> Removing item from minutes: <MeetBot.items.Topic object at 0x96f7eac>
20:09:21 <rkabalin_> #topic langpacks (unplanned topic)
20:09:27 <rkabalin_> thanks bugg_home
20:09:30 <dobedobedoh> hehe :)
20:09:30 <bugg_home> np
20:09:36 <fmarier_> richardm: each help file is one big blob of HTML?
20:09:45 <richardm> Yes
20:09:48 <dobedobedoh> it's a little unpleasant to edit
20:10:18 <richardm> Well, you can download a .po file, edti that & upload it to launchpad again
20:10:40 <rkabalin_> what I would like to ask is how permissions work, it looks like I ma not able to upload .po if I am not an admin
20:10:43 <fmarier_> which i think is what mits is doing
20:11:14 <anzeljg> richardm: in light of translations, what is the status of PluralForms?
20:11:20 <richardm> I'm not sure exactly how the permissions work, but if mits is doing it it means people in the appropriate translation groups have permission
20:11:33 <richardm> translation teams i mean
20:11:49 <rkabalin_> that might work for the files that were downloaded from launchpad
20:11:55 <richardm> anzeljg: The status is I haven't started it yet I'm afraid
20:12:07 <anzeljg> ok, leave it for now...
20:12:10 <richardm> I need to hack something into the scripts that create langpacks from .po files
20:12:59 <fmarier_> (for the benefit of those who don't know what PluralForms are about, in sl, words have different endings depending on how many things there are: 1, 2, 3, etc.)
20:13:22 <fmarier_> so it's not just singular v. plural, it's like singular, plural2, plural3, or something like that
20:13:30 <anzeljg> singular
20:13:42 <anzeljg> plural for 2, 102, etc. things
20:13:56 <anzeljg> plural for 3, 4, 103, 104, etc. things
20:14:06 <anzeljg> and finally plural for 0, 5, 6, ... things
20:14:21 <anzeljg> ;)
20:14:25 <fmarier_> rkabalin_: does russian have the same problem?
20:14:31 <rkabalin_> yep
20:14:34 <richardm> rkabalin_: you can't even upload .po files as a member of ~mahara-lang?
20:14:42 <rkabalin_> there are "many" plural forms ))
20:14:56 <anzeljg> and czech, slovak, croatian, polish, serbian, arabic,...
20:15:22 <fmarier_> rkabalin_: so you'll be interested in whatever solution we come up with, i guess :)
20:16:43 <rkabalin_> richardm: I think I am able to upload only that .po which I initailly downloaded from launchpad, so it has some headers. If I use the conversion script to porduce .po from mahara lang dir, this fill will not be accepted
20:16:50 <rkabalin_> s/fill/file
20:17:28 <rkabalin_> anyway, that is not a problem any more
20:17:54 <richardm> Yes, it does seem you have to get the headers right for launchpad
20:17:55 <rkabalin_> since the package is converted already
20:18:21 <rkabalin_> but, might be an issue for someone
20:18:58 <richardm> I got them wrong a few times and ended up being the author of the Maori translation inlaunchpad. embarrassing.
20:19:13 <rkabalin_> ok, shall we move on then
20:19:40 <fmarier_> richardm: i was thinking about that the other day. could you not change the translation and then resubmit another one with Ian's name?
20:19:51 <fmarier_> (if you wanted to restore the right authorship info)
20:20:03 <richardm> fmarier_: it doesn't work, once the strings are in
20:20:28 <fmarier_> but if you modify a string, surely it must change the author?
20:20:36 <richardm> My problem was I initially submitted them with the wrong 'context' and when i corrected that i mistakenly left the author off
20:20:48 <rkabalin_> but if you remove the langpack completely and create it again?
20:21:06 <richardm> rkabalin_: i didn't try that
20:21:15 <fmarier_> rkabalin_: that wouldn't work because launchpad keeps all the translations to share them with other projects I think
20:21:22 <richardm> fmarier_: and yes that would work provided you changed all the strings
20:21:41 <richardm> i'll try it
20:21:42 <fmarier_> but what i was thinking was this: prepend all of the strings with a period and upload under your name
20:21:50 <fmarier_> then remove the period and reupload under Ian's name
20:21:51 <richardm> yeah i bet that would work
20:23:36 <richardm> ..
20:24:26 <rkabalin_> #topic Next meeting
20:25:31 <rkabalin_> I propose Oct 5th 7:30 UTC
20:26:23 <anitsirk> works for me
20:26:30 <dobedobedoh> wfm
20:26:41 <fmarier_> wfm2
20:26:43 <bugg_home> hah
20:26:48 <anzeljg> i'll try to attend
20:26:54 <bugg_home> that's my birthday xP
20:26:59 <hughdavenport> sounds good
20:27:06 <iarenaza> wfm3
20:27:08 <anzeljg> happy birthday in advance  ;)
20:27:10 <hughdavenport> happy bday for a month bugg_home
20:27:24 <bugg_home> hughdavenport, you'll probably be there :P
20:27:48 <bugg_home> that's 19:30 our time eh?
20:27:50 <fmarier_> bugg_home: only if he cares about you more than Mahara :P
20:27:55 <rkabalin_> yep
20:28:27 <hughdavenport> hmm, that is a toss up :P
20:28:30 <bugg_home> fmarier_, if I do anything you might be invited too ;P
20:28:36 <rkabalin_> #info next meeting Oct 5th 7:30 UTC
20:29:13 <fmarier_> #link http://www.timeanddate.com/worldclock/fixedtime.html?iso=20111005T0730
20:29:22 <fmarier_> that's actually 20:30 NZ time btw
20:29:28 <fmarier_> looks like daylight saving is kicking in
20:29:33 <rkabalin_> thanks fmarier_
20:29:48 <fmarier_> 08:30 London time
20:30:02 <rkabalin_> can't easily find that page to generate new
20:30:04 <dobedobedoh> line in!
20:30:07 <dobedobedoh> lie in*
20:30:15 <rkabalin_> #topic Any other business
20:30:25 <dobedobedoh> Can I just give a quick update on unit testing ?
20:30:30 <rkabalin_> yep
20:30:30 <fmarier_> rkabalin_: from the homepage: Time Zone Calculators | Event Time Announcer
20:30:40 <anzeljg> as I said before, I got two things
20:30:44 <rkabalin_> fmarier_: thanks
20:30:59 <fmarier_> i was just going to say, let's not forget anzeljg's items :)
20:31:18 <anitsirk> sorry guys. gotta go to another meeting. have a great day / evening.
20:31:25 <anzeljg> bye
20:31:36 <rkabalin_> bye
20:31:40 <hughdavenport> that will be me off as well, will read later
20:31:47 <iarenaza> byt hughdavenport
20:31:48 <anzeljg> dobedobedoh: you can go first ;)
20:31:57 <dobedobedoh> heh tah
20:32:22 <dobedobedoh> Basically, I've updated the patches from mjollnir`
20:32:25 <dobedobedoh> They're in gerrit
20:32:45 <dobedobedoh> There are about 6 commits with a total of 5 bugs in launchpad - you may have seem them yesterday/today
20:33:09 <fmarier_> i saw the bugs, haven't had time to look at the code yet
20:33:10 <dobedobedoh> I've also updated the wiki with some thoughts on the jenkins side: https://wiki.mahara.org/index.php/Talk:Developer_Area/Unit_Testing
20:33:40 <rkabalin_> #info dobedobedoh  updated the patches from mjollnir`
20:33:41 <dobedobedoh> It mostly works so far :)
20:33:51 <rkabalin_> #link  https://wiki.mahara.org/index.php/Talk:Developer_Area/Unit_Testing
20:34:07 <fmarier_> having a CLI installer would be wonderful
20:34:11 <dobedobedoh> I've hit a few bugs when things get in a slightly upset state - they're mentioned above
20:34:15 <fmarier_> especially for the packages
20:34:16 <dobedobedoh> I've done a proof of concept
20:34:29 <dobedobedoh> it's really easy now thanks to mjollnir`'s work
20:34:35 <dobedobedoh> and a CLI upgrader would also be ace
20:34:53 <dobedobedoh> The patches are all tagged phpunit: https://reviews.mahara.org/#q,status:open+project:mahara+branch:master+topic:phpunit,n,z
20:34:57 <dobedobedoh> ..
20:35:21 <fmarier_> big thanks to dobedobedoh for picking that up!
20:35:43 <fmarier_> and for Mjollnir` of course for writing the original :)
20:35:58 <rkabalin_> thanks dobedobedoh, that is a great job
20:36:46 <richardm> yeah, looks great dobedobedoh, so all the patches are done?
20:36:48 <richardm> :)
20:36:52 <dobedobedoh> RI think so
20:37:10 <dobedobedoh> Or tahret, I thought so... I just came across some kind of bug which I've been looking at for the past few minutes
20:37:14 * dobedobedoh is multitasking
20:37:48 <dobedobedoh> But yes, it's more or less done with those caveats
20:37:59 <dobedobedoh> I'll submit an update to the final patch later this week
20:38:13 <dobedobedoh> There's something wrong with the view tests... they were all working, but I've somehow broken them
20:38:31 <dobedobedoh> I think I know what I did though
20:38:32 <dobedobedoh> ..
20:39:33 <dobedobedoh> That's me done though :)
20:39:36 <rkabalin_> we should probably go towards using unit testing more actively
20:40:03 <fmarier_> having unit tests would be really good
20:40:47 <richardm> dobedobedoh: once the patches are merged, can you add some stuff to the page about how to run them?
20:41:03 <dobedobedoh> heh - I've just found my bug. Accidentally removed the tear down in my last tidy up before usbmitting :|
20:41:17 <dobedobedoh> Sure - There's a basic readme, so I'll add some notes from there
20:41:29 <dobedobedoh> There's also another page on the wiki https://wiki.mahara.org/index.php/Developer_Area/Unit_Testing
20:41:35 <dobedobedoh> That has some notes on how to run them
20:41:36 <rkabalin_> we might discuss it in details in the future and even introduce some policy
20:41:36 <dobedobedoh> ..
20:41:45 <richardm> ah ok cool :)
20:42:30 <rkabalin_> any other things to discuss?
20:42:55 <iarenaza> anzeljg had a couple of things
20:43:40 <anzeljg> Are you waiting for me? Can I go?
20:44:01 <dobedobedoh> I'm done so feel free to move on
20:44:04 <anzeljg> Before I begin - when I was back to school, I got "Mahara 1.4 release crew" mug, and I would like to thank a person, who sent it to me...
20:44:23 <rkabalin_> ok, I finish it then
20:44:40 <anzeljg> First thing: survey artefact -  for creating/using custom surveys/questionnaires
20:44:54 * rkabalin_ ignore my last line
20:45:02 <anzeljg> I've almost done it, you can see/try it at:
20:45:14 <anzeljg> #link http://mahara.ledina.org/DEV/
20:45:22 <anzeljg> user: test, pass: test140test
20:46:00 <anzeljg> basically a user chooses survey and other users(tipically tutors), who will gain the access to his/her results
20:46:24 * dobedobedoh sees samwisemoss's influence in there
20:46:31 <anzeljg> user than answers to all the questions and gets back the results, graph, etc.
20:46:37 <rkabalin_> #info anzeljg's survey artefact -  for creating/using custom surveys/questionnaires are available for testing
20:47:11 <anzeljg> the results can be embeded to view/page with appropriate blocktype
20:47:20 <rkabalin_> that is cool
20:47:46 <anzeljg> there is also a possibility to (currently) export results from other users (that one got the acces to) to CSV file
20:48:05 <anzeljg> in the future I also wanted to add an online report kind of thing...
20:48:18 <dobedobedoh> That looks very cool. LEAP2A would also be cool there
20:48:20 <dobedobedoh> but very mahara specific
20:48:26 <anzeljg> for the graphs I've user pChart 2
20:48:37 <anzeljg> #link http://www.pchart.net/
20:48:59 <anzeljg> s/user/used
20:49:53 <anzeljg> dobedobedoh: there were other influences...
20:50:45 <dobedobedoh> heh yeah - just saw an archery page
20:51:25 <fmarier_> anzeljg: so how does one fill out a survey?
20:51:43 <fmarier_> i'm trying it out and have found the survey results block
20:51:44 <anzeljg> 1. click Add Survey button
20:52:06 <fmarier_> ah i see, from Content...
20:52:09 <anzeljg> 2. Select survey in drop-down box (and foreign language if it is a language related survey)
20:52:18 <anzeljg> oh you mean that... ;)
20:52:26 <anzeljg> 3. Fill out survey
20:52:41 <anzeljg> 4. Click Save button and get the results...
20:53:34 <fmarier_> very cool!
20:53:46 <anzeljg> I've also played with help (extended it). You can see it, if you click to "Listening comprehention" with slovenian flag and click first help icon
20:53:53 <richardm> Very cool indeed
20:53:58 <anzeljg> it is extended to allow embedding audio files...
20:54:32 <dobedobedoh> Cool
20:54:59 <anzeljg> like: {audio:test.mp3} inside ordinary HTML help file
20:55:13 <fmarier_> right, if I had Flash I guess I'd be hearing something :)
20:55:47 <anzeljg> somesurveys are complete and someare in test phase,so...
20:56:37 <anzeljg> I need to write the documentation for other on how to create custom surveys...
20:56:42 <rkabalin_> yeah, that is quite cool, thanks anzeljg
20:56:48 <dobedobedoh> thanks anzeljg :)
20:57:20 <anzeljg> that's about it
20:57:28 <fmarier_> anzeljg: would it make sense to look into merging your help file changes upstream?
20:57:50 <anzeljg> it might...
20:57:52 <dobedobedoh> is there anything to gain by moving the help file audio change from flash to flowplayer?
20:58:07 <fmarier_> dobedobedoh: no, flowplayer is flash as well
20:58:23 <dobedobedoh> Ah - I thought it had HTML5 support too?
20:58:27 <fmarier_> it would be nice however to start using HTML5 stuff
20:58:28 <anzeljg> but I've started with the idea, that each survey is self-contained
20:58:43 <anzeljg> at first there were only .xml files for each survey...
20:58:45 <iarenaza> really cool stuff anzeljg :)
20:59:07 <anzeljg> but realized that surveys can containf image files, audio files, help files...
20:59:25 <anzeljg> help files are in different places than in ordinary mahara, so...
20:59:47 <fmarier_> ah i see, they're not really help files as such, they're part of the survey
21:00:03 <fmarier_> was there anything you need to modify in core to get all of this working?
21:00:08 <anzeljg> basically, for each survey there is a .xml file in htdocs/aretfact/survey/surveys
21:00:23 <anzeljg> e.g. test.survey.xml
21:00:49 <anzeljg> there is also a folder, e.g. test.survey (without xml extension) that contain all help files
21:01:25 <anzeljg> help files are in htdocs/artefact/survey/surveys/test.survey/en.utf8 (or sl.utf8)
21:02:07 <anzeljg> not in core directly, just used core components and added some stuff
21:02:22 <anzeljg> last line was response to fmarier_
21:02:23 <richardm> do we need to find a place in dataroot to store that stuff?
21:03:11 <anzeljg> if admin wants to add a custom survey, he/she must add it to htdocs/artefact/survey/surveys folder
21:03:28 * dobedobedoh thinks that'd probaby be more futureproof to allow admin to upload a survey
21:03:41 <anzeljg> I mean, e.g. test.survey.xml and test.survey folder (with all the images, audio and help files)
21:03:52 <dobedobedoh> or the institution admin for isolated institutions
21:04:28 <dobedobedoh> And also to ease things for larger sites running multiple frontends and  package managed code-bases
21:05:09 <anzeljg> I've missed that one: to allow admin to upload surveys into htdocs... or dataroot?
21:05:19 <dobedobedoh> into dataroot
21:05:33 <dobedobedoh> e.g. an admin can upload a zip file containing the datastructure (relatively easy to test)
21:05:42 <dobedobedoh> and then that'd be stored in the dataroot too
21:05:44 <dobedobedoh> rather than webroot
21:05:55 <anzeljg> and also unzipped?
21:06:06 <dobedobedoh> sorry, yes - unzipped somehow
21:06:32 * dobedobedoh hasn't thought it throughf ully, but IMO per-site data shoud be in the dataroot
21:06:46 <anzeljg> so all the surveys should be in the dataroot, including the ones I (or others) intend to ship with the survey artefact?
21:06:57 <dobedobedoh> ah...
21:07:07 <dobedobedoh> maybe a combination of the two?
21:07:34 <anzeljg> ok. I see I will have to add that option (for uploading custom surveys...)
21:07:52 <dobedobedoh> It looks really cool though :)
21:07:57 <anzeljg> thanks
21:08:21 <dobedobedoh> ..
21:08:29 <rkabalin_> ok, what I have missed is a chair for next meeting
21:08:55 <anzeljg> oh, one more thing, the when using blocktpyes, user can select matching pallete for graph, so it matches the selected page theme (aqua, default, sunset, etc.)
21:09:13 <anzeljg> ..
21:09:23 <rkabalin_> any volunteers to chair next meeting?
21:09:42 * dobedobedoh can do it
21:10:20 <rkabalin_> thanks dobedobedoh
21:10:37 <rkabalin_> ok, we have done it
21:10:41 <rkabalin_> #endmeeting