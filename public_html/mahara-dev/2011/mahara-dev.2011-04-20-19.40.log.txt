19:40:32 <fmarier_> #startmeeting
19:40:32 <maharameet> Meeting started Wed Apr 20 19:40:32 2011 UTC.  The chair is fmarier_. Information about MeetBot at http://wiki.debian.org/MeetBot.
19:40:32 <maharameet> Useful Commands: #action #agreed #help #info #idea #link #topic.
19:40:48 <fmarier_> #topic Meeting attendees
19:40:54 <fmarier_> #info fmarier_ is Francois Marier
19:40:59 <anitsirk> #info anitsirk: Kristina Hoeppner, Catalyst IT, New Zealand
19:41:15 <rkabalin> #info rkabalin: Ruslan Kabalin, LUNS Ltd.
19:41:51 <richardm> #info richardm Richard Mansfield
19:42:15 <fmarier_> that looks like us so far
19:42:35 <anitsirk> that'll be the quickest meeting then.
19:42:35 <fmarier_> #topic Meeting etiquette
19:42:49 <fmarier_> that's a quick one that i wanted to cover before we start
19:43:05 <fmarier_> basically it's just a suggestion based on something i saw in the ubuntu irc meetings
19:43:19 <fmarier_> if you are writing something which takes a bit of time to explain
19:43:26 <fmarier_> i.e. a couple of lines of text
19:43:40 <fmarier_> then please say ".." on a line by itself when you are finished
19:44:00 <fmarier_> that way people can avoid interrupting and we also know when you're done so we don't wait
19:44:05 <fmarier_> :)
19:44:07 <fmarier_> ..
19:44:09 <anitsirk> sounds good.
19:44:19 <richardm> excellent
19:44:22 <fmarier_> i thought it was quite a handy shortcut
19:44:22 <rkabalin> ok
19:44:33 <fmarier_> #info typing .. indicates that you are finished with your input
19:44:38 <fmarier_> ^^^ for the minutes
19:44:51 <fmarier_> alright, that's all i wanted to say about this
19:45:06 <fmarier_> moving on to the next thing on the agenda
19:45:11 <fmarier_> #topic Items from previous meetings
19:45:27 <fmarier_> i'll start with the items from people who are here :)
19:45:41 <fmarier_> #info rkabalin_ file a wishlist bug on the tracker with objectionable content report items
19:46:20 <rkabalin> I did not do that
19:46:26 <rkabalin> sorry
19:46:35 <fmarier_> ok, what was it about? I can't remember
19:46:57 * rkabalin looking through log
19:48:25 <rkabalin> right, is it just a bug that refelcts my ideas I summarised on the wiki so that we would not forget to implement it
19:48:53 <rkabalin> ..
19:49:24 <fmarier_> alright, so it mostly just needs a link to the wiki I guess?
19:49:48 <rkabalin> yep, I will do that
19:50:37 <fmarier_> excellent
19:50:43 <rkabalin> basically three bugs - for the forum, wall and blog comments
19:50:49 <fmarier_> you can put it here: https://bugs.launchpad.net/mahara/+bug/767569
19:51:00 <rkabalin> ok
19:51:35 <fmarier_> #action rkabalin to fill in more details about improving objectionable content
19:51:39 <fmarier_> #link https://bugs.launchpad.net/mahara/+bug/767569
19:51:52 <fmarier_> #info anitsirk proceed with asciidoc trial and come up with more info
19:52:06 <rkabalin> thanks fmarier_
19:52:16 <fmarier_> anitsirk: that one is in progress I believe?
19:52:45 <anitsirk> yes. it is
19:52:57 <fmarier_> it's also here on our list of current tasks
19:53:03 <fmarier_> #link http://wiki.mahara.org/Developer_Area/Current_Tasks
19:53:12 <anitsirk> thanks for putting it there.
19:53:25 <anitsirk> I'll put it on the agenda when I have more info / a plan
19:53:45 <fmarier_> ok, sounds good, no need to make it an action item then
19:53:53 <anitsirk> no. :-)
19:54:00 <fmarier_> rangi are you awake / online ?
19:54:17 <anitsirk> he's always online. might still be on the bus.
19:54:39 <fmarier_> he's got an android phone, that's not a good reason to miss a meeting ;-)
19:54:50 <fmarier_> #info fmarier email wiki contributors to start the relicensing process
19:55:10 <fmarier_> i have emailed people, got lots of replies back
19:55:12 <anitsirk> let's park this item ftm. he usually says hi when he comes in.
19:55:21 <fmarier_> all of them said "yes, i agree"
19:55:38 <fmarier_> however, i haven't heard back from everyone yet
19:55:49 <fmarier_> i'm thinking of pinging them one last time
19:55:56 <rkabalin> sounds good
19:56:11 <fmarier_> and then changing the license
19:56:26 <fmarier_> we can always take people's contributions out later if they oppose this
19:56:27 <fmarier_> ..
19:57:11 <rkabalin> yeah, I agree
19:57:24 <fmarier_> Ubuntu has done the same thing too
19:57:31 <fmarier_> #link http://popey.com/blog/2011/03/08/ubuntu-wiki-relicensing-request-for-comments/
19:57:56 <fmarier_> basically, with a large enough group of people it's pretty hard to get a response from everyone
19:58:19 <fmarier_> #action fmarier_ to ping wiki contributors one last time and then make the license change
19:58:21 <anitsirk> that's a good plan. we can't wait forever. thanks for contacting everyone, fmarier_ That's a big help for the documentation later on.
19:58:37 <rkabalin> yes, thanks fmarier_
19:58:50 <fmarier_> ok, that's it for this topic
19:59:28 <fmarier_> #topic Update on 1.4 [Francois]
20:00:11 <fmarier_> due to a late start in getting Gerrit going (and having to deal with two security releases), we haven't had as much time as we needed to go through the bug list
20:00:22 <fmarier_> so we're thinking of releasing in early May
20:00:48 <fmarier_> richardm and i will also have a good look through the bugs targetted for 1.4 to try and find the ones that can be postponed
20:01:03 <fmarier_> (we don't want to delay the release for too long)
20:01:06 <fmarier_> ..
20:01:54 <fmarier_> also it looks like we'll have a stable release (the last one for 1.2) just before 1.4 comes out (maybe something like 2 days before)
20:02:20 <fmarier_> ..
20:02:44 <rkabalin> sounds good. I think I will take httpswwwroot stuff and complete it by release
20:02:59 <rkabalin> if noone objects
20:03:09 <fmarier_> of course, I should say that everybody's help is welcome :D if there's a bug you particularly care about for 1.4, then now is the time to do it :)
20:03:16 <fmarier_> rkabalin: that sounds good
20:03:17 <richardm> yeah I think everyone should fix their pet bugs soon
20:03:46 <fmarier_> alright, let me summarize this for the minutes
20:04:04 <fmarier_> #info release is now scheduled for early May
20:04:30 <fmarier_> #info bugs targetted for 1.4 will be triaged again to bring the list down if possible
20:04:46 <fmarier_> #info everyone should fix their pet bugs soon :)
20:05:19 <fmarier_> does anybody have anything to add or should we move on to the next topic ?
20:05:50 <rkabalin> nothing from me, everything sounds fine
20:05:51 <anitsirk> moving on is fine. just wanted to thank you all who look into the bugs that i report. lordp fixed one of my pet bugs. :-)
20:06:09 <fmarier_> anitsirk: the warning about deleting a view?
20:06:18 <anitsirk> yes
20:06:27 <anitsirk> alan started it, i think and it looks good now
20:06:48 <fmarier_> cool
20:06:50 <rkabalin> that user renaming stuff he is doing is also great
20:07:32 <fmarier_> it's pretty cool how it's easier to get a feel for what's going on now when you have to review the code that gets in :)
20:07:56 <fmarier_> which incidently brings us to our next topic:
20:08:05 <fmarier_> #topic Gerrit discussion [Francois]
20:08:27 <fmarier_> first of all, now that we've used it for a week or two, what are people's opinion of Gerrit?
20:08:41 <richardm> I like it so far
20:08:48 <rkabalin> I like it a lot!
20:09:00 <richardm> It's still in its honeymoon period
20:09:05 <fmarier_> hahaha
20:09:25 * fmarier_ refrains from making jokes about the marriage being consumed
20:09:30 <rkabalin> quite useful, though requires good understanding of git
20:10:02 <richardm> I'm a bit worried about what's going to happen when we're putting big features in that require loads of patches
20:10:05 <rangi> fmarier_: i am now
20:10:07 <richardm> But so far it's good
20:10:07 <rangi> whats up?
20:10:42 <fmarier_> hi rangi can we get back to you after the current topic?
20:10:57 <fmarier_> i have no idea how to switch topics and comeback with meetbot
20:11:16 <fmarier_> i'm afraid our minutes will be totally screwed if we do :)
20:11:36 <anitsirk> i don't think we can
20:11:42 <richardm> So do we need to gush lovingly about gerrit for a bit longer?
20:11:48 <fmarier_> richardm: yeah, i think we'll have to break these big features down
20:12:00 <fmarier_> into a number of patches
20:12:11 <richardm> yeah, we'll have to
20:12:12 <fmarier_> but that's also the way that bigger projects do it
20:12:17 <fmarier_> like Linux or Koha
20:12:47 <fmarier_> i guess we'll just have to wait and see
20:12:48 <fmarier_> ..
20:13:10 <rangi> fmarier_: yep
20:13:47 <rangi> fmarier_: http://bugs.koha-community.org/bugzilla3/showdependencytree.cgi?id=5575&hide_resolved=0
20:13:50 <rangi> how we do it for koha
20:13:51 <rkabalin> moreover, you do not have to push the whole feature at the same time, you may add dependencies later and group patches logically using "topic"
20:14:51 <anitsirk> #info how koha handles big features with gerrit: http://bugs.koha-community.org/bugzilla3/showdependencytree.cgi?id=5575&hide_resolved=0
20:15:03 <fmarier_> rangi: so basically you break it down into lots of smaller features
20:15:04 <rkabalin> ..
20:15:55 <rangi> yup
20:16:13 <fmarier_> one thing i want to get out of this topic is a list of what people think are good ways of working with gerrit
20:16:17 <fmarier_> the first one seems to be:
20:17:05 <fmarier_> #info if you want to merge a big feature, break it down into lots of smaller ones (maybe dependencies first, etc.)
20:17:25 <fmarier_> and rkabalin mentioned using gerrit tags as well
20:17:32 <fmarier_> so i guess we could say this:
20:17:39 <fmarier_> #info When pushing patches that are related, use a Gerrit tag
20:17:46 <fmarier_> anything else?
20:17:46 <fmarier_> ..
20:18:41 <fmarier_> one technical point would be to not review your own code (i.e. not give it a +2) I guess
20:18:50 <rkabalin> #info every new patch should be applied on top of the clean checked-out branch to avoid adding dependencies in error
20:19:20 <fmarier_> rkabalin: good point about rebasing before you submit a new or revised patchset
20:19:47 <richardm> When reviewing a series of related patches, consider stopping at the first reject, because everything after that will have its reviewed status overwritten
20:20:12 <richardm> ... and you'll have to review the rest again even if they haven't changed
20:20:16 <fmarier_> richardm: yeah unless you have more things to reject I guess
20:20:16 <richardm> ..
20:20:22 <richardm> yep
20:21:12 <fmarier_> #info when reviewing a series of related patches, keep in mind that the patches after the first rejected one will need to be reviewed again
20:21:58 <fmarier_> while it's not right to review your own code (that defeats the purpose of doing code reviews) i think it's fine to mark it as verified once you have tested it. what do others think?
20:22:02 <fmarier_> ..
20:22:26 <rkabalin> that is an interesting point
20:23:33 <rkabalin> I thought instead, verification of someone's patch should be done by reviewers
20:24:26 <richardm> That would be ideal, but I don't think we should enforce it
20:24:32 <rkabalin> to avoid errors in master and improve the quality
20:24:33 <rkabalin> ..
20:24:53 <fmarier_> that's a good point, ideally it would be done by a QA team
20:25:08 <fmarier_> you submit your patch, it gets reviewed by another dev
20:25:15 <fmarier_> then it gets tested by qa
20:25:19 <richardm> Some things are going to be too time consuming to test, and we might not have enough people to do it
20:25:25 <fmarier_> and then if it passes everything it makes it there
20:26:00 <fmarier_> but maybe we should focus on reviewing code for now and look at expanding this later?
20:26:24 <fmarier_> i'm just worried about increasing our workload to much by adding lots of tasks all at once
20:26:27 <fmarier_> ..
20:26:29 <richardm> I think at the very least we need a way to say "I want someone else to test this patch"
20:26:47 <rkabalin> yep, probably
20:26:54 <richardm> At the moment people are assuming you'll verify your own code.
20:27:00 <rkabalin> let's postpone it
20:27:24 <fmarier_> richardm: yeah, perhaps we could say that you can verify your own code, but if you want others to do it than say so and leave it as unverified?
20:27:30 <rkabalin> my point here is that gerrit potentially can attract more developers
20:28:37 <rkabalin> and if we had no gerrit, we would use patches instead, so each patch would be tested
20:29:44 <fmarier_> rkabalin: i do like your idea and i do think we should aim at testing our code more
20:30:06 <rkabalin> with gerrit if verification is not done by someone, the probability of having error on master is higher
20:30:24 <rkabalin> than in old workflow
20:30:27 <rkabalin> ..
20:31:10 <fmarier_> well, a patch should be well tested before it's submitted to gerrit though
20:31:22 <fmarier_> otherwise it's wasting reviewers' time
20:31:23 <fmarier_> ..
20:32:14 <richardm> Yes, and if we get new devs on there who don't test their own code beforehand we'll work out who they are pretty quickly
20:33:11 <rkabalin> well, yes
20:33:18 <fmarier_> rkabalin: i don't think we're reducing the amount of patch testing we do compared with what we did before
20:33:51 <richardm> I think we're probably increasing it actually.
20:34:06 <fmarier_> one thing i should mention is the current rules that are built into gerrit:
20:34:21 <fmarier_> #info anybody can submit new change requests
20:34:36 <fmarier_> #info anybody can do a code review (-1 to +1)
20:34:57 <fmarier_> #info only reviewers and testers can mark code as verified (-1 to +1)
20:35:23 <fmarier_> #info only reviewers can do a full code review (-2 to +2) and submit patches for merge
20:35:29 <fmarier_> ..
20:35:47 <fmarier_> and in terms of who is what
20:36:01 <fmarier_> #info old committers are now reviewers
20:36:08 <fmarier_> #info nobody is a tester yet
20:36:43 <fmarier_> #info once jenkins is running, it will act as a tester giving patches a -1 or a +1 (Verified) when tests pass/fail
20:36:47 <fmarier_> ..
20:37:32 <richardm> fmarier_: even though the tests may have nothing to do with the patch?
20:37:33 <rkabalin> I see
20:38:44 <fmarier_> richardm: the +1 verified from jenkins should be treated as an indication of the code passing basic tests, not as the only thing you need before submitting the code
20:39:01 <richardm> Yep that's all we can do with jenkins
20:39:17 <fmarier_> i would say that we should always require a human tester
20:39:27 <fmarier_> even if it's just the patch submitter
20:39:42 <richardm> So it'll +1 verify it, but someone still has to submit manually
20:39:47 <fmarier_> (but only in the case of the patch submitter having the rights to mark things as verifiy)
20:39:48 <richardm> That's cool.
20:39:58 <rkabalin> though I am still a bit sceptic about patch testing amount, you can never guarantee that commiter tested it properly, and you can't interprit all control structures and the code in your mind when you are looking through it
20:40:10 <fmarier_> as stated above, random people can't mark things as verified, they have to be reviewers or testers
20:40:13 <fmarier_> ..
20:40:15 <rkabalin> jenkins will probably solve it
20:40:50 <fmarier_> rkabalin: that's true, but if you look at the people who can mark things as verified, they were already able to commit straight into master without any testing
20:40:58 <fmarier_> so we've already improved on that
20:41:00 <richardm> rkabalin: it all depends on how many tests it does
20:41:04 <fmarier_> ..
20:41:15 <rkabalin> yep, agree
20:42:06 <fmarier_> for example, lordp who is a new contributor (and therefore never had commit rights) cannot mark things as verified so once of us needs to test his changes before they get in
20:42:14 <richardm> rkabalin: As a reviewer, if you see something that looks weird or dodgy it's your responsibility to test it
20:42:33 <fmarier_> #info "Verified" v. "Code review" (verify +1 your own changes, but don't +2 them)
20:42:58 <fmarier_> #info if you want someone to test your code, leave it as "verified: 0" and ask for someone to test it
20:43:17 <fmarier_> #info as a reviewer, if you see something that looks weird or dodgy, it's your responsibility to test it
20:43:33 <fmarier_> is that a good summary? ^^^^
20:43:34 <fmarier_> ..
20:43:36 <rkabalin> that sounds good
20:44:42 <fmarier_> btw rkabalin thanks for being so involved in the code reviews, it's been really fun to leave in the evening with some outstanding patches and arrive in the morning with reviews already done :)
20:45:06 <fmarier_> for once timezone issues are actually acting in our favour
20:45:06 <rkabalin> the same for me ;)
20:45:06 <fmarier_> ..
20:45:11 <richardm> Yep, it's been great
20:45:27 <fmarier_> alright, i did have one last thing to suggest
20:45:37 <fmarier_> #info Leave associated bugs as "in progress" until commited by Gerrit
20:45:53 <fmarier_> then of course, you can flick it to "fix committed" once it's been merged by gerrit
20:45:56 <fmarier_> ..
20:46:08 <rkabalin> that might be difficult not to forget
20:46:39 <rkabalin> you have to track that your patch has gone to master and change the bug progress
20:46:47 <fmarier_> if it's targetted to a release, then we'll probably see it when we go through the list
20:46:52 <fmarier_> but yeah, it's not ideal
20:47:24 <fmarier_> maybe i should look at gerrit hooks and see if i could send an email to launchpad to flick it automatically once it's been merged
20:47:32 <fmarier_> ..
20:47:58 <richardm> Yep, or just remind the committer/submitter by email
20:48:08 <richardm> (there might be 2 commits for 1 bug)
20:48:16 <fmarier_> ah true
20:48:45 <fmarier_> i guess all a hook could do is add a note to the bug saying that a changeset was merged into master
20:49:00 <fmarier_> it doesn't know whether or not the bug is completely done
20:49:34 <rkabalin> a reminder might be better
20:49:59 <fmarier_> #action fmarier_ to investigate whether gerrit could have a hook that would add a note to launchpad (with a link to the gerrit changeset) for patchsets that got merged and mentioned a bug number
20:50:12 <fmarier_> rkabalin: what sort of reminder?
20:50:48 <rkabalin> that your patch has been approved, please update launchpad status if any
20:51:50 <fmarier_> #action fmarier_ investigate hook to send an email to patchset submitter once it's been merged (to remind them to update bug status in tracker)
20:52:13 <fmarier_> i can't promise i'll do all of that before the next meeting though :)
20:52:23 <anitsirk> a comment that was made on one bug caught my attention. i think the commenter wanted to have the commit path put onto the bug report so that it was easier to find that commit. i think that is a good idea if somebody wants to cherry-pick a certain solution. or is there another easy way of seeing what was done? and I guess also including bug numbers on the commits (if you don't already do that) so that when we prepare the release n
20:52:23 <anitsirk> for the new version we could check bug reports more easily for any discussion that hints at what has changed (as I can't just look at code and know what you did ;-) )
20:52:26 <fmarier_> we have a few 1.4 bugs to fix too :)
20:53:35 <anitsirk> hello iarenaza
20:53:38 <rkabalin> hi iarenaza
20:53:40 <fmarier_> anitsirk: we already include bug numbers in commit messages and the hook i'm proposing will include a link to the patches that were committed related to a bug
20:53:40 <iarenaza> hi
20:53:51 <iarenaza> sorry for being so late
20:53:51 <fmarier_> hi iarenaza !
20:53:59 <anitsirk> great. thanks fmarier_
20:54:27 <fmarier_> so anyways, let's finishing that topic, i think it was a very productive discussion
20:54:39 <fmarier_> i'll summarize it on the wiki
20:54:44 <fmarier_> #action fmarier write code review guidelines on the wiki
20:55:37 <fmarier_> lets quickly go back to the items from rangi and iarenaza now they are both here
20:55:54 <fmarier_> #topic Items from previous meetings - Episode 2
20:55:57 <iarenaza> #info iarenaza: Iñaki Arenaza, Mondragon Unibertsiatea
20:56:08 <fmarier_> #info rangi to ask the evergreen guys if they have a test suite for their asciidoc manual
20:56:20 <fmarier_> rangi: did you have a chance to ask them?
20:56:26 <rangi> yes, just did
20:56:30 <rangi> about 40 mins ago
20:56:59 <rangi> 08:18 < rangi> :)
20:57:00 <rangi> 08:18 < rangi> cool, what they were thinking to do, is have their jenkins build the docs and run  tests on them
20:57:03 <rangi> 08:18 < rangi> which sounded doable to me
20:57:05 <rangi> 08:19 < dbs> totally
20:57:17 * anitsirk thinks rangi is awesome because he is in the last stages of releasing koha 3.4 and still makes it to the mahara dev meeting and gets his action items done.
20:57:18 <rangi> they think its a good idea, dont have their buildbot doing it yet
20:57:50 <fmarier_> do they run any manual tests on them?
20:59:00 <rangi> yeah just using testasciidoc
20:59:09 <rangi> http://www.methods.co.nz/asciidoc/testasciidoc.html
20:59:32 <fmarier_> #link evergreen run testasciidoc over their documentation: http://www.methods.co.nz/asciidoc/testasciidoc.html
21:00:11 <fmarier_> which comes with the asciidoc package
21:00:16 <rangi> *nod*
21:00:26 <fmarier_> sounds pretty simple then
21:01:12 <fmarier_> alright, inaki had one item as well...
21:01:22 <fmarier_> #info iarenaza further look at Live@edu accounts
21:01:43 <fmarier_> i assume that was about looking into whether or not we can get a test account?
21:01:53 <iarenaza> fmarier_ I provided accounts to gregor and anitsirk a couple of weeks ago.
21:02:09 <iarenaza> both in google apps for education and live@edu sites
21:02:15 <fmarier_> excellent
21:02:17 <iarenaza> I can provide more on request
21:02:41 <iarenaza> we've got a whole domain for mahara devs :-)
21:02:51 <fmarier_> :)
21:03:10 <fmarier_> we could start a mahara school then!
21:03:19 <anitsirk> :-D
21:03:21 <rkabalin> ;)
21:03:47 <fmarier_> well, since this is sorted, let's move to the last topic
21:03:50 <fmarier_> #topic lang.mahara.org and AMOS roadmap and funding? [iarenaza]
21:04:07 <fmarier_> that was on the schedule 2 meetings ago i believe
21:04:26 * fmarier_ has no idea what it was about though
21:04:36 <iarenaza> I talked to David Mudrak about how much effort (and money) would be to modify AMOS to be used with Mahara
21:04:49 <fmarier_> AMOS = the moodle 2.0 translation thing?
21:04:53 <iarenaza> yes
21:05:30 <iarenaza> and before I start moving things, I wanted to know if there's interest from the project to use it
21:05:58 * fmarier_ hasn't actually seen it
21:06:15 <iarenaza> I could get some funding, but I don't expect it will cover for all the work needed.
21:06:30 <rkabalin> iarenaza: you may ask that in translators forum
21:06:38 <richardm> iarenaza: tricky, we already started converting langpacks into .po format thinking we might use the launchpad translation service
21:07:07 <iarenaza> richardm: that's the sort of statement that clears things a lot :-)
21:07:11 <fmarier_> i was just going to say that richardm has done work on a .po file importer/exporter
21:07:24 <fmarier_> to allow both the existing langpacks and also po files
21:07:39 <fmarier_> (basically it will be up to the translators to use what they prefer)
21:07:48 <richardm> I've done scripts to convert the langpacks in/out of .po, but haven't done the launchpad side of it yet
21:08:01 <fmarier_> but having a po file will enable us to use standard tools (like the translation interface on launchpad)
21:08:19 <fmarier_> so it doesn't prevent us from also using AMOS
21:08:41 <fmarier_> but provides an additional interface for writing translation
21:08:44 <iarenaza> I don't have a strong preference. I know moodle translators are quite happy with AMOS (does all the git stuff by itself, for example)
21:09:31 <rkabalin> just was wondering, can AMOS for example track language-related changes on master and somehow inform translators about what has to be updated?
21:09:33 <iarenaza> but David told me the tool is tied to moodle, and so would need some work to make it work with Mahara
21:09:42 <iarenaza> rkabalin: yes
21:09:50 * rkabalin feels sorry for interupting
21:09:52 <iarenaza> that's part of what it does
21:10:04 <rkabalin> cool
21:10:17 <fmarier_> (the reason we started writing this po file stuff is that we're getting a paid translation to Maori done and po files work much better with existing translation software)
21:11:24 <iarenaza> when you commit changes that touch language strings, you add some special text in the commit message (they are called AMOS scripts) to tell the tool about those changes. They are not needed for new strings, only for changes, deletions, moving strings between language files, etc.
21:11:25 <fmarier_> #info iarenaza has some funding to adapt AMOS (moodle 2.0 translation tool) for Mahara but wouldn't be able to cover the whole thing
21:11:56 <iarenaza> fmarier_ I don't even have a quote for it, because I wanted to ask here first
21:12:03 <fmarier_> #info richardm has implemented a .po file importer/exporter to provide an additional way of translating mahara
21:12:43 <fmarier_> iarenaza: sounds like a pretty clever tool
21:13:18 * iarenaza looking for moodle docs page with AMOS description
21:13:37 <iarenaza> Here it is: http://docs.moodle.org/en/AMOS
21:13:41 <fmarier_> i guess ruslan's suggestion is a good one: checking with the translation forum to see how many of them would like to see AMOS ported
21:13:46 <iarenaza> The translation portal is at lang.moodle.org
21:13:49 <fmarier_> #link AMOS: http://docs.moodle.org/en/AMOS
21:13:53 <iarenaza> ok
21:14:06 <fmarier_> #link Moodle 2.0 translation portal: http://lang.moodle.org
21:14:46 <fmarier_> i suppose it might also be worth waiting to see whether or not translators like the launchpad interface (once we've got it going)
21:15:47 <iarenaza> ok
21:16:21 <fmarier_> iarenaza: what's your feeling on it? do you think we should really try to use AMOS?
21:16:33 <rkabalin> the main problem I guess is inconsistency between EN and other languages once something exiting has changed, it looks like it is solved in AMOS
21:16:36 <richardm> Unfortunately it might be a while before I can get the launchpad stuff going, with the release coming up.
21:16:36 <fmarier_> you're the translation expert after all, i've never done one :)
21:16:50 <rkabalin> s/exiting/ existing
21:16:59 <iarenaza> Anything is better than the current way of dealing with translations :) People have a hard time dealing with git
21:17:26 <fmarier_> rkabalin: yeah what richard and I have started doing is to create new langstrings when the meaning of one changes.
21:17:30 <richardm> Yep, the current way is really putting people off
21:17:43 <fmarier_> that way it's more obvious that there is something new to translate
21:17:46 <iarenaza> I don't do much translation work, I do all the git stuff for the Basque language people, and sometimes for the Spanish people.
21:17:46 <fmarier_> ..
21:18:06 <iarenaza> In fact, we are using David's adminlang patch for the Basque translation
21:18:26 <fmarier_> #info having to use git is putting people off from translating Mahara
21:18:57 <iarenaza> I've got a couple of scripts to find new/obsolete strings and help files, but that's it
21:19:56 <iarenaza> I haven't used the Launchpad translation interface, so I can't compare it to AMOS
21:20:03 <fmarier_> ok, so it sounds to me like there is a real problem there
21:20:19 <fmarier_> i would suggest that we first setup the translating stuff in launchpad
21:20:37 <fmarier_> then based on that, we can talk again about AMOS and whether or not we can try to find funding for it?
21:20:48 <iarenaza> fine with me
21:21:26 <rkabalin> I can imagine, one who is willing to translate rarely has git skills
21:21:33 <rkabalin> yep, that is fine
21:21:44 <fmarier_> #agreed first have a look at launchpad translation tool and discuss whether or not to try to find funding to port AMOS
21:21:58 <fmarier_> cool, thanks for bringing that up iarenaza
21:22:00 <richardm> ok, fmarier_ I will talk to you later today about the launchpad stuff, there are a couple of things we need to decide on before we can start
21:22:22 <fmarier_> it's important to identify areas were there are things blocking potential contributors
21:22:39 <fmarier_> richardm: sounds good
21:22:54 <fmarier_> let's move on to the final item on the agenda
21:23:03 <fmarier_> #topic Next meeting
21:23:21 <fmarier_> who wants to chair the next one?
21:23:31 <fmarier_> iarenaza? anitsirk? richardm? :)
21:24:12 <anitsirk> i could if i'm available at that time
21:24:23 <iarenaza> I guess I'm the only one that hasn't chaired a meeting yet...
21:24:35 <richardm> iarenaza: no you're not
21:24:47 <anitsirk> newbies can of course, go first :-)
21:25:09 <iarenaza> Ok, I'll do it
21:25:20 <fmarier_> iarenaza: thanks!
21:25:25 <rkabalin> thanks iarenaza
21:25:26 <fmarier_> as far as the date is concerned, what about 25 May at 7:30 UTC ?
21:25:38 <rkabalin> sounds fine
21:25:40 <fmarier_> http://www.timeanddate.com/worldclock/fixedtime.html?iso=20110525T0730
21:25:44 * iarenaza now to read the wiki instructions for chairs :-O
21:26:03 <fmarier_> iarenaza: http://wiki.mahara.org/Developer_Area/Developer_Meetings/Chair_Duties
21:26:07 <anitsirk> date and time are fine
21:26:08 <iarenaza> gimme a second to check my schedule
21:26:35 <fmarier_> that's 8:30am for London and 7:30pm for NZ
21:26:52 <iarenaza> that's 9:30am for me
21:27:01 <iarenaza> it's ok,
21:27:13 <fmarier_> richardm?
21:27:35 <richardm> yep
21:27:40 <fmarier_> #agreed Next meeting: 25 May at 7:30 UTC
21:27:48 <fmarier_> #link http://www.timeanddate.com/worldclock/fixedtime.html?iso=20110525T0730
21:28:01 <fmarier_> #topic Any other business
21:28:09 <fmarier_> anything else before we close?
21:28:19 <anitsirk> not from my end
21:28:22 <rkabalin> nothing from me
21:28:34 <iarenaza> nothing else from me
21:29:19 <fmarier_> alright, let's close this meeting then. thanks everyone for coming!
21:29:21 <iarenaza> from the chat logs, today's was supposed to be a short meeting! :-) :-)
21:29:23 <fmarier_> #endmeeting