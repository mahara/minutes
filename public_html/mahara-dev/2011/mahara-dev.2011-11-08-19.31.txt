===================
#mahara-dev Meeting
===================


Meeting started by richardm at 19:31:40 UTC. The full logs are available
at
http://meetbot.mahara.org/mahara-dev/2011/mahara-dev.2011-11-08-19.31.log.html
.



Meeting summary
---------------
* Meeting attendees  (richardm, 19:31:47)
  * richardm is Richard Mansfield, Catalyst, NZ  (richardm, 19:31:55)
  * dobedobedoh is Andrew Nicols - LUNS Ltd, UK  (dobedobedoh, 19:32:09)
  * hughdavenport is Hugh Davenport, Catalyst IT, NZ  (hugh_home,
    19:32:11)
  * pxh is Piers Harding, Catalyst, NZ  (pxh, 19:32:12)
  * dan_p is Dan Poltawski - LUNS Ltd, UK  (dan_p, 19:32:15)
  * rkabalin is Ruslan Kabalin, LUNS Ltd., UK  (rkabalin_, 19:32:17)
  * dajan is Dominique-Alain from Switzerland  (dajan, 19:32:20)
  * anzeljg is Gregor An�elj, developer and translator  (anzeljg,
    19:32:24)
  * anitsirk is Kristina Hoeppner from Catalyst IT, Wellington, NZ
    (anitsirk, 19:32:40)

* Items from previous meeting  (richardm, 19:33:12)
  * dan_p LUNS to investigate adding a new mahara integration project to
    run selenium tests  (richardm, 19:33:22)
  * dobedobedoh to look at modifying release & test running scripts to
    look for tests in the right places  (richardm, 19:39:39)
  * ACTION: dobedobedoh to create a bug for modifying release & test
    running scripts to look for tests in the right places  (richardm,
    19:41:47)

* Starting the discussion about a possible date / month for the Mahara
  1.5 release (Kristina)  (richardm, 19:42:12)
  * IDEA: I just wanted to start the discussion about the release date
    as we are getting close to the end of 2011 and more and more people
    ask about Mahara 1.5 and soon will ask about when it's going to be
    released. As most of our user base is in the northern hemisphere,
    I'd like to see a release date in late spring so that the edu sector
    can upgrade over the northern hemispheric summer as that seems to be
    the best and for many only time to upgra  (anitsirk, 19:42:25)
  * votes for freeze dates: Dec: 3, Jan: 3, Feb: 1, Mar: 3, Apr: 2
    (richardm, 20:03:45)

* Handling security issues (Francois)  (richardm, 20:06:18)
  * click the security checkbox when reporting a security issue on the
    tracker  (richardm, 20:08:05)
  * patches should go as attachments on that private bug instead of
    gerrit  (richardm, 20:08:15)
  * don't discuss it on IRC, the forums, etc. just email
    security@mahara.org  (richardm, 20:08:55)
  * ACTION: dobedobedoh to look at the possibility of adding a hook to
    the Makefile to reject patches with security bugs in the subject
    (richardm, 20:13:08)

* Future possibilities for flexible page designs - layouts and themes
  (Mike Kelly)  (richardm, 20:13:57)
  * waawaamilk reckons we should go for absolute positioning for all
    objects, it's the way of the future  (richardm, 20:17:19)
  * LINK: http://mahara.org/interaction/forum/topic.php?id=3925
    (rkabalin_, 20:20:07)
  * mikekelly_ will put together a test instance with rows in views, or
    absolute positioning of all blocks, if he gets time  (richardm,
    20:26:59)
  * anzeljg is extending custom skins with Google Web Fonts  (richardm,
    20:29:10)
  * everyone wants more flexible page layouts in mahara!  (richardm,
    20:34:22)

* Request for development docs (Dajan)  (richardm, 20:39:26)
  * it would be useful to have up to date docs to talk to conference
    participants, but current info is unstructured & difficult to
    assemble  (richardm, 20:42:29)
  * IDEA: by fmarier_ : link to a list of upcoming features (constantly
    updated release notes) in the quarterly newsletter to keep community
    informed about these  (anitsirk, 20:48:10)
  * we'll try tagging all bugs that should go in the release notes
    (richardm, 20:56:22)
  * dajan to start adding feature tags to bugs in the 1.5 milestone
    (richardm, 21:04:04)
  * everyone to start adding feature tags when setting bugs to fix
    committed (when it's a feature)  (richardm, 21:04:29)
  * ACTION: dajan to report on tagging existing 1.5 features on the
    tracker  (richardm, 21:06:44)

* Next meeting and chair  (richardm, 21:06:53)
  * IDEA: Wednesday 7th December @ 07:30 UTC (07:30 GMT/20:30 NZDT)
    (richardm, 21:07:05)
  * LINK:
    http://www.timeanddate.com/worldclock/fixedtime.html?iso=20111207T0730
    (richardm, 21:07:06)
  * hugh_home to chair the next meeting  (richardm, 21:07:37)
  * IDEA: Tuesday 6th December @ 07:30 UTC (07:30 GMT/20:30 NZDT)
    (richardm, 21:08:25)
  * LINK:
    http://www.timeanddate.com/worldclock/fixedtime.html?iso=20111206T0730
    (fmarier_, 21:09:58)
  * next meeting will be Tuesday 6th December @ 07:30 UTC, chaired by
    hugh_home  (richardm, 21:11:16)

* Any other business  (richardm, 21:12:01)

Meeting ended at 21:18:13 UTC.




Action Items
------------
* dobedobedoh to create a bug for modifying release & test running
  scripts to look for tests in the right places
* dobedobedoh to look at the possibility of adding a hook to the
  Makefile to reject patches with security bugs in the subject
* dajan to report on tagging existing 1.5 features on the tracker




Action Items, by person
-----------------------
* dajan
  * dajan to report on tagging existing 1.5 features on the tracker
* dobedobedoh
  * dobedobedoh to create a bug for modifying release & test running
    scripts to look for tests in the right places
  * dobedobedoh to look at the possibility of adding a hook to the
    Makefile to reject patches with security bugs in the subject
* **UNASSIGNED**
  * (none)




People Present (lines said)
---------------------------
* richardm (108)
* fmarier_ (83)
* anitsirk (50)
* dajan (46)
* dobedobedoh (28)
* hugh_home (27)
* mikekelly_ (26)
* waawaamilk (25)
* dan_p (19)
* pxh (13)
* anzeljg (12)
* rkabalin_ (8)
* _anzeljg_ (7)
* maharameet (6)
* alberto (4)
* elky (1)




Generated by `MeetBot`_ 0.1.4

.. _`MeetBot`: http://wiki.debian.org/MeetBot
