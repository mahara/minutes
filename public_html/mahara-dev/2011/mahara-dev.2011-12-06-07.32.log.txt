07:32:36 <hughdavenport_> #startmeeting
07:32:36 <maharameet> Meeting started Tue Dec  6 07:32:36 2011 UTC.  The chair is hughdavenport_. Information about MeetBot at http://wiki.debian.org/MeetBot.
07:32:36 <maharameet> Useful Commands: #action #agreed #help #info #idea #link #topic.
07:33:00 <hughdavenport_> #info hughdavenport_ is Hugh Davenport and Brett Wilkins from Catalyst IT Wellington
07:33:02 <anitsirk> #info @anitsirk is Kristina Hoeppner, Catalyst IT, NZ
07:33:14 <fmarier> #info fmarier is Francois Marier
07:33:15 <anzeljg> #info anzeljg is Gregor An�elj, ICT teacher, translator, developer
07:33:17 <dobedobedoh> #info dobedobedoh is Andrew Nicols, LUNS Limited, UK
07:33:41 <richardm> #info richardm is Richard Mansfield, Catalyst, NZ
07:33:44 <elky> #info elky is Melissa Draper, Catalyst IT, NZ
07:34:53 <hughdavenport_> anyone else, jimcrib lamiette rkabalin waawaamilk dan_p lordp?
07:35:29 <hughdavenport_> right, we'll get into it then
07:35:46 <hughdavenport_> #topic Items from last meeting
07:35:47 <hughdavenport_> #info dobedobedoh to look at the possibility of adding a hook to the Makefile to reject patches with security bugs in the subject
07:35:56 <elky> please
07:36:00 <elky> please please please :P
07:36:00 <dobedobedoh> I've started taking a look at this
07:36:22 <dobedobedoh> We need to modify the .gitattributes file in the release script to get it to exclude only the test directories containing phpunit tests
07:36:43 <dobedobedoh> I've got it to do that, but I'm also trying to get it to exclude empty 'tests' directories
07:36:43 <elky> eh?
07:36:56 <fmarier> that's the DONE item that hughdavenport_ skipped :)
07:36:57 <dobedobedoh> The packages are created using git archive
07:37:06 <elky> ah
07:37:14 * dobedobedoh needs to read ;)
07:37:17 <dobedobedoh> It's not done though
07:37:17 <hughdavenport_> ah, was I too smart in skipping it :P
07:37:27 <hughdavenport_> oh, shall i go back to that one?
07:37:29 <dobedobedoh> oh - so it is
07:37:34 <elky> so i'm not going more crazy, that's good.
07:37:38 <dobedobedoh> sorry, I was reporting on the actual content of the bug
07:37:38 <hughdavenport_> #undo
07:37:38 <maharameet> Removing item from minutes: <MeetBot.items.Info object at 0x9866a6c>
07:37:38 <fmarier> well, those who want to discuss it can do so on the bug report I guess
07:37:41 * dobedobedoh is going crazy
07:37:41 <hughdavenport_> #item dobedobedoh to create a bug for modifying release & test running scripts to look for tests in the right places. DONE
07:37:45 <fmarier> #link https://bugs.launchpad.net/mahara/+bug/897593
07:37:47 <hughdavenport_> #info dobedobedoh to create a bug for modifying release & test running scripts to look for tests in the right places. DONE
07:37:48 <maharabot> Launchpad bug 897593 in mahara "Update .gitattributes to excluded tests from built releases" [Low,In progress] - Assigned to Andrew Nicols (dobedobedoh)
07:38:00 <dobedobedoh> Apologies - please move on!
07:38:14 <fmarier> the action item from last week (creating the bug) is done :)
07:38:19 <dobedobedoh> yup
07:38:21 <fmarier> s/week/month/
07:38:28 <hughdavenport_> right, will go on then
07:38:31 <hughdavenport_> #info dobedobedoh to look at the possibility of adding a hook to the Makefile to reject patches with security bugs in the subject
07:38:33 <fmarier> there's even a patch on gerrit
07:39:09 <dobedobedoh> I've had a look at the Launchpad API and can convert a bug# into a security/not-security
07:39:12 <dobedobedoh> and public/not-public
07:39:17 <dobedobedoh> so that's a start
07:39:32 <hughdavenport_> so it is possible?
07:39:36 <hughdavenport_> that is good news
07:39:39 <dobedobedoh> It is :)
07:39:49 <hughdavenport_> nice, any more to talk on this one?
07:40:09 <dobedobedoh> I haven't got it to play with anything yet - I was thinking it would probably be best on the gerrit server rather than the Makefile
07:40:16 <dobedobedoh> but that's all from me
07:40:22 <hughdavenport_> #info dajan to report on tagging existing 1.5 features on the tracker: newfeature tag
07:40:26 <hughdavenport_> next up
07:40:32 <fmarier> dobedobedoh yeah maybe as a server side hook
07:40:46 <elky> it would definitely be better on the server
07:40:58 <dobedobedoh> agreed
07:41:27 <hughdavenport_> just realized dajan isn't here
07:41:39 <elky> especially since LP is hosted in the wrong hemisphere and can take a while to respond here
07:41:41 <fmarier> well he's done it, so it's awesome
07:41:46 <hughdavenport_> does anyone know about that? fmarier you were tagging things
07:41:54 <hughdavenport_> right
07:41:57 <fmarier> #link https://bugs.launchpad.net/mahara/+bugs?field.tag=newfeature
07:41:59 <hughdavenport_> #topic Update on the user manual (Kristina)#link http://manual.mahara.org/
07:42:05 <fmarier> that's the list of the ones he's tagged
07:42:08 <hughdavenport_> #topic Update on the user manual (Kristina)
07:42:13 <hughdavenport_> #link http://manual.mahara.org/
07:42:14 <anitsirk> As you may have seen in the newsletter from Dec. 1, 2011, we started publishing the new user documentation at http://manual.mahara.org It is been written in reStructuredText and turned into fancy looking HTML (and PDF and ePub) with the help of Sphinx.
07:42:14 <anitsirk> #link http://mahara.org/view/artefact.php?artefact=176713&view=36871 - Mahara Newsletter, December 2011
07:42:14 <anitsirk> I had abandonded the Asciidoc files and converted them into rST syntax when fmarier showed suggested rST and Sphinx to me. We can also publish directly from git to ReadtheDocs and it should be possible to use Launchpad translations for translating it. :-) That is still under investigation.
07:42:15 <anitsirk> The PDF output on RTD isn't updated, but we'll deal with that later. I want to get at least the content as complete as possible. ;-)
07:42:15 <anitsirk> #info the mahara 1.4 user manual is written in reStructuredText, converted into HTML / PDF with Sphinx and published from git to ReadtheDocs at updated at http://manual.mahara.org
07:42:15 <anitsirk> #link http://docutils.sourceforge.net/rst.html - reStructuredText: Markup syntax and parser system
07:42:15 <anitsirk> #link http://sphinx.pocoo.org - Sphinx: software to convert rST files into proper documentations
07:42:16 <anitsirk> #link http://readthedocs.org - Read the Docs: community place to search for and search manuals
07:42:16 <anitsirk> #link http://readthedocs
07:42:16 <anitsirk> When I'm done with the 1.4 manual, it will be updating it to cover the already existing 71 1.5 features. ;-)
07:42:17 <anitsirk> That's about covers it what I wanted to say about the manual.
07:42:36 <anitsirk> ..
07:42:37 <elky> excellent work on that, anitsirk :)
07:42:50 <anitsirk> thanks, elky. fmarier helped heaps
07:43:13 <anzeljg> a question: is it possible that you create the screnshots also in other languages or are the translators responsible for that?
07:43:19 <fmarier> manual is in git, people can email patches to anitsirk :)
07:43:34 <fmarier> anzeljg: we haven't looked at translations yet, but it's definitely on our list
07:43:49 <anitsirk> anzeljg: we haven't looked into that yet and thus don't know how to deal with screenshots
07:44:04 <anzeljg> ok
07:44:11 <anzeljg> it would be great though
07:44:20 <anitsirk> but i have a document for documentation writes which states that screenshots shouldn't contain any text cause that would make translations more difficult in launchpad i think
07:44:35 <anitsirk> *writers
07:45:07 <elky> if we were to have someone make wireframe svgs, it'd be more translatable, but it would also be confusing for users I think
07:45:24 <anzeljg> yeah, but when you create screenshots of Mahara, used in manual, they certainly contain language strings... right?
07:45:26 <fmarier> elky: then it wouldn't look like a mahara screenshot though
07:45:35 <anitsirk> i have some svg files that are also in git to adapt those more easily but i'm not converting screenshtos into wireframes
07:45:38 <anitsirk> ;-)
07:45:41 <elky> fmarier, yep, hence the second part of what i said
07:45:52 <fmarier> we'll get screenshots in the right language, we just don't know how yet because we haven't looked into it
07:45:58 <anitsirk> anzeljg: the strings that you see on the screen, yes, but not any step-by-step instructions
07:46:16 <anzeljg> i was reffering to the first one
07:46:16 <fmarier> there were a lot of other little details to take care of with the english manual :)
07:47:07 <anitsirk> fmarier is right. but i think it is a good solution and it's definitely easy to update the files already in english
07:47:17 <elky> i've heard rumors that google is researching how to translate images on the internets, but that will be quite a long way off i'd imagine
07:48:25 * dobedobedoh wonders whether Selenium could be of any use...
07:48:33 <anitsirk> i don't trust google traslations too much. they mucked up heinz' newsletter contribution so much that i couldn't link to it...
07:48:57 <anitsirk> we haven't looked at any integration with automated tests yet as it's been only me updating the manual
07:49:25 <anitsirk> sphinx has a cool summary when it makes the build files and the biggest oopses are returned
07:49:33 <fmarier> dobedobedoh: you mean for taking screenshots automatically?
07:49:44 <dobedobedoh> fmarier: Yup
07:49:51 <fmarier> that's interesting :)
07:49:54 <elky> anitsirk, their translations are essentially self-poisoned and they're pulling out of it. however the rumor is that they're looking at the engine to do the translation, something like hand it an image and an xml file and voila
07:50:22 <elky> dobedobedoh, that is a great idea.
07:50:29 <anitsirk> mhh. let's wait and see, elky.
07:50:41 <elky> anitsirk, yeah, i wouldn't go holding my breath for it ;)
07:50:53 <dobedobedoh> heh yeah - I can't see that being easy
07:50:56 <anitsirk> mhh. that may not quite work as i do put extra numbers in their to identify the steps
07:51:21 <elky> anitsirk, you could provide that as an overlay and something like GD could combine the two layers
07:51:49 * anitsirk wonders if that works in sphinx as everything is compiled in there
07:52:07 <elky> we work with our upstreams ;)
07:52:23 <fmarier> i think we're talking about a problem we don't have :) we have translators that diligently take their own screenshots :)
07:52:39 <elky> yep, this is a geek tangent for sure :)
07:52:56 <fmarier> yeah and an interesting one I must say
07:53:08 <elky> but lets do move on
07:53:19 <hughdavenport_> so moving on?
07:53:40 <hughdavenport_> #topic Changes to the backend password hash function (hugh)
07:53:41 <hughdavenport_> #link https://wiki.mahara.org/index.php/Developer_Area/Specifications_in_Development/Improve_Password_Storage
07:54:00 <hughdavenport_> Righto, so I have pushed a few changes to gerrit changing the way we store the hashed passwords
07:54:11 <hughdavenport_> not sure of the numbers atm, as not at my usual computer
07:54:25 <hughdavenport_> but basically have changed it from a sha1 hash to bcrypt
07:54:36 <hughdavenport_> and have added a passwordsaltmain like thing (from moodle)
07:54:47 <hughdavenport_> so if the db is stolen, you can't brute the passwords
07:54:47 <fmarier> there's lots of good stuff in that spec, well worth reading
07:54:50 <hughdavenport_> ..
07:55:21 <elky> that sounds like a nice insurance policy
07:55:32 <elky> (the last bit)
07:55:38 <hughdavenport_> oh and also, we have a inbetween hash, sha512 for bulk actions, and the upgrade path
07:55:38 <dobedobedoh> Looks good to me
07:55:53 <anzeljg> nice
07:55:57 <hughdavenport_> anything more ppl want from that?
07:56:00 <fmarier> yeah you need to steal a dump of the db and a copy of config.php
07:56:17 <hughdavenport_> take a look on gerrit to see what is what, hashtag is passwords iirc
07:56:18 <fmarier> then spend a bit of time generating bcrypt hashes for common passwords
07:56:47 <elky> with those lists i tacked onto that bug?
07:56:58 <fmarier> #link https://reviews.mahara.org/#q,status:open+project:mahara+branch:master+topic:password-hash,n,z
07:57:28 <hughdavenport_> thanks fmarier
07:58:27 <hughdavenport_> #link https://bugs.launchpad.net/mahara/+bug/843568
07:58:29 <maharabot> Launchpad bug 843568 in mahara "Stored passwords with a stronger hash algorithm" [Medium,In progress] - Assigned to Hugh Davenport (hugh-catalyst)
07:58:50 <hughdavenport_> elky what lists you tacked on to the bug?
07:59:18 <elky> i forget the bug number, not that bug number
07:59:29 <fmarier> #link https://bugs.launchpad.net/mahara/+bug/844457
07:59:31 <maharabot> Launchpad bug 844457 in mahara "suckypasswords check is very limited, could be expanded" [Wishlist,Triaged]
07:59:33 <fmarier> ^^^ that one
07:59:43 <elky> yeah, thanks fmarier, was still looking
07:59:45 <hughdavenport_> ah right
07:59:52 * hughdavenport_ is less confused
08:00:00 <dobedobedoh> heh
08:00:24 <hughdavenport_> right, anything more?
08:01:19 <hughdavenport_> #topic Large bug triage and 1.5.0 (Francois)
08:01:20 <hughdavenport_> #link https://launchpad.net/mahara/+milestone/1.5.0
08:01:26 <hughdavenport_> fmarier take the stage
08:01:33 <fmarier> so you may have seen a large number of bug updates :)
08:01:51 <elky> the email aftermath anyway
08:01:53 <fmarier> i've gone through all of the outstanding bugs on the tracker
08:01:54 <hughdavenport_> yes :P
08:01:55 * anitsirk hasn't yet waded through the awesomeness yet :-(
08:02:23 <elky> by outstanding you mean not-milestoned?
08:02:29 <fmarier> have normalized the tags, found duplicates and adjusted the prioties / milestones of a few
08:02:38 <fmarier> elky: no, all the ones that are not "fix released"
08:02:49 <elky> ah
08:02:49 <dobedobedoh> long job
08:02:59 <hughdavenport_> yeh good job
08:03:29 <elky> how's the RSI?
08:03:38 <fmarier> the reason why i wanted to bring this up is that i have pre-triaged the list for the 1.5 milestone
08:03:39 <anitsirk> what's RSI?
08:03:47 <elky> repetitive strain injury
08:03:47 <fmarier> anitsirk: repetitive stress injury
08:03:49 <dobedobedoh> Repetitive Strain Injury
08:03:59 <anitsirk> thanks
08:04:26 <fmarier> so if you think there's something missing on 1.5 and that you can do in the next month or two, then please feel free to add it there
08:05:03 <elky> fmarier, i'm noticing we now have several android related issues. perhaps we need to find some way we can test those better
08:05:09 <fmarier> also, if you have something allocated to you, please consider whether or not you will have time to do it in the next month and if not, consider unassigning yourself from it
08:05:55 <anitsirk> "next" month" is probably a bit short or would need to be a bit longer with the holidays and all
08:06:09 <fmarier> i think that many people wanted to have an early freeze date next year, so it would be good to bring that list of bugs down
08:06:09 <elky> "221 Fix Committed"
08:06:12 <elky> niiiice
08:06:47 <hughdavenport_> anything else with this topic?
08:06:53 <anitsirk> #info fmarier pre-triaged the list of bugs for the 1.5 milestone
08:06:55 <fmarier> but basically, have a look, make sure none of your pet bugs are missing
08:07:42 <dobedobedoh> It'd be good if we had a list of tags that we actively use
08:07:46 <anitsirk> #info if you are allocated to a bug but cannot get to it within the next month, please unassign yourself.
08:07:49 <dobedobedoh> So that we don't undo all of your work
08:08:06 <anitsirk> #info if you think there's something missing on 1.5 and that you can do in the next month or two, then please feel free to add it there
08:08:14 <fmarier> dobedobedoh: there is on this page: https://bugs.launchpad.net/mahara
08:08:15 <elky> dobedobedoh, they seem to be more readable in the right-column of the main bugs area now
08:08:18 <hughdavenport_> there is an official tags list, ekly found it
08:08:27 <fmarier> click on "show more tags"
08:08:28 <hughdavenport_> good spelling of elky for me
08:08:36 <elky> hehe
08:08:46 <dobedobedoh> ah yes
08:08:50 * dobedobedoh was looking in the wrong place
08:08:58 <fmarier> also there is auto-completion of some of the most common tags
08:09:21 * dobedobedoh grrs at the 'Show less tags'
08:09:30 <fmarier> what i did while triaging them was to enter a tag, then save and click on it to see how many bugs showed up
08:09:35 <anitsirk> oh. the tag cloud was converted into a list
08:09:36 <dobedobedoh> There are tags missing from that list I know are used
08:09:44 <fmarier> it really helps finding duplicate bugs
08:10:18 <fmarier> dobedobedoh: yeah, i only made the most common ones "official" because otherwise the list was too long
08:10:34 <dobedobedoh> Ah okay
08:10:51 <fmarier> but if you think i've missed important ones, feel free to add them
08:11:08 <fmarier> https://bugs.launchpad.net/mahara/+manage-official-tags
08:11:25 <hughdavenport_> #link https://bugs.launchpad.net/mahara/+manage-official-tags
08:11:29 <fmarier> there are still a few that need to be cleaned up on there
08:13:05 <anitsirk> #info there are now official tags for mahara. only the most common ones are listed in the official tag list at https://bugs.launchpad.net/mahara
08:13:18 <fmarier> i guess that topic is done now
08:13:38 <hughdavenport_> #topic Next meeting and Chair
08:13:43 <hughdavenport_> I suggest Wednesday, 11 January 2012 at 07:30:00 UTC time
08:13:44 <hughdavenport_> #link http://www.timeanddate.com/worldclock/fixedtime.html?iso=20120111T0730
08:13:51 <hughdavenport_> thoughts?
08:13:51 <fmarier> hughdavenport_: how about late jan instead?
08:14:08 <fmarier> with christmans & all, there won't be too much to talk about i reckon
08:14:32 <hughdavenport_> or feb, so we don't have a meeting then another one two weeks after?
08:14:49 <hughdavenport_> bug +1's my last
08:14:52 <anzeljg> agree
08:14:56 <fmarier> hughdavenport_: well we can have one end of jan, then one end of feb
08:15:11 <dobedobedoh> End of Feb wfm
08:15:13 <dobedobedoh> and end of jan
08:15:15 <hughdavenport_> so we just go to end of month from now?
08:15:16 <anitsirk> +1 fmarier
08:15:22 <fmarier> but first weeek of feb works for me too
08:15:35 <anitsirk> hughdavenport: i think we've done that before
08:15:47 <fmarier> hughdavenport_: we're not all that regular, it's every 4-5 weeks roughly i think
08:16:13 <dobedobedoh> So 19:30 UTC on what day?
08:16:14 <fmarier> so how about tuesday 31 jan or feb 1 ?
08:16:27 <dobedobedoh> Feb 1st -1 for me ;)
08:16:45 <anitsirk> should work either date
08:16:50 <hughdavenport_> #link http://www.timeanddate.com/worldclock/fixedtime.html?iso=20110201T0730
08:17:16 <anitsirk> but we usually alternate the mornings and evenings
08:17:20 <hughdavenport_> ah yes
08:17:25 <anitsirk> it should be NZ morning
08:17:38 <hughdavenport_> #undo
08:17:38 <maharameet> Removing item from minutes: <MeetBot.items.Link object at 0x98fec2c>
08:17:41 <hughdavenport_> #link http://www.timeanddate.com/worldclock/fixedtime.html?iso=20110201T1930
08:17:50 <hughdavenport_> feb 1st 1930 UTC
08:17:58 <anitsirk> that way the americans may be able to join in. i'll let them know at the MUG meeting in january
08:17:58 <dobedobedoh> So it'll be 1st feb for you guys after all
08:18:05 <fmarier> dobedobedoh said he didn't want feb 1
08:18:07 <fmarier> #link http://www.timeanddate.com/worldclock/fixedtime.html?iso=20120131T1930
08:18:11 <fmarier> ^^^ 31 jan
08:18:15 <dobedobedoh> I can do 1st feb if it's not 1st feb here ;)
08:18:39 <fmarier> that last one is 31 jan UTC
08:18:46 <anitsirk> is it your birthday with all the smileys you include, dobedobedoh ?
08:19:00 <dobedobedoh> heh yup
08:19:06 <anitsirk> :-D
08:19:35 <fmarier> what more could you hope for on your birthday than to take part in a mahara dev meeting? ;-)
08:19:45 <dobedobedoh> :-p
08:19:49 <anzeljg> :-D
08:19:53 <anitsirk> +1 for that meeting date and time
08:19:56 <dobedobedoh> +1
08:19:58 <anzeljg> +1
08:20:16 <hughdavenport_> +2
08:20:33 <richardm> +1
08:20:35 <fmarier> hughdavenport_: is that a veto? ;)
08:20:39 <fmarier> +1
08:20:44 <anitsirk> nah. that's just bugg as well
08:20:48 <hughdavenport_> fmarier i have bugg
08:20:51 <elky> +1
08:20:59 <fmarier> ah right, i forgot about your silent side-kick
08:21:05 <hughdavenport_> yup
08:21:08 <hughdavenport_> he is talking
08:21:20 <hughdavenport_> we are enjoying some refreshments :D
08:21:20 <elky> and we don't hear it, so he's silent :P
08:21:32 <elky> timesheetable pub visit?
08:21:41 <hughdavenport_> elky do you hear anything on this?
08:21:42 <fmarier> hughdavenport_: training for the zoodoo i take it?
08:21:45 <hughdavenport_> and yup :D
08:21:49 <hughdavenport_> *2
08:21:52 <hughdavenport_> *4 i should say
08:22:06 <hughdavenport_> right anyway i think we are agreed
08:22:11 <elky> yup
08:22:17 <hughdavenport_> chair?
08:22:18 <fmarier> dobedobedoh: by the time we have the next meeting, dan_p will be working for Martin right?
08:22:24 <dobedobedoh> I think so yes
08:22:30 <dobedobedoh> But he'll be in the UK still
08:22:51 <hughdavenport_> bugg wants to chair sometime, but probably nz evening
08:22:57 <anitsirk> hehe
08:23:22 <anitsirk> i can chair. 8:30 is late ;-)
08:23:37 <dobedobedoh> fine by me :)
08:23:41 <dobedobedoh> 07:30 was early for me
08:23:45 <anzeljg> +1
08:24:22 <hughdavenport_> we agreed, 31 Jan 1930 UTC anitsirk chairing?
08:24:27 <anzeljg> +1
08:24:38 <elky> wfm
08:24:42 <fmarier> i think so
08:24:50 <hughdavenport_> #info Next meeting 31 Jan 1930 UTC anitsirk chairing
08:24:51 <fmarier> #link http://www.timeanddate.com/worldclock/fixedtime.html?iso=20120131T1930
08:25:02 <hughdavenport_> #topic Any other business?
08:25:09 <anzeljg> I've got two...
08:25:11 <anitsirk> got one
08:25:38 <hughdavenport_> anzeljg: you go first
08:25:51 <anzeljg> First: Just as a reminder - what is the status of PluralForms?
08:26:19 <anzeljg> Second: I'm working on integrating GoogleWebFonts, this part is almost complete.
08:26:34 <hughdavenport_> ooo, exciting
08:26:35 <fmarier> i personally lost track of where we got with PluralForms
08:26:44 <richardm> anzeljg: wrt plural forms, i was waiting for you to approve my patch!
08:26:58 <richardm> somewhere on github i think
08:26:59 <anzeljg> And just started working on LivePreview when user designs a custom Skin...
08:27:12 <anzeljg> Please have a look at:
08:27:21 <anzeljg> #link http://screencast.com/t/cWrdGaNz4mU
08:27:30 <anzeljg> Feedback welcome...
08:27:53 <hughdavenport_> right, that all from you anzeljg ?
08:27:59 <anzeljg> richardm: I think that was OK. Will have another look. Can you please send me the link to the patch again...
08:28:00 <anitsirk> #info anzeljg started working on LivePreview when user designs a custom Skin http://screencast.com/t/cWrdGaNz4mU and welcomes feedback
08:28:20 <anitsirk> #info anzeljg almost finished integrating GoogleWebFonts
08:28:52 <anitsirk> my one thing: i wish you all happy holidays and a good start into 2012 :-) I know it's still a few weeks away, but anyways
08:28:55 <hughdavenport_> anitsirk: you next
08:29:06 <anitsirk> done
08:29:07 <hughdavenport_> oh, thanks heaps!
08:29:08 <elky> aww
08:29:09 <hughdavenport_> you too
08:29:26 <hughdavenport_> any final final business?
08:29:42 <anitsirk> nope
08:29:51 <hughdavenport_> #endmeeting