20:07:20 <rkabalin_> #startmeeting
20:07:20 <maharameet> Meeting started Wed Nov 21 20:07:20 2012 UTC.  The chair is rkabalin_. Information about MeetBot at http://wiki.debian.org/MeetBot.
20:07:20 <maharameet> Useful Commands: #action #agreed #help #info #idea #link #topic.
20:07:29 <rkabalin_> Name yourself please
20:07:30 <sonn> Hi
20:07:40 <anitsirk> anitsirk: Kristina Hoeppner at Catalyst IT, Wellington, NZ
20:07:57 <rkabalin_> #info rkabalin_ Ruslan Kabalin, Lancaster University, UK
20:08:04 <sonn> #info sonn: Son Nguyen, at Catalyst IT, Wellington, NZ
20:08:16 <anitsirk> #info: anitsirk: Kristina Hoeppner at Catalyst IT, Wellington, NZ
20:08:27 <anitsirk> long time no meeting and forgot the command ;-)
20:08:33 <rkabalin_> ))
20:08:33 <sonn> :)
20:08:41 <sonn> elky is comming
20:08:42 <anitsirk> elky just came in.
20:09:05 <rkabalin_> Welcome to the 22th Mahara Dev Meeting
20:09:30 * elky is melissa draper from catalyst it
20:09:36 <elky> #info is melissa draper from catalyst it
20:09:41 <elky> and still waking up :D
20:09:57 <anitsirk> i guess that's all for now.
20:10:07 <rkabalin_> ok, I think that is everyone so far
20:10:13 <rkabalin_> #topic Items from last meetings
20:10:24 <rkabalin_> I can't see any action items
20:10:54 <anitsirk> it was a short meeting as well.
20:11:01 <rkabalin_> yeah
20:11:09 <rkabalin_> #topic New tag in Launchpad
20:11:21 <anitsirk> #info we have a new tag called "nominatedfeature" in launchpad.
20:11:29 <anitsirk> https://bugs.launchpad.net/mahara/+bugs?field.tag=nominatedfeature
20:12:02 <anitsirk> #info: we'll be using it for all new features from now on to indicate what might go into a new release
20:12:25 <anitsirk> #info it is to help the release manager to see early what might be considered for release and what to keep an eye on.
20:12:47 <anitsirk> it can also be used after a release is released because we can search for milestones etc.
20:13:06 <anitsirk> the old "newfeature1.6" etc. tag was only applied to features that were merged into master but not for pending features.
20:13:12 <anitsirk> we try to remedy that.
20:13:13 <anitsirk> ..
20:13:48 <rkabalin_> good idea
20:14:08 <rkabalin_> yeah, that will be helpful
20:14:10 <elky> Yeah, otherwise everything's a wishlist, and there's lots of them
20:15:09 <rkabalin_> ok, next topic then?
20:15:10 <anitsirk> we'll also try to work more with blueprints esp. for larger features that span several bugs.
20:15:22 <anitsirk> a good example is https://blueprints.launchpad.net/mahara/+spec/no-javascript
20:15:22 <rkabalin_> blueprints?
20:15:26 <anitsirk> https://blueprints.launchpad.net/mahara/+spec/no-javascript
20:15:32 <rkabalin_> ah, those are in launchpad
20:15:50 <anitsirk> #idea we'll also try to work more with blueprints esp. for larger features that span several bugs. e.g. https://blueprints.launchpad.net/mahara/+spec/no-javascript
20:15:53 <elky> yep, we moved the specs in develoment page there a while back
20:16:00 * rkabalin_ did not know we started using it
20:16:19 <anitsirk> the wiki pages still exist and have more elaborate information, but in launchpad we can link bugs more easily
20:16:32 <elky> I thought we mentioned it in the meeting, sorry
20:17:00 <rkabalin_> #info Mahara blueprints: https://blueprints.launchpad.net/mahara
20:17:22 <rkabalin_> I might have missed it somehow
20:17:46 <rkabalin_> that is good
20:18:14 <elky> we might have forgot to mention it too. i'd seen them used a bit within the ubuntu dev cycle, but i don't think anyone else had hence they'd not been adopted.
20:19:20 <anitsirk> i think that's all for this topic, rkabalin_
20:19:36 <rkabalin_> anyone has something to add on this topic?
20:19:54 <anitsirk> nope
20:20:01 <rkabalin_> #topic Next meeting and Chair
20:20:05 <elky> nope
20:20:36 <rkabalin_> anyone willing to chair next meeting?
20:20:41 <anitsirk> i can
20:20:47 <rkabalin_> thanks
20:21:06 <anitsirk> i think a january date would be better with christmas etc. coming up
20:21:15 <rkabalin_> yes
20:21:32 <anitsirk> what did you have in mind, rkabalin_ ?
20:21:34 <rkabalin_> please suggest the most convenient date for you
20:22:11 <anitsirk> jan 9 or 16 look good
20:22:37 <rkabalin_> the meeting will be at 8PM in NZ next time
20:22:43 <sonn> I vote for Jan 16th
20:22:47 <rkabalin_> both dates are fine with me
20:23:01 <rkabalin_> 16th slightly better
20:23:15 <anitsirk> rkabalin_: is that ok with you seeing that it is 7 a.m. uk time?
20:23:27 <anitsirk> i'd be happy to make it 9 pm NZ
20:23:43 <rkabalin_> anitsirk yeah
20:23:49 <anitsirk> ;-)
20:23:57 <elky> yeah to which?
20:24:42 <sonn> 9PM is fine for me
20:24:50 <rkabalin_> 7am is ok
20:25:09 <rkabalin_> 8am is better ;)
20:25:11 <anitsirk> sonn and elky: which one is better for you two?
20:25:19 <anitsirk> ah then we go with 8 am rkabalin_
20:25:33 <anitsirk> because nobody here want's to get up at 7 either when it's our turn ;-)
20:25:51 <elky> maybe if it's 8am there'll be mor than just rkabalin
20:26:01 <rkabalin_> Is it 7.25 now?
20:26:08 <anitsirk> so we would have http://www.timeanddate.com/worldclock/fixedtime.html?msg=23rd+Mahara+Developer+Meeting&iso=20130109T08 - please check if it's correct.
20:26:14 <elky> no, daylight savings makes it 9:25
20:26:17 <anitsirk> rkabalin_: no. it's 9:26 a.m.
20:26:28 <rkabalin_> I see
20:26:51 <elky> because uk and nz daylight savings changes go in opposite directions
20:27:38 <anitsirk> #info next developer meeting will be held on 16 January 2013 at 8:00 UTC: http://www.timeanddate.com/worldclock/fixedtime.html?msg=23rd+Mahara+Developer+Meeting&iso=20130116T08
20:27:45 <anitsirk> earlyier link was incorrect.
20:27:56 <rkabalin_> thanks anitsirk
20:28:09 <anitsirk> #info chair for the next dev meeting will be anitsirk
20:28:28 <rkabalin_> #topic Any other business
20:28:33 <elky> I have one
20:28:34 <anitsirk> i have a quick one
20:28:42 <rkabalin_> ok, go on
20:29:10 <anitsirk> #info the next mahara newsletter will be published at the beginning of January 2013. If you want to have a story published, please submit it to newsletter@mahara.org
20:29:27 <anitsirk> that's all
20:30:17 <anitsirk> #info as for deadline for newsletter submissions: i guess before Christmas would make most sense. ;-)
20:31:08 <elky> that really all now? :P
20:31:23 <anitsirk> yes
20:31:45 <elky> aquaman/hugh and I have been discussing the past week about the minaccept script, and we're going to make some changes that will enforce docblocks for new classes and methods and one other thing that i can't remember now. Unless someone is allergic to that idea with good reasoning.
20:31:56 <elky> We'll make a post on the dev forum with the details
20:32:03 <elky> oh, yes, i remember the other now
20:32:12 <elky> trying to enforce {} around table names
20:32:32 <anitsirk> elky: please put an #idea in front of your idea so it shows up in the meeting minutes
20:32:40 <elky> #idea aquaman/hugh and I have been discussing the past week about the minaccept script, and we're going to make some changes that will enforce docblocks for new classes and methods and one other thing that i can't remember now. Unless someone is allergic to that idea with good reasoning.
20:32:49 <elky> #idea the second is trying to enforce {} around table names
20:33:06 <elky> #info we'll make apost on the dev forum
20:33:13 <sonn> in SQL queries?
20:33:16 <rkabalin_> not everyone using it?
20:33:21 <rkabalin_> {} I mean
20:33:38 <elky> rkabalin, indeed, gregor had to fix a place it had been taken off recently
20:33:40 <elky> sonn, yes
20:34:06 <elky> rkabalin, the query had been rewritten and the {} were missed
20:34:12 <rkabalin_> I see
20:34:34 <elky> and it caused prefixed tables to not work as expected
20:35:22 <rkabalin_> that is a very serious norms violation, we need to check in in minaccept definitely
20:35:31 <elky> yeah
20:35:43 <elky> it somehow got through review
20:36:20 <elky> anyway, the docblock idea is likely to be the more annoying of the two changes
20:36:44 <elky> so we wanted to put it past the meeting attendees. though since that's just you from the UK then we'll put it on the forum too
20:37:15 <rkabalin_> thanks
20:37:56 <rkabalin_> anything else we need to discuss?
20:38:35 <rkabalin_> what was that aquaman item, does anyone know?
20:39:00 <elky> We'll get him to follow up on the forum
20:39:05 <anitsirk> rkabalin_: it's something aquaman should talk about. i have an inkling, but am not sure.
20:39:36 <rkabalin_> ok, thanks everyone for coming then
20:39:46 <elky> he had the same thing on the last agenda, it could be relic, but i know there's something he wants to mention
20:39:47 <anitsirk> thanks for chairing this meeting, rkabalin_
20:39:59 <rkabalin_> #endmeeting