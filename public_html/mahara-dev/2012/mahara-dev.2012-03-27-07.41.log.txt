07:41:27 <anitsirk> #startmeeting
07:41:27 <maharameet> Meeting started Tue Mar 27 07:41:27 2012 UTC.  The chair is anitsirk. Information about MeetBot at http://wiki.debian.org/MeetBot.
07:41:27 <maharameet> Useful Commands: #action #agreed #help #info #idea #link #topic.
07:41:29 <dobedobedoh> Seems that one of the irc nodes was down and supybot isn't very good at DNS roundrobin failover
07:41:35 <anitsirk> Welcome to the 15th Mahara Developer Meeting. If you could please start a new line with #info and state your name and location please.
07:41:49 <anitsirk> #info anitsirk = Kristina Hoeppner, Catalyst IT, Wellington, NZ
07:41:51 <anzeljg> #info anzeljg is Gregor Anzelj, translator and developer, Ljubljana, Slovenia
07:41:52 <dobedobedoh> #info Andrew Nicols - Lancaster University
07:41:58 <fmarier> #info fmarier is Francois Marier
07:42:04 <richardm> #info richardm is Richard Mansfield, Catalyst IT, Wellington, New Zealand
07:42:09 <rkabalin_> #info Ruslan Kabalin - Lancaster University, UK
07:42:24 <elky> #info elky is melissa draper, Catalyst IT
07:42:44 <anitsirk> #topic Items from last meeting
07:43:08 <anitsirk> #info all action items were related to checking bugs before freezing mahara 1.5. thus nothing to report.
07:43:20 <anitsirk> #topic Triggers in Mahara (Richard)
07:43:24 <anitsirk> over to you, richardm
07:43:28 <fmarier> we have an RC, yay!
07:43:43 <fmarier> round of applause for elky
07:43:51 <anitsirk> oh. sorry. pop. that was the champagne cork :-)
07:43:59 <rkabalin_> Well done!
07:43:59 <elky> yay me!
07:44:03 <richardm> yay!
07:44:06 <dobedobedoh> Ace :) Well done all
07:44:07 <fmarier> anitsirk: drinking in the office? ;-)
07:44:13 <anitsirk> nope. i'm at home
07:44:20 <dobedobedoh> Don't drink and root ;)
07:44:38 <anitsirk> :-)
07:45:02 <richardm> So with the triggers, I just wanted to mention that we have a function to create db triggers, should anyone need them
07:45:18 <richardm> Thanks to rkabalin_'s review :)
07:45:22 <anitsirk> #info mahara has a function to create db triggers if needed
07:46:02 <fmarier> richardm: what sort of things do you expect triggers will be useful for?
07:46:12 <richardm> I guess I don't need to say much more, if you feel like you need to use them, read the function in lib/dml.php
07:46:22 <richardm> but for our case
07:47:01 <richardm> after a ton of toing & froing we decided to cache counts of unread notifications in the user table
07:47:22 <richardm> (because reading the notification_internal_activity was generating tons of slow queries)
07:48:19 <richardm> so i guess it's useful when a db field's value is completely determined by some other thing in the db
07:49:00 <richardm> i'm not aware of anything else we should use them for yet, but people should know they're there
07:49:01 <rkabalin_> How do we upgrade if trigger needs to be changed
07:49:03 <rkabalin_> ?
07:49:03 <richardm> ..
07:49:11 <richardm> drop & recreate
07:49:38 <rkabalin_> OK, that is how I expected
07:50:09 <rkabalin_> ..
07:50:15 <anitsirk> #info If a trigger needs to be changed, you drop and recreate
07:50:15 <richardm> so there's a db_drop_trigger function which is not used anywhere yet
07:50:25 <richardm> but that's what you need to use
07:50:45 <richardm> ..
07:50:48 <anitsirk> #info the db_drop_trigger function would need to be used for that
07:51:13 <richardm> that's all from me, anitsirk
07:51:30 <anitsirk> any other questions or comments for this topic before we move on?
07:51:55 <anitsirk> fmarier: you are up next.
07:51:56 <anitsirk> #topic Upstream status of Dwoo (Francois)
07:51:57 <anitsirk> #info https://twitter.com/#!/seldaek/status/167546484865241088 triggered the topic
07:51:57 <rkabalin_> nothing from me
07:51:58 <anitsirk> #link https://twitter.com/#!/seldaek/status/167546484865241088
07:52:29 <fmarier> indeed, Mjollnir` told us that she was looking for a new maintainer for the dwoo debian package and pointed to that tweet
07:53:13 <fmarier> so i guess the question is: if dwoo is not going to be maintained much in the future, should we keep using it or should we look at switching to something else?
07:53:28 * fmarier doesn't know much about other php templating systems
07:53:41 <anitsirk> #info Mjollnir` was looking for a new maintainer for the dwoo debian package
07:53:46 <rkabalin_> We could maintain it as a part of Mahara
07:54:07 <fmarier> yeah, that's an option. we did that with pieforms
07:54:08 <rkabalin_> (not as standalone thing)
07:54:27 <anitsirk> #help: if dwoo is not going to be maintained much in the future, should we keep using it or should we look at switching to something else?
07:54:32 <richardm> we could switch to the new one that seldaek is working on if/when we need to
07:54:39 <dobedobedoh> We've already changed the templating engine underneath people once, and it can be quite expensive for our users to rewrite their customisations :\
07:54:39 <richardm> i forget what it's called
07:54:51 <fmarier> richardm: ah, that's why he's dropping dwoo?
07:55:08 <richardm> yeah he has a blog post on it somewhere
07:55:16 <fmarier> dobedobedoh: except that last time we didn't really have to change much when we switched away from smarty
07:55:21 <dobedobedoh> true
07:55:47 <fmarier> are they any big templating systems for php or is it just homebrewed things from various developers?
07:56:04 <anitsirk> #idea we could switch but that could be expensive to rewrite customizations
07:56:48 <elky> fmarier, not that i can think of off the top of my head
07:57:00 <anitsirk> #help are there any big templating systems for php or is it just homebrewed things from various developers?
07:57:20 <rkabalin_> I think we should keep Dwoo, as it entirely meets our requirements at the moment and maintain it as a part of Mahara. If someone wants to use it separately, that person could easily do that
07:57:36 <fmarier> ok, well, i don't have much more to say on this topic. we don't have an immediate action to take on this, but i just wanted to throw it out there
07:57:57 <anitsirk> #idea from rkabalin_ : I think we should keep Dwoo, as it entirely meets our requirements at the moment and maintain it as a part of Mahara.
07:58:24 <anitsirk> do we have to action something right away or would something have to go on to the more long term todo list?
07:58:50 <fmarier> i think rkabalin_'s suggestion of doing nothing until we have a good reason to switch is fine
07:59:00 <elky> review the situation at the beginning of each cycle, probably
07:59:10 <anzeljg> I think I found it: http://symfony.com/
07:59:12 <rkabalin_> fmarier: yeap, that what I meant
07:59:28 <fmarier> review the situation if we learn of a better templating system and bring it up at a meeting
07:59:39 <fmarier> symfony is a whole framework i think
07:59:41 <richardm> ah yeah symfony, that's the one
07:59:47 <rkabalin_> sumfony is MVC framework, not just a templating thingy
07:59:50 <elky> that's more than a templating engine by the looks
07:59:55 <dobedobedoh> symfony is a whole framework
07:59:58 <fmarier> yeah, that would be a massive rewrite of mahara :)
08:00:01 <anitsirk> do you agree on what rkabalin_ suggested and fmarier summarized?
08:00:03 <dobedobedoh> (It's quite nice to use)
08:00:04 <elky> yeahno
08:00:13 <rkabalin_> anitsirk: yea, I agree
08:00:22 <Mjollnir`> here
08:00:30 <Mjollnir`> it's twig that he switched to
08:00:53 <Mjollnir`> it's included in symfony but it's also a standalone templating system
08:00:54 <dobedobedoh> Mjollnir`: any idea what the reason for the switch is?
08:01:21 <Mjollnir`> i guess he got involved in symfony for other reasons anyway
08:01:27 <anzeljg> #link http://twig.sensiolabs.org/
08:01:27 <fmarier> #link http://twig.sensiolabs.org/
08:01:32 <fmarier> that looks nice
08:01:38 <Mjollnir`> fwiw, i really like twig
08:01:45 <fmarier> i'll create a wishlist bug for it
08:02:03 <dobedobedoh> Looks like it wouldn't be to much of a switch, and hopefully it would be maintained if it's being used with symfony
08:02:20 <Mjollnir`> but moving from smarty to dwoo we didn't change any templates really. otoh we didn't really take advantage of using dwoo over smarty for that reason ...
08:02:45 <fmarier> yeah the only advantage we got out of using dwoo was the auto-escaping
08:02:57 <Mjollnir`> yeah.
08:03:10 <Mjollnir`> but jordi is not doing releases iirc, you'd have to track git
08:03:15 <anitsirk> #action fmarier to create a wishlist bug for twig as templating system
08:04:21 <Mjollnir`> lack of security support at some point in the future might be a reason to switch
08:04:33 <fmarier> #link https://bugs.launchpad.net/mahara/+bug/966001
08:04:35 <richardm> Mjollnir`: do you reckon jordi would be keen to do the work switch mahara to twig (like he did with dwoo)
08:04:35 <maharabug> Launchpad bug 966001 in mahara "Switch templating system from dwoo to twig" [Wishlist,Triaged]
08:04:53 <Mjollnir`> richardm: no
08:05:05 <Mjollnir`> richardm: i don't think jordi is directly involved in twig at all
08:05:13 <Mjollnir`> he's just not maintaining dwoo anymore because he's using symfony
08:06:18 <dobedobedoh> If we do a rewrite, we should stop calling the template engine $smarty
08:07:14 <anzeljg> Mybe we should call it $template (in cas we switch again in the future)?
08:07:35 <fmarier> or $notsmarty ;-)
08:07:47 <fmarier> anyways, that's all i had
08:07:48 <dobedobedoh> I'm thinking $templator or something akin
08:07:51 <Mjollnir`> twig syntax is based on whatever django uses for templates by the way
08:07:59 <fmarier> thank you very much Mjollnir` for jumping in
08:08:13 <rkabalin_> I suggest to keep it $smarty :)
08:08:16 * fmarier loves Django
08:08:24 <rkabalin_> For historical reasons
08:08:39 <elky> uh oh
08:08:42 <Mjollnir`> http://fabien.potencier.org/article/34/templating-engines-in-php
08:08:46 <rkabalin_> so, everyone will keep asking why it is called smarty :)
08:09:01 <anitsirk> sorry. back again
08:09:10 <fmarier> #link http://fabien.potencier.org/article/34/templating-engines-in-php
08:10:00 <Mjollnir`> fmarier: no worries, you highlight me, i appear :)
08:10:00 <anitsirk> shall we move on?
08:10:10 * Mjollnir` goes back to idling
08:10:15 <fmarier> sure
08:10:22 <anitsirk> Mjollnir`: thanks for stopping by
08:10:26 <elky> yup
08:10:29 <anzeljg> http://wiki.cmsmadesimple.org/index.php/FAQ2#Who.27s_that_Smarty_guy.3F
08:10:44 <anitsirk> #topic Should CIA commit go on #mahara-dev? (Francois)
08:10:44 <anitsirk> #info http://feeding.cloud.geek.nz/2012/03/watching-gerrit-merges-on-irc-using.html for background reading on the topic
08:10:45 <anitsirk> #link http://feeding.cloud.geek.nz/2012/03/watching-gerrit-merges-on-irc-using.html
08:10:58 <anitsirk> fmarier: you are up again :-)
08:11:12 <dobedobedoh> +1 for me on this
08:11:25 <fmarier> so that's a quick one: #mahara-commits currently has notices whenever something is merged from gerrit
08:11:35 <fmarier> should we move these notices to #mahara-dev ?
08:11:39 <anitsirk> this is really cool :-)
08:11:44 <fmarier> or do people prefer a separate channel for them ?
08:11:50 <fmarier> ..
08:12:01 <anitsirk> #info #mahara-commits currently has notices whenever something is merged from gerrit
08:12:02 <dobedobedoh> We used to have them on #mahara-dev and I think it helped to keep up with what was going on
08:12:12 <elky> i was thinking about putting maharabug in there to catch the bug numbers in the commit lines, this would be better i think
08:12:23 <fmarier> anybody object?
08:12:29 <richardm> not me
08:12:34 <rkabalin_> I agree
08:12:34 <elky> nope
08:12:58 <fmarier> ok, then I guess I get an action to move it to the other channel :)
08:13:07 <anitsirk> #agreed CIA commits currently on #mahara-cmmits move to #mahara-dev
08:13:22 <anitsirk> #action fmarier to move CIA commits from #mahara-commits to #mahara-dev
08:13:54 <anitsirk> then let's go to the next topic, fmarier and richardm
08:13:56 <anitsirk> #topic Update on how we give access to new translators (Francois & Richard)
08:14:24 <anitsirk> tip: if you put an #info at the beginning of your line, you won't have me repeating you ;-)
08:14:48 <anzeljg> FBI is missing...
08:14:54 <elky> heh
08:15:06 <fmarier> that was my action item :)
08:15:13 <anzeljg> i know!
08:15:18 <elky> mayeb i should rename maharabug, anzeljg?
08:15:29 <anzeljg> cool
08:15:43 <anitsirk> and what will 007 do?
08:15:44 <fmarier> then we'll have the whole DHS with us
08:16:56 <elky> so, back to the translations?
08:16:58 <anitsirk> fmarier and richardm: do you need a translator for the current topic? ;-)
08:17:08 <anzeljg> mogoče pa res..
08:17:51 <fmarier> Richard and I have been discussing what might be the best way to assign translators to languages for Mahara
08:18:15 <fmarier> with the goal of attracting new translators if possible
08:18:39 <fmarier> elky, dobedobedoh and I also discussed that a few days ago
08:19:01 <elky> that was a fun evening.
08:19:18 <fmarier> richardm can correct me if i'm wrong but the long term goal is to make the translation group either open or to assign it to the Launchpad Translators group
08:19:43 <richardm> fmarier: i thought you recommended Ubuntu Translators
08:20:10 <fmarier> #link https://launchpad.net/~launchpad-translators
08:20:21 <elky> richardm, launchpad-translators is even broader
08:20:27 <fmarier> richardm: no, this one is the one i was talking about
08:21:01 <richardm> right, i noticed that for some languages, the ubuntu team is a member of the launchpad team, but for others, that's not the case
08:21:51 <anitsirk> #idea make the translation group either open or assign it to the respective Launchpad Translators group
08:22:00 <fmarier> so ideally the owners of the mahara langpacks should be the teams in this list: https://translations.launchpad.net/+groups/launchpad-translators/
08:22:09 <fmarier> because they're the most active translation groups
08:22:31 <fmarier> and we should encourage our existing translators or prospective translators to join these groups
08:22:51 <anitsirk> #idea ideally the owners of the mahara langpacks should be the teams in this list: https://translations.launchpad.net/+groups/launchpad-translators/ because they are the most active translation groups
08:23:09 <fmarier> in some cases, the mahara translators prefer to have their own group. that's fine, but the default should be to use the existing launchpad groups i think
08:23:13 <anitsirk> fmarier: that was very easy and quick in the german group. approval came within a day
08:23:42 <fmarier> anzeljg: as a translator, what do you think of that?
08:23:49 <anitsirk> #info in some cases, the mahara translators prefer to have their own group.
08:23:59 <richardm> anitsirk: yes, but i don't think it's like that for other languages, the portuguese one had ~40 pending members, so obviously harder to join it
08:24:13 <anitsirk> lucky me :-)
08:24:50 <fmarier> richardm: did you have anything to add?
08:25:41 <rkabalin_> I think not every existing translatioer group will approve membership in Mahara langpacks
08:25:53 <elky> anzeljg, incase you missed it: <fmarier> anzeljg: as a translator, what do you think of that?
08:26:01 <fmarier> rkabalin_: what do you mean by that?
08:26:20 <richardm> nope, sounds good to me, i guess we just need to note that *everyone* has to agree to join the launchpad group before we could ever switch our translation group to Launchpad Translatorss
08:26:23 <fmarier> not every translation group will want to own the mahara translation?
08:26:39 <anzeljg> i don't have problems - i don't need to own my mahara translation group
08:26:39 <fmarier> richardm: indeed
08:26:52 <anzeljg> i would love to have somebody to translate it with me, but...
08:27:01 <rkabalin_> We want to make every existing lunchpad translators group a member of mahara translatoers, right?
08:27:14 <fmarier> anzeljg: you wouldn't mind joining the slovenian team on launchpad so that we could make it the owner of the mahara translation?
08:27:22 <anzeljg> no problem
08:27:55 <fmarier> what's the language code for slovenian again?
08:27:59 <anitsirk> #action anzeljg joins the slovenian team on launchpad so that we could make it the owner of the mahara translation
08:28:10 <anzeljg> but i would still be the Slovenian Mahara maintainer???
08:28:14 <anzeljg> the code is sl
08:28:21 <fmarier> #link https://launchpad.net/~lp-l10n-sl
08:28:28 <fmarier> we could use that team
08:28:31 <anzeljg> sl = Slovenina, si = Slovenia to be exact
08:28:45 <anzeljg> sl =Slovenian, sorry
08:28:55 <fmarier> and yes, you'd still be the slovenian mahara translator, but anybody in that team could help out if they wanted
08:29:13 <anzeljg> not translator, i meant maintainer
08:29:14 <anitsirk> so would it be anzeljg who approves any items under "needs review"?
08:29:28 <fmarier> of course, we'd wait until you were in that team before switching it over :)
08:29:28 <anzeljg> that's my question, thanks anitsirk
08:29:42 <rkabalin_> back to my point
08:29:45 <fmarier> anitsirk: i'm not sure how that works, i've never joined a team
08:29:49 <anitsirk> i haven't quite figured that one out yet who can do what
08:29:52 <fmarier> (a translation team)
08:30:37 <rkabalin_> using Slovenian group example, may launchpad Slovenian group reject the ownership of Mahara translators?
08:30:40 <anzeljg> I am waiting approval for "Slovenian Launchpad translation team"
08:30:50 <anitsirk> mhh. because now that heinz is not the translator for the german lang pack anymore :-( we had a great number of people interested in helping out :-) Yippie. however, with so many new people i fear that we might get a bunch of inconsistencies in that could confuse users if there is no person to keep an eye on the big picture
08:31:52 <anitsirk> #help who would approve lang strings marked for review in a translation if we assign the launchpad translators group as the owner of the mahara translation?
08:31:54 <fmarier> rkabalin_: it hasn't happened yet that a translation team asked to be removed
08:31:55 <richardm> if anzeljg were to join the lp-l10n-sl team, he would be able to approve the stuff that needs reviewing, but so would anyone else on the team
08:32:43 <anitsirk> #info anyone from a translation team can approve strings that need reviewing
08:32:56 <rkabalin_> you may add Russian one as well then, I am already a member of it, and remove Mahara Russian treanslators
08:33:14 <fmarier> if a translation maintainer notices that a translation team has lead to inconsistency and a lower quality of the translation, then we can always change it back to a Mahara-specific translation team
08:33:34 <anitsirk> #info if a translation maintainer notices that a translation team has lead to inconsistency and a lower quality of the translation, then we can always change it back to a Mahara-specific translation team
08:33:51 <anitsirk> fmarier: where do we see who the maintainer of a lang pack is? on the mahara translation page?
08:33:59 <anitsirk> … in launchpad?
08:34:16 <fmarier> https://translations.launchpad.net/+groups/mahara-translation-group
08:35:02 <fmarier> rkabalin_: this one? https://launchpad.net/~lp-l10n-ru
08:35:09 <anitsirk> if anzeljg joins the Slovenian translators, he wouldn't be mentioned anymore though.
08:35:41 <fmarier> no, we can still see which individuals have actually translated things
08:35:48 <anitsirk> oh. i hope we don't kick anzeljg out when we mention his name
08:36:28 <anzeljg> one question: what if I don't get approved, see: https://launchpad.net/~lp-l10n-sl/+members#active (application dates)?
08:36:31 <anitsirk> fmarier: but we would not see which maharian feels most responsible
08:37:28 <fmarier> https://translations.launchpad.net/mahara-lang/trunk/+pots/mahara/sl/+details
08:37:50 <richardm> anzeljg: we don't switch unless/until you're approved
08:37:53 <rkabalin_> fmarier: https://launchpad.net/~mahara-lang-ru < this one needs to be assigned instead of https://launchpad.net/~lp-l10n-ru
08:38:24 <anzeljg> ok
08:38:25 <rkabalin_> ah, sorry ignore me
08:38:41 <fmarier> rkabalin_: i just switched them
08:38:51 <fmarier> it was the mahara one, now it's the lp one
08:39:03 <elky> https://translations.launchpad.net/mahara-lang/trunk/+pots/mahara/sl/+filter?person=gregor-anzelj
08:39:14 <elky> shows how many strings in the pot
08:39:54 <anitsirk> #info the general launchpad russian translator team is now used for the russian mahara translation https://launchpad.net/~lp-l10n-ru
08:40:11 <anitsirk> fmarier: instead of making action items for you i wait until you are done and give it an info ;-)
08:40:19 <anzeljg> elky: cool, didn't know that it exists...
08:40:27 <fmarier> anitsirk: :)
08:40:32 <elky> anzeljg, :)
08:40:57 <elky> so we can tell the 5000 string translator from the 5 string translator :)
08:42:03 <dobedobedoh> sorry ;) We just don't call them Resumes!
08:42:29 <anitsirk> shall we agree that we use the general launchpad translators group for a lang pack unless there are reasons not to do so?
08:42:41 <elky> yup
08:42:44 <anzeljg> +1
08:42:54 <fmarier> anitsirk: groups (plural)
08:43:19 <anitsirk> fmarier: can we assign multiple groups to one translation?
08:43:21 <anzeljg> fmarier: don't even start ;)
08:43:22 <fmarier> not to be confused with Launchpad Translators which we need everybody to agree to
08:43:48 <fmarier> sorry, not groups, but rather, teams
08:44:09 <fmarier> translation group = list of assignments between language and team
08:44:26 <anitsirk> #rephrase: we we use the general launchpad translators teams for the mahara lang packs unless there are reasons not to do so
08:44:44 <fmarier> so we'll use the most active lp teams for each language unless there's a reason for using a different one for a given mahara translation
08:45:08 * fmarier is being pedantic
08:45:21 <anitsirk> fmarier: no much better and to the point
08:45:52 <anitsirk> #agreed we'll use the most active launchpad teams for each language unless there's a reason for using a different one for a given mahara translation
08:46:36 <anitsirk> and many thanks to all the translators who have so far shouldered all strings themselves (and many will probably continue to do so)
08:46:56 <fmarier> +1 to that
08:47:10 <fmarier> translators are awesome
08:47:34 <anitsirk> +1
08:47:39 <anzeljg> you guys are awesome too
08:47:44 <elky> and you can find them by tripping over people in other places on the internet.
08:47:56 <richardm> btw, I guess I failed to follow that policy today when I assigned the finnish guy individually, but I also sent him an email to ask him what he wants
08:48:16 <richardm> waiting on a reply at the moment
08:48:31 <fmarier> you should probably suggest the lp-l10n-fi team if it exists
08:48:56 <fmarier> has he updated the translation recently?
08:49:05 <fmarier> i thought ar and fi were very out of date
08:49:12 <richardm> Yep, that's the team i suggested
08:49:38 <anitsirk> anything else on this topic?
08:49:40 <richardm> He has a habit of doing it once, every time his site upgrades
08:50:03 <richardm> nope
08:50:16 <fmarier> would be good to give other people the ability to help out in between releases then
08:51:12 <elky> oh btw, everyone's fallen behind greek for completeness
08:51:19 <elky> they're at 100%
08:51:34 <dobedobedoh> thing is, not all languages need the full 100%
08:51:37 <dobedobedoh> e.g. english
08:51:55 <anzeljg> but there are more people translation nto greek, and only one guy to translate to slovenian + develop thinkgs ;)
08:52:06 <anzeljg> translationg into...
08:52:10 <anzeljg> ..
08:52:12 <anzeljg> ;)
08:52:23 * fmarier is done
08:52:25 <anzeljg> i can't type anymore!
08:52:39 <anitsirk> then let's move on :-)
08:52:39 <anitsirk> #topic Next meeting and Chair
08:52:40 <anitsirk> What about April 24, 2012 at 19:30 UTC? http://www.timeanddate.com/worldclock/fixedtime.html?msg=16th+Mahara+Developer+Meeting&iso=20120424T1930
08:52:41 <anitsirk> I know this is very early for most in NZ, but with the daylight savings time changes going later might be tough on anzeljg and potentially others in his time zone
08:53:27 <anzeljg> I have small kids and need to get them to bed, so it should be fine
08:53:48 <anzeljg> could you add Ljubljana/Slovenia into that list?
08:54:30 <fmarier> anzeljg: it's a proprietary service we don't control, so we can't change anything on there
08:54:38 <anzeljg> :(
08:54:46 <anitsirk> anzeljg: sorry. i can't edit that list
08:54:57 * anzeljg understands...
08:55:02 <rkabalin_> april 24th is fine with me
08:55:06 <dobedobedoh> Works for me
08:55:09 <elky> i must say, i do like their redesign
08:55:11 <anitsirk> anzeljg: would 30 min. later then be ok. i think some here might like that better ;-)
08:55:18 <dobedobedoh> Do we have enough to discuss to still hold monthly meets, or should we consider less frequent?
08:55:23 <anzeljg> no problem for me...
08:55:56 <fmarier> it could be every 6 weeks
08:56:05 <anzeljg> agree
08:56:06 <anitsirk> +1
08:56:22 <anitsirk> unless we want to keep the meeting time short to about 1 hour
08:56:48 <anitsirk> today's list of topics seems to be the right length and we are just a bit over 1 hour
08:56:53 <fmarier> that's another very good idea: frequent meetings but strict cap on number of hours
08:57:03 <fmarier> s/hours/minutes/
08:57:18 <anitsirk> i prefer shorter but more focused meetings instead of 2 hour ones
08:57:18 <anzeljg> i agree on about 1 hour per meeting
08:57:35 <anzeljg> +1
08:57:42 <rkabalin_> +1
08:58:00 <fmarier> so we need the next chair to be a very strict one ;)
08:58:15 <anitsirk> back to the question then though: still the 24th or 2 weeks later?
08:58:34 <anzeljg> 24th at 20:00 UTC
08:58:49 <anitsirk> #idea http://www.timeanddate.com/worldclock/fixedtime.html?msg=16th+Mahara+Developer+Meeting&iso=20120424T20
08:59:13 <dobedobedoh> Good for me
08:59:29 <fmarier> wfm
08:59:33 <elky> +1
08:59:38 <richardm> +1
08:59:44 <anitsirk> and i might be able to report from the MUG meeting that's 2 hours before then
09:00:06 <anitsirk> #agreed: next meeting will be on April 24, 2012 at 20:00 UTC http://www.timeanddate.com/worldclock/fixedtime.html?msg=16th+Mahara+Developer+Meeting&iso=20120424T20
09:00:16 <anitsirk> #help who wants to be chair?
09:00:29 <fmarier> elky?
09:00:53 <elky> at 8am? :(
09:01:05 <elky> I can try
09:01:53 <anitsirk> any other takers? once we suggested that the chair should come from a time zone when s/he is comfortable and not asleep ;-)
09:02:09 <fmarier> a wise idea
09:02:17 <anitsirk> ehm that was usually the evening for the chair in any time zone
09:02:43 <anzeljg> anitsirk: what are you trying to tell
09:02:52 <anitsirk> that doesn't sound right but i hope you get my drift. clearly, the evening is not for me.
09:02:53 <dobedobedoh> I can take it
09:03:06 <elky> woot!
09:03:14 <anitsirk> anzeljg: heh. that's what i had feared.
09:03:38 <anitsirk> NZ is usually wider awake in the evening, thus we chair the meetings that fall early morning for you.
09:03:52 <dobedobedoh> It means that the chair doesn't have to get up super early to prepare
09:04:08 <anzeljg> no, no...
09:04:16 <elky> and gets coffee before they have to try run a meeting
09:04:18 <anitsirk> dobedobedoh: thank you. may i take that as an agreed?
09:04:28 <dobedobedoh> if that's fine with everyone else
09:04:30 <anzeljg> I thought I was the only  one in that timezone, so she was thinking of me ;)
09:05:01 <rkabalin_> I am in that timezone as well :)
09:05:12 <anzeljg> silly me
09:05:14 <anitsirk> anzeljg: you were the one super super late. that's why i didn't want to suggest a later time which would be better for NZ. but now we had already moved on to the chair :-)
09:05:50 <anitsirk> rkabalin_ did you move?
09:06:02 <rkabalin_> no, I am in the UK
09:06:07 <dobedobedoh> rkabalin_ is sat opposite me ;)
09:06:14 <anitsirk> ah, one hour behind anzeljg ;-)
09:06:24 <elky> 1hr is close enough
09:06:29 <anzeljg> ;)
09:06:52 <anitsirk> ok. i don't see any objections to dobedobedoh chairing the next meeting.
09:07:02 <anitsirk> ekly: we can keep you in mind for the one after that ;-)
09:07:23 <elky> see also: http://xkcd.com/448/
09:07:35 <anitsirk> #agreed: dobedobedoh is going to chair the 16th Mahara Developer Meeting.
09:07:41 <anitsirk> #topic Any other business
09:07:53 <anitsirk> i have a quick one
09:07:57 <fmarier> i have one too
09:08:06 <anitsirk> #info The 5th Newsletter will be out on April 1. I still take submissions for dev stories, new plugins etc. until the end of today UTC or tomorrow if I receive a quick message that about 150 words are on its way.
09:08:16 <elky> april 1, eh?
09:08:21 <anzeljg> nothing from me this time ;)))
09:08:25 <anitsirk> sorry, fmarier: i thought i'd get mine out of the way.
09:08:27 * elky grins evilly.
09:08:28 <fmarier> that should be a hilarious newslette then :)
09:08:36 <anitsirk> elky: hehe. it's most likely going to be the 2nd for us.
09:08:41 <anitsirk> was like that last year, too.
09:09:01 <rkabalin_> fmarier: yeah, like Mahara project is going to be aquired by Google
09:09:01 <anitsirk> april 1 aussie time. that's why it shows 31st of march on the newsletter as the server is in texas
09:09:07 <fmarier> you should have a "Mahara has been purchased by Blackboard" story in there :)
09:09:22 <dobedobedoh> please no!
09:09:36 <dobedobedoh> bah
09:09:43 <elky> By Microsoft, then?
09:09:45 <dobedobedoh> It's not even 01/04 and I've fallen for one
09:09:57 <elky> hahah
09:10:15 <rkabalin_> :)
09:10:17 <elky> dobedobedoh, you saw the moodlerooms news?
09:10:19 <anitsirk> dobedobedoh: it's probably been too early yet for you to see the press release
09:10:40 <rkabalin_> dobedobedoh is too serious today ;)
09:10:44 <dobedobedoh> I've read it all
09:10:47 <anitsirk> it is e.g. http://www.sacbee.com/2012/03/26/4367794/blackboard-acquires-moodlerooms.html
09:10:51 <dobedobedoh> and netspot
09:11:20 * dobedobedoh is too tired today
09:12:02 <anitsirk> doesn't seem to be anyone shouting out that they'll have something for me for the newsletter (except anzeljg who said something already earlier). so should we move to fmarier's topic?
09:12:33 <rkabalin_> yep
09:12:37 <fmarier> My "other business" item is that this is will be my last mahara meeting as a Catalyst employee.
09:13:02 <anitsirk> #info it's the last mahara dev meeting for fmarier als catalyst employee
09:13:12 <anitsirk> :-((
09:13:20 <fmarier> i've got two weeks left and then i'm off to Auckland
09:13:34 <dobedobedoh> Congratulations! What will you be doing there?
09:13:43 <elky> you're down to a week and a half, actually :P
09:13:46 <anzeljg> Moodle HQ?
09:13:58 <fmarier> I'll be working for the Identity team at Mozilla
09:14:22 <fmarier> (i.e. not working on Firefox directly)
09:14:37 <dan_p> congrats!
09:14:39 <elky> Aww, i was lining up lots of firefox bugs to send you :P
09:14:45 <elky> (not really)
09:14:51 <fmarier> dan_p: thanks!
09:14:55 <anzeljg> all the best!
09:15:05 <fmarier> elky: bugzilla.mozilla.org :-P
09:15:27 <rkabalin_> fmarier: congratulations!
09:15:37 * elky edits her hosts file...
09:15:38 <anitsirk> i'm excited for you, fmarier though i'll miss chatting to you and learning from you (gotta read your blog more often now)
09:15:52 <anzeljg> fmarier: http://www.youtube.com/watch?v=AOkBEEABrj8
09:16:03 <elky> anitsirk, it's the definition of bittersweet, really.
09:16:48 <anzeljg> everybody leaving: first Heinz, now Fmarier...
09:18:03 <richardm> fmarier: so does the "as a catalyst employee" bit mean you intend to be in the next meeting too? :)
09:18:05 <fmarier> that's the normal cycle of life. some people leave, new people join...
09:18:10 <elky> at least he's going off to make everyone's internet more awesome
09:18:24 <anitsirk> anzeljg: we had a new developer starting today and elky is taking over some things
09:18:37 <fmarier> richardm: i'll try to make it to the meeting. i should be able to
09:18:54 <anitsirk> :-)
09:19:03 <elky> I'm fulltime on mahara now, i was doing other stuff as well previously
09:19:14 <anzeljg> nice
09:21:22 <anitsirk> i don't want to cut the tears and cheers short (we'll see fmarier around :-) ), but maybe if there is nothing else, we should come to a close?
09:21:58 <elky> wow, nearly 2 hours, i think so!
09:22:26 <anitsirk> thank you all for coming and participating.
09:22:28 <anitsirk> #endmeeting