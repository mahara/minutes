20:10:10 <iarenaza> #startmeeting
20:10:10 <maharameet> Meeting started Wed Aug 29 20:10:10 2012 UTC.  The chair is iarenaza. Information about MeetBot at http://wiki.debian.org/MeetBot.
20:10:10 <maharameet> Useful Commands: #action #agreed #help #info #idea #link #topic.
20:10:22 <iarenaza> #info iarenaza is Iñaki Arenaza, Mondragon Unibertsitatea, Spain
20:10:35 <aqualaptop> #info aqualaptop is Hugh Davenport, Catalyst IT Ltd, NZ
20:10:44 <anzeljg> #info anzeljg is Gregor Anželj, Gimnazija Bežigrad, Ljubljana, Slovenia
20:10:45 <sonn> #info sonn Son Nguyen, Catalyst IT Ltd., NZ
20:11:02 <iarenaza> #topic Items from last meetings
20:11:15 <iarenaza> There were three items in the last meeting minutes:
20:11:25 <iarenaza> 1. hugh to work with alan on maharadroid/api stuff before feature freeze
20:11:57 <aqualaptop> all done
20:12:07 <iarenaza> fine!
20:12:15 <iarenaza> 2. elky to confirm plans with alan
20:12:36 <iarenaza> I guess you can't confirm this, aqualaptop, can you?
20:12:55 <aqualaptop> hrm, do we know whats "plans" were?
20:13:10 <aqualaptop> i prob can't i'm afaid
20:13:24 <iarenaza> I don't. I think I should have had a closer look at the full irc log
20:13:50 <iarenaza> Ok, no problem. We'll move it to the pending items for next meeting.
20:14:00 <aqualaptop> sure
20:14:23 <aqualaptop> alan doesn't appear to be on our internal irc currently, i pinged elky on there though
20:14:26 <aqualaptop> no resp
20:14:53 <iarenaza> the action item is from the topic about updating code guidelines and extend it to other than php
20:15:07 <aqualaptop> ah, awesome, i thought that was a few meetings ago :P
20:15:13 <aqualaptop> https://wiki.mahara.org/index.php/Developer_Area/Coding_guidelines
20:15:17 <aqualaptop> it has php, js
20:15:28 <aqualaptop> and i'm going to put up css with help of our designer
20:15:33 <anzeljg> css?
20:15:39 * aqualaptop has had a hard time with all the new themeing reviews
20:15:40 <anzeljg> oh... i see
20:15:55 <aqualaptop> there is also sql that could go up there?
20:16:10 <aqualaptop> and maybe tpl, though we may change templating engine from smarty
20:16:15 <anzeljg> and xml perhaps?
20:16:36 <aqualaptop> the js one is basically the same as the php one, if anyone can skim over it and let me know, or make changes
20:16:45 <aqualaptop> the php one could also have an eye
20:16:51 <aqualaptop> xml could do as well
20:16:55 <aqualaptop> #link https://wiki.mahara.org/index.php/Developer_Area/Coding_guidelines
20:17:24 <aqualaptop> #info language guidelines to have, php, js, css, tpl, sql, xml
20:17:48 <iarenaza> #info If anyone can skim over the Coding guidelines, let Hugh know about changes, suggestions, etc.
20:18:32 <iarenaza> ok, so the only remaining item is Kristina's: "kristina, dajan and anzeljg to coordinate and extend anzeljg's spec to work out similarities and differences etc"
20:18:52 <iarenaza> I'm going to copy&paste what she emailed me.
20:19:11 <iarenaza> But basically, Laurent had already added his thoughts about getting his
20:19:11 <iarenaza> oEmbed plugin into core at
20:19:12 <iarenaza> https://wiki.mahara.org/index.php/Developer_Area/Specifications_in_Development/External_media_block_extension
20:19:42 <iarenaza> At that time I had asked him to wait with the implementation until we
20:19:49 <iarenaza> had finished the refactoring of the external media block for the safe
20:19:58 <iarenaza> iframe admin interface as that changed a few things for 1.6 now. Once we
20:20:04 <iarenaza> were done I let him know but haven't received a response so far. When I
20:20:09 <iarenaza> saw dajan in Fribourg at the beginning of August, he said he would talk
20:20:13 <iarenaza> to Laurent and see if he could put it into the review system. The
20:20:17 <iarenaza> earliest integration would be for Mahara 1.7 now as the feature freeze
20:20:21 <iarenaza> had been at the beginning of August.
20:20:31 <iarenaza> In regard to embed.ly: Anyone can install it, but I would recommend not
20:20:35 <iarenaza> making it part of Mahara core as it relies on a third-party and the
20:20:38 <iarenaza> availability of their APIs that are not open (if I understand
20:20:41 <iarenaza> correctly). I could see a similar problem appearing as with the Google
20:20:45 <iarenaza> Apps block where we need to update constantly to keep up. Besides, from
20:20:48 <iarenaza> 1.6 on you can use the safeiframe admin interface to easily add
20:20:52 <iarenaza> additional iframes. And they don't just work in the external media block
20:20:55 <iarenaza> but also in journals and text boxes. :-)
20:21:00 <iarenaza> That doesn't have anything to do with shared hosting though. Don't know
20:21:03 <iarenaza> what anzeljg is referring to there.
20:21:04 <iarenaza> ..
20:21:09 <iarenaza> That's it!
20:21:23 <aqualaptop> updated the guidelines page for other languages, just shells for now, but i'll put something in them
20:21:29 <aqualaptop> wow, that is an email and a half
20:21:29 <anzeljg> Kristina talked to me and said that people that are on shared hosting
20:21:53 <anzeljg> could not use oembed and that it would be nice for them to have a chance to use embed.ly
20:22:08 <anzeljg> ..
20:23:31 <iarenaza> So the idea would be to include oEmbed in core, as use embed.ly (as a contributed plugin) for those that can't use oEmbed?
20:23:58 <anzeljg> YES, if i understood that correctly
20:24:21 <aqualaptop> that looks about right
20:24:35 <aqualaptop> and make use of safe iframes as well
20:24:44 <iarenaza> yes, of course.
20:25:11 <anzeljg> regarding Google Apps
20:25:23 <anzeljg> i think we agreed that we put them out of core
20:25:43 <anzeljg> also i'm working on integrating that as a part of cloud plugin
20:25:56 <aqualaptop> yeh, that is on elky's todo list, make sure that it works with safe-iframes out of box or something
20:25:59 <anzeljg> but Google support for embedding things, etc. SUCK!!!
20:26:06 <anzeljg> ..
20:26:10 <aqualaptop> anzeljg: how is your cloud thing going?
20:26:19 <anzeljg> what do you mean
20:26:24 <iarenaza> there's nothing in the minutes about it (putting it out of core), but I seem to remember that was the general consensus
20:26:42 <anzeljg> iarenaza: a meeting before...
20:27:03 <aqualaptop> i seem to recall that came up in our internal team meeting that she was taking it out, i could be wrong though
20:27:20 <iarenaza> #idea Include oEmbed in core (making use of safe iframes), as use embed.ly (as a contributed plugin) for those that can't use oEmbed
20:27:39 <iarenaza> #undo
20:27:39 <maharameet> Removing item from minutes: <MeetBot.items.Idea object at 0x1350810>
20:27:57 <iarenaza> #idea Include oEmbed in core (making use of safe iframes), and use embed.ly (as a contributed plugin) for those that can't use oEmbed
20:28:04 <anzeljg> aqualaptop: basic frame is completed and so is support/integration of Box, Dropbox, SUgarSync, Zotero, WIndows Live SkyDrive
20:28:19 <anzeljg> finnishing integration for Google Dive and GitHub...
20:28:29 <anzeljg> Evernote on horizon...
20:28:31 <anzeljg> ..
20:29:07 <aqualaptop> nice
20:29:27 <aqualaptop> alan has appeared on our irc channel, i have asked him if he knows what "plans" are
20:29:32 <aqualaptop> to hijack a topic :P
20:30:01 <aqualaptop> no resp though
20:30:44 <iarenaza> So coming back to the oEmbed integration, we should ping Laurent
20:31:23 <iarenaza> to ask him to make any changes needed for safe iframe integration,
20:31:31 <aqualaptop> yeh, will have to wait for 1.7 now, but would be good to get it in earlyish
20:31:41 <iarenaza> and then put the code in the review system. Is that it?
20:31:59 <aqualaptop> yup
20:32:05 <iarenaza> Do we have an estimate for 1.7 feature freeze date? So we can tell Laurent
20:32:13 <aqualaptop> feature freeze is feb next year
20:32:19 <aqualaptop> https://wiki.mahara.org/index.php/6MonthlyCycle
20:32:24 <aqualaptop> #link https://wiki.mahara.org/index.php/6MonthlyCycle
20:32:35 <aqualaptop> like clockwork now :D
20:32:36 <iarenaza> Ok, that should be enough time. But as you say, the sooner, the better :-)
20:32:51 <aqualaptop> yeh, less rush on reviewers
20:33:47 <iarenaza> I'll contact dajan to get Laurent's contact, and tell him about the plan and dates.
20:34:01 <aqualaptop> sweet :D
20:34:12 <iarenaza> #action iarenaza to contact Laurent (via dajan) and tell him about the plan and the dates.
20:34:24 <iarenaza> Shall we move onto the next topic?
20:34:48 <aqualaptop> sounds good
20:34:51 <iarenaza> #topic Supported android versions (and iphone?) for responsive design (2.2?)
20:35:00 <iarenaza> all yours aqualaptop :-)
20:35:42 <aqualaptop> righto, we decided in our internal meeting the other day that we should have support for only some android versions
20:35:59 <aqualaptop> we threw out the ballpark number of 2.2, which sounds reasonable
20:36:23 <iarenaza> yep, 2.1 and lower are a minority now.
20:36:33 <aqualaptop> i also think we should do the same for iOS, but nfi how that works, /me despises apple
20:36:42 <aqualaptop> any macfans here?
20:36:49 <iarenaza> not me :-)
20:36:56 <sonn> not me
20:37:09 <anzeljg> no
20:37:12 <aqualaptop> heh
20:37:21 <iarenaza> aqualaptop: are you talking about native apps or just web thingie?
20:37:28 <aqualaptop> shall we just say current - 2, like for android?
20:37:30 <aqualaptop> oh, sorry
20:37:38 <aqualaptop> this is for the new responsive design for 1.6
20:38:06 <aqualaptop> maharadroid is done by alanmc, seperate issue
20:38:15 <iarenaza> aqualaptop: just to be sure :-)
20:38:20 <aqualaptop> don't believe there is an iphone app, but there may, but isn't core
20:38:30 <aqualaptop> so yeh, this is just the theme, so mobile browser
20:38:32 <iarenaza> aqualaptop: yes, there is, by a third party
20:39:28 <aqualaptop> righto
20:39:32 <aqualaptop> #link http://en.wikipedia.org/wiki/IOS_version_history
20:40:05 <aqualaptop> so we should upport 5.1.1
20:40:44 <aqualaptop> there "current"
20:40:53 <aqualaptop> that sound good to ppl?
20:41:25 <iarenaza> i know there are still quite a few iPhone 3G going around.
20:41:54 <iarenaza> Don't know how big is the difference between 4.2.1 and 5.1.1. browsers.
20:42:26 <iarenaza> But given that resources are limited, 5.1.1 looks like a compromise
20:43:02 <sonn> good to me
20:43:19 <aqualaptop> i'll check what android classes 2.2 as
20:43:52 <aqualaptop> so 2.2 is current - 4
20:44:09 <aqualaptop> may 10
20:44:28 <aqualaptop> so 4.2.1 should be reasonable then?
20:44:31 <aqualaptop> datewise
20:44:42 <aqualaptop> can't see anything about "supported" with android
20:45:00 <iarenaza> it depends on the manufacturer a lot.
20:45:17 <aqualaptop> or we could up the android one to 2.3?
20:45:43 <aqualaptop> thouh i think there are a lot of rougue froyo's round
20:46:01 <aqualaptop> shall we say 2.2 for android, and 4.2.1 for ios
20:46:07 <iarenaza> are we primarily targeting phones or tablets?
20:46:08 <aqualaptop> that is just for 1.6 release
20:46:15 <aqualaptop> both i believe
20:46:48 <iarenaza> cause it makes a (big) difference. Tablets are 3.x and up (except rare units)
20:47:22 <iarenaza> but phones have lots and lots of units in 2.3 (and probably 2.2)
20:48:00 <aqualaptop> i would say have same compatability with both, and then change for 1.7
20:48:21 <aqualaptop> probably just a "hunch" of what devices are out there
20:48:25 <iarenaza> But at the end, it boils down to differences in the shipped browsers. If they are bug-compatible, the os version doesn't matter
20:48:58 <aqualaptop> yeh, llike i mean, i don't think we have a backwards supported version for chrome and ff? correct me if wrong
20:49:03 <aqualaptop> though IE can diaf :D
20:49:21 <iarenaza> :-)
20:49:45 <aqualaptop> but i'm pretty sure mahara won't work right on chrome and ff in the low low versions (ie the ones on current debian stable, which can't even open facebook :P)
20:50:14 <iarenaza> so, do we agree on 2.2+ for android and 4.2.1+ for ios?
20:50:25 <aqualaptop> this all came up because one of our testers found a bug in a 2.1 device iirc
20:50:28 <aqualaptop> #agree
20:50:35 <iarenaza> #agree
20:50:36 <sonn> #agree
20:50:44 <anzeljg> #agree
20:50:50 <aqualaptop> settled
20:51:05 <aqualaptop> closer to 1.7, we can decide on new versions
20:51:11 <aqualaptop> but that should be all for now
20:51:13 <sonn> sure
20:51:15 <aqualaptop> next topic?
20:51:23 <iarenaza> #agreed support responsive design on 2.2+ for android and 4.2.1+ for ios
20:51:45 <iarenaza> #topic Testers welcome during RC period (incl new responsive design, as well as other new features)
20:52:02 <aqualaptop> righto, the next topic will explain the release process a bit more
20:52:10 <aqualaptop> but the gist is, UI freeze is happening today
20:52:17 <aqualaptop> then next week a RC will come out
20:52:26 <iarenaza> #info UI freeze is happening today
20:52:30 <aqualaptop> then in 3 weeks after that, 1.6 :D
20:52:50 <iarenaza> Whoho!
20:52:51 <aqualaptop> so, there are lots of new features in 1.6, and lots of UI changes (a responsive design for one!)
20:53:10 <iarenaza> #info lots of new features in 1.6, and lots of UI changes
20:53:15 <aqualaptop> it would be good if we can get ppl to test all this (it has been through review, but that doesn't always catch everythign)
20:53:36 <aqualaptop> so, when RC comes out, tell friends and family, install it (or upgrade it), and tes test test
20:53:39 <aqualaptop> and file bugs
20:53:53 <aqualaptop> and we shall fix any release critical bugs before 1.6
20:54:02 <aqualaptop> ..
20:54:13 <iarenaza> #info when RC comes out, install it (or upgrade it), and test, and file bugs
20:55:14 <iarenaza> ok, I'll rather busy during those 3 weeks, but I'll try to upgrade a copy of our production instance and see how it goes.
20:56:03 <iarenaza> I'll also spread the word in the Spanish Moot (19-21 Sept)
20:56:39 <aqualaptop> :D
20:56:55 <iarenaza> Any other thing on this topic?
20:57:13 <aqualaptop> that is all
20:57:26 <iarenaza> #topic Next steps for the release
20:57:45 <aqualaptop> #link https://wiki.mahara.org/index.php/6MonthlyCycle
20:57:55 <aqualaptop> as mentioned before, we have this new release cycle
20:58:16 <aqualaptop> we've had the feature freeze, and the UI freeze is happening today (NZ time, COB afaik)
20:58:26 <aqualaptop> next step is
20:58:34 <aqualaptop> - RC comes out next week
20:58:39 <aqualaptop> - 3 weeks of testing
20:58:53 <aqualaptop> during which time any release critical bugs are fixed
20:59:08 <aqualaptop> - 1st week of october, 16 is released
20:59:14 <iarenaza> #info RC comes out next week, then 3 weeks of testing (with only critical bugs fixed)
20:59:39 <iarenaza> #info 1st week of october, 1.6 is released
20:59:43 <aqualaptop> after which, bugs are fixed as normal, most on master, and only things that are critical to 1.6, and point releases made
20:59:48 <aqualaptop> any questions?
20:59:49 <aqualaptop> ..
21:00:01 <iarenaza> I don't see the release party on the calendar? :-?
21:00:31 <anzeljg> how long are new features accepted (for 1.7 i mean)
21:01:00 <aqualaptop> till about feb next year, same link, shows a cool graphic that elky put up
21:01:01 <iarenaza> anzeljg: supposedly until the next feature freeze (feb 2013)
21:01:09 <aqualaptop> showing holidays and ubuntu releases etc
21:01:30 <anzeljg> iarenaza: yeah, what about testing and review?
21:01:30 <iarenaza> aqualaptop: very informative indeed!
21:01:50 <aqualaptop> yeh, she is good at making stuff like that
21:02:09 <iarenaza> anzeljg: I guess not pushing to the last minute is always a good idea :-)
21:02:09 <aqualaptop> i found out yesterday that she has a neat thing that takes all our emails saying who is sick, and puts them in a calendar
21:02:23 <aqualaptop> yup, submit early, submit often :D
21:02:58 <iarenaza> so if you estimate that the testing and review is going to take, say, 2 weeks, better submit it one month before the deadline (just in case)
21:03:21 <iarenaza> s/one month before/at least one month before/
21:03:30 <aqualaptop> yeh i would say that
21:03:41 <aqualaptop> any closer, it starts getting prioritized
21:05:25 <iarenaza> is there a way to "brive" reviewers to prioritize one's changes? }:-)
21:05:43 <aqualaptop> brive them, prob not :P
21:05:47 <aqualaptop> bribe on the other hand
21:05:51 <aqualaptop> maybe
21:05:58 <iarenaza> s/brive/bribe/ (oops!)
21:06:00 <aqualaptop> make it a cool feature :P
21:07:04 <anzeljg> skins?!? (if time permits)
21:07:38 <aqualaptop> could be cool
21:07:41 <iarenaza> anzeljg: that would be cool. But with the new responsive design, that could be tricky
21:07:50 <aqualaptop> but yes, i think 1.7 features is a topic for another meeting :D
21:08:15 <iarenaza> anything else for this topic? anyone?
21:09:03 <anzeljg> i would like more info on responsive design (private email please)
21:10:19 <iarenaza> ok, so move on to next topic
21:10:22 <iarenaza> #topic Next meeting and Chair
21:10:27 <aqualaptop> anzeljg: flick me an email, hugh@catalyst.net.nz with what you want
21:10:32 <aqualaptop> i'll volunteer
21:11:25 <iarenaza> #action aqualaptop to chair next meeting
21:12:05 <iarenaza> I was going to propose Sept, 26 for next meeting, but that's the week before the 1.6 release.
21:12:10 <iarenaza> Maybe it's not a good idea.
21:12:18 <aqualaptop> hrm, maybe the week after
21:12:22 <aqualaptop> or 2 weeks?
21:12:34 <iarenaza> yeah, was thinking the same.
21:12:52 <iarenaza> Oct, 17?
21:13:07 <anzeljg> fine
21:13:13 <aqualaptop> fbm
21:13:14 <sonn> fine for me
21:13:20 <aqualaptop> time, evening nz this one
21:13:24 <iarenaza> 07:30 UTC this time
21:13:53 <iarenaza> http://www.timeanddate.com/worldclock/fixedtime.html?iso=20121017T0730&msg=21st%20Mahara%20Developer%20Meeting
21:13:57 <iarenaza> That would be it?
21:14:54 <aqualaptop> looks about right
21:14:59 <aqualaptop> #link http://www.timeanddate.com/worldclock/fixedtime.html?iso=20121017T0730&msg=21st%20Mahara%20Developer%20Meeting
21:15:34 <iarenaza> So we'll move on to the last topic
21:15:38 <iarenaza> #topic Any other business
21:15:41 <aqualaptop> yup
21:15:45 <anzeljg> got one
21:15:47 <iarenaza> aqualaptop: I think you had one
21:15:55 <aqualaptop> i'll be quick
21:16:10 <aqualaptop> so, there is something in the reviews for changing templating system
21:16:18 <aqualaptop> #link https://reviews.mahara.org/#/q/status:open+project:mahara+branch:master+topic:twig,n,z
21:16:26 <aqualaptop> #link https://bugs.launchpad.net/mahara/+bug/966001
21:16:41 <aqualaptop> chris at catalyst was doing this, but he is leaving tomorrow
21:17:00 <anzeljg> everybody is leaving?!?
21:17:09 <aqualaptop> it has been put off for 1.7 (because of the release, not him)
21:17:21 <aqualaptop> but i think it should get done soon after the release
21:17:31 <aqualaptop> i'll probably take over it
21:18:00 <iarenaza> twig was mostly compatible with dwoo, wasn't it?
21:18:04 <aqualaptop> he is leaving cat, but has shown interest in sticking round the mahara community, tried to get him here today, but too early :P
21:18:07 <elky> anzeljg, it's been one of those years. chris wasn't a core dev so it's not like francois and richard going, thank goodness.
21:18:17 <elky> ohai
21:18:22 <aqualaptop> ohai
21:18:31 <sonn> hi
21:18:40 <aqualaptop> so yeh, that is that, mention it at next meeting i guess
21:18:42 <aqualaptop> ..
21:18:46 <iarenaza> hi elky, are you feeling better?
21:18:55 <elky> aqualaptop, i have no clue what "plans" were at the moment
21:19:05 <aqualaptop> lol, was just asking that
21:19:21 <elky> iarenaza, trying to :) i have to give a presentation on sunday
21:19:29 <iarenaza> ups!
21:19:45 <aqualaptop> righto, i think that is all AOB for me
21:19:51 <aqualaptop> anzeljg:
21:20:48 <anzeljg> right
21:21:03 <jimcrib> I will still be around aqualaptop
21:21:04 <anzeljg> some time ago me and Kristina had a talk about PluralForms
21:21:30 <anzeljg> there were question in German Translation Community on how to translate PluralForms etc.
21:21:58 <anzeljg> I think it is a good idea that we document PluralForms and their usage somewhere on the wiki
21:22:25 <iarenaza> #idea document PluralForms and their usage somewhere on the wiki
21:22:31 <anzeljg> and also have a list of string that already are PluralForm "compatible" and the ones that shpuld become that...
21:22:31 <anzeljg> ..
21:23:06 <iarenaza> anzeljg: is PluralForms used outside the German translation?
21:23:45 <anzeljg> Slovenian (obviously)
21:24:06 <anzeljg> Should be useful for all slavic languages: Czech, Russian, Croatian, etc.
21:24:12 <anzeljg> AFAIK
21:24:12 <anzeljg> ..
21:24:47 <iarenaza> anzeljg: thanks a lot
21:25:25 <aqualaptop> awesome
21:25:37 <iarenaza> anzeljg: do you volunteer to document it?
21:26:09 <iarenaza> as you seem to know it quite well :-)
21:26:27 <anzeljg> if Kristina will help than yes...
21:26:31 <aqualaptop> oh yeh, i forgot to link this before
21:26:47 <aqualaptop> #link http://master-mahara.catalystdemo.net.nz/ (has the responsive theme on it, current with review system)
21:27:00 <aqualaptop> and will have one more AOB once everyone is done :P
21:27:00 <anzeljg> or anybody from Catalyst who knows the strings
21:27:09 <aqualaptop> ohai?
21:27:22 <aqualaptop> oh, documentation
21:27:26 <aqualaptop> swear word
21:27:31 <aqualaptop> urg
21:27:58 <elky> ...
21:27:58 <iarenaza> anzeljg: ok, won't make it an action item until you talk to Kristina :-)
21:28:52 <anzeljg> no iarenaza, just do the action item
21:28:58 <iarenaza> ok
21:29:30 <iarenaza> #action anzeljg (with help from someone at Catalyst) will document PluralForms and their usage on the wiki
21:29:44 <anzeljg> :)
21:30:15 <iarenaza> anyone else? (apart from aqualaptop)
21:31:19 <iarenaza> so aqualaptop, all yours again!
21:31:33 <aqualaptop> swt
21:31:46 <aqualaptop> right, so, as some of you may of seen if you look at the reviews
21:32:00 <aqualaptop> i tend to use one site for all of the stuff that needs testing remotely
21:32:19 <aqualaptop> i was thinking, i could hook up gerrit to our deployment system at catalyst
21:32:40 <aqualaptop> so that we can deploy a "review" and keep it up to date by "redeploying" it
21:32:58 <aqualaptop> this allows a quick and easy way to be able to test a new feature etc
21:33:09 <aqualaptop> will require someone from catalyst to do the button press
21:33:17 <aqualaptop> but yeh, that is a WIP
21:33:35 <aqualaptop> and it could be done further by automatically putting in some content etc
21:33:42 <aqualaptop> ..
21:33:55 <anzeljg> sorry, but i'll have to leave
21:33:57 <anzeljg> bye
21:34:00 <iarenaza> bye anzeljg
21:34:11 <aqualaptop> cya
21:34:13 <elky> bye anzeljg, thanks for coming
21:35:17 <aqualaptop> oh, and these testing sites will probably have automatic browserid etc set up
21:36:27 <iarenaza> but that would be for testers only, right?
21:36:48 <aqualaptop> anyone who wants to test
21:38:18 <iarenaza> Aha. And how would the give feedback to developers/reviewers? via launchpad? gerrit?
21:38:32 <aqualaptop> gerrit still
21:38:41 <aqualaptop> so it would be based of a gerrit changeset
21:38:48 <aqualaptop> exactly same code etc
21:38:57 <aqualaptop> but just installed somewhere to test easily
21:39:38 <aqualaptop> and i was thinking (more long term this one), if we actually started sending in test cases, we could test automatically, would still need someone to test manually though
21:39:56 <sonn> It would be great
21:40:56 <aqualaptop> that said, this may not happen for a while, dependant on my time
21:40:57 <iarenaza> Looks like a plan :-)
21:41:16 <aqualaptop> but yes, that is all from me now, i on't think any more AOB's for today :P
21:41:24 <iarenaza> :-D
21:41:59 <iarenaza> So anything else before I close the meeting?
21:42:37 <aqualaptop> nothing from me
21:42:53 <iarenaza> #endmeeting